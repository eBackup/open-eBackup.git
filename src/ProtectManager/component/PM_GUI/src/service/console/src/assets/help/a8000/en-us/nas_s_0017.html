<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 6: Performing Backup">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="nas_s_0011.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0017">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 6: Performing Backup</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0017"></a><a name="nas_s_0017"></a>

<h1 class="topictitle1">Step 6: Performing Backup</h1>
<div><p>Before performing backup, you need to associate the resources to be protected with a specified SLA policy. The system protects the resources based on the SLA policy and periodically performs backup jobs based on the SLA policy. You can perform a backup job immediately through manual backup.</p>
<div class="section"><h4 class="sectiontitle">Precautions</h4><ul id="nas_s_0017__ul39292282509"><li id="nas_s_0017__li147271733132211">During the execution of a manual backup job, the parameters (except <span class="uicontrol" id="nas_s_0017__uicontrol970711415461"><b><span id="nas_s_0017__text1370618444612"><strong>Automatic Retry</strong></span></b></span>) defined in the SLA, such as <span class="uicontrol" id="nas_s_0017__uicontrol10707164144616"><b><span id="nas_s_0017__text1770784134614"><strong>Rate Limiting Policies</strong></span></b></span> and <span class="uicontrol" id="nas_s_0017__uicontrol970710414466"><b><span id="nas_s_0017__text2070784164616"><strong>Automatic Indexing</strong></span></b></span>, will take effect.</li><li id="nas_s_0017__li31561026997">Copies generated by manual backup are retained for the duration defined in the SLA.</li></ul>
<ul><li>If a replication or archive policy has been defined in the SLA, the system will perform replication or archiving once based on the SLA when you perform manual backup.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="nas_s_0017__en-us_topic_0000001839142377_uicontrol1247565664010"><b><span id="nas_s_0017__en-us_topic_0000001839142377_text847555644011"><strong>Protection</strong></span> &gt; File Systems &gt; NAS File Systems</b></span>.</span><p><div class="note" id="nas_s_0017__en-us_topic_0000001839142377_note1911016143182"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0017__en-us_topic_0000001839142377_p1811010145186">For 1.5.0, choose <span class="uicontrol" id="nas_s_0017__en-us_topic_0000001839142377_uicontrol1211016146184"><b><span id="nas_s_0017__en-us_topic_0000001839142377_text51105144188"><strong>Protection</strong></span> &gt; File Services &gt; NAS File Systems</b></span>.</p>
</div></div>
</p></li><li><span>In the row where a NAS file system to be protected resides, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Protect</strong></span> </b></span>.</span></li><li><span>Select the created SLA.</span><p><p>You can also click <span class="uicontrol"><b><span><strong>Create</strong></span></b></span> to create an SLA.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0017__en-us_topic_0000001964713008_p125947145711"><span id="nas_s_0017__en-us_topic_0000001964713008_ph1413818422327">If a WORM policy has been configured for the resources to be protected, select an SLA without a WORM policy to avoid WORM policy conflicts.</span></p>
</div></div>
</p></li><li><span>Set the share protocol.</span><p><p>Specify the share protocol used for backing up a file system. Ensure that the share protocol selected during restoration is the same as that specified during backup. Otherwise, the metadata and permission information of the file system may be lost during restoration.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>If no share protocol is available, create a share for the NAS file system at the production end. If the share protocol is set to CIFS, ensure that the NAS file system to be protected at the production end uses the NTFS security style. Otherwise, backup will fail.</p>
</div></div>
</p></li><li><span>Select the agent host for executing the backup job.</span><p><p>By default, the built-in agent host is selected by the system. NAS file systems support only the built-in agent.</p>
</p></li><li><span>Set the automatic indexing function for backup copies.</span><p><p>This function is disabled by default. If a VM or VM group is selected for protection and the disks to be backed up contain system disks, the system automatically creates indexes for backup copies after this function is enabled. When using such a copy for restoration, you can select one or more files in the copy for restoration and search for files, directories, or links in the copy.</p>
<div class="note" id="nas_s_0017__note231414303259"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="nas_s_0017__ul023110513259"><li id="nas_s_0017__li132316517255">Only 1.6.0 and later versions support this parameter.</li><li id="nas_s_0017__li92931654182518">Indexes need to occupy backup storage space. If the total number of index files in the system exceeds 1 billion, perform operations by referring to "Failed to Create a Copy Index" in the <i><cite id="nas_s_0017__cite677218601619">OceanProtect <span id="nas_s_0017__vmware_gud_0028_0_nas_s_0016_en-us_topic_0000001792502994_ph187704762018">Appliance 1.5.0-1.6.0DataBackup 1.6.0</span> Troubleshooting</cite></i>.</li></ul>
</div></div>
</p></li><li><span>Set the secure archiving function.</span><p><div class="p">This parameter is displayed when the selected SLA contains archive policies. If this option is selected, only backup copies that are detected to be uninfected during ransomware detection are archived.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul><li>Only 1.6.0 and later versions support this function.</li><li>You need to click <strong>Configure</strong> to set the ransomware protection policy for the resource to be protected if the following conditions are met: <strong>Archive Time</strong> is not set to <strong>Immediately after successful backup</strong> or <strong>Archive Policy</strong> is not set to <strong>Only the latest copy</strong> in the SLA archive policy, and the ransomware protection policy is not set. For details, see "Creating Ransomware Protection and WORM Policies" in the <i><cite>OceanProtect DataBackup 1.5.0-1.6.0 Data Backup Feature Guide (Ransomware Protection for Copies)</cite></i>.</li></ul>
</div></div>
</div>
</p></li><li><span>Set the <strong>Automatic Indexing of Object Storage Archive Copies</strong> function.</span><p><p>This function is disabled by default. This parameter is displayed when <strong>Archive Storage Type</strong> is set to <span class="parmvalue"><b>Object Storage</b></span>. This function cannot be disabled after it is enabled. After this function is enabled, the system automatically creates file indexes for copies. After indexes are created, one or more files can be restored. You can also manually create indexes for copies.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="nas_s_0017__nas_s_0017_ul023110513259"><li id="nas_s_0017__nas_s_0017_li132316517255">Only 1.6.0 and later versions support this parameter.</li><li id="nas_s_0017__nas_s_0017_li92931654182518">Indexes need to occupy backup storage space. If the total number of index files in the system exceeds 1 billion, perform operations by referring to "Failed to Create a Copy Index" in the <i><cite id="nas_s_0017__nas_s_0017_cite677218601619">OceanProtect <span id="nas_s_0017__nas_s_0017_vmware_gud_0028_0_nas_s_0016_en-us_topic_0000001792502994_ph187704762018">Appliance 1.5.0-1.6.0DataBackup 1.6.0</span> Troubleshooting</cite></i>.</li></ul>
</div></div>
</p></li><li><span>Click <span class="uicontrol" id="nas_s_0017__en-us_topic_0000001964713008_uicontrol23691650677"><b>OK</b></span>.</span><p><p id="nas_s_0017__en-us_topic_0000001964713008_en-us_topic_0000001656691397_p25041111535">If the current system time is later than the start time of the first backup specified in the SLA, you can select <strong id="nas_s_0017__en-us_topic_0000001964713008_b9624185216713">Execute manual backup now</strong> in the dialog box that is displayed or choose to perform automatic backup periodically based on the backup policy set in the SLA.</p>
</p></li><li><strong>Optional: </strong><span>Perform manual backup.</span><p><div class="p">If you want to execute a backup job immediately, perform manual backup through the following operations. Otherwise, skip this step.<ol type="a"><li>In the row of the target resource, choose <strong id="nas_s_0017__en-us_topic_0000001964713008_b146814226353">More</strong> &gt; <span id="nas_s_0017__en-us_topic_0000001964713008_text18439122416533"><strong>Manual Backup</strong></span>.<div class="note" id="nas_s_0017__en-us_topic_0000001964713008_note1527151103"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0017__en-us_topic_0000001964713008_en-us_topic_0000001607531736_p870125314544">You can select multiple resources to perform manual backup in batches. Select multiple protected resources and choose <strong id="nas_s_0017__en-us_topic_0000001964713008_b1657653183519">More</strong> &gt; <span id="nas_s_0017__en-us_topic_0000001964713008_text122852865419"><strong>Manual Backup</strong></span> in the upper left corner of the resource list.</p>
</div></div>
</li><li>Set the name of the copy generated during manual backup.<p id="nas_s_0017__en-us_topic_0000001964713008_p444210492470">If this parameter is left unspecified, the system sets the copy name to <strong id="nas_s_0017__en-us_topic_0000001964713008_b17709174711816">backup_</strong><em id="nas_s_0017__en-us_topic_0000001964713008_i1371044717814">Timestamp</em> by default.</p>
</li><li>Select the <span><strong>Forever Incremental (Synthetic Full) Backup</strong></span> protection policy.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0017__en-us_topic_0000001964713008_p13522118194919"><span id="nas_s_0017__en-us_topic_0000001964713008_ph19374310343">For 1.6.0 and later versions, if the selected protection policy is different from that configured in the associated SLA, the WORM configuration does not take effect.</span></p>
</div></div>
</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</div>
</p></li></ol>
<p></p>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="nas_s_0011.html">Backing Up a NAS File System</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>