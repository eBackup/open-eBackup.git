<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring Files in a NAS Share">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="nas_s_0056.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="nas_s_0062">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring Files in a NAS Share</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="nas_s_0062"></a><a name="nas_s_0062"></a>
  <h1 class="topictitle1">Restoring Files in a NAS Share</h1>
  <div>
   <p>This section describes how to restore a NAS share file to its original location, a new location, or a local location.</p>
   <div class="section">
    <h4 class="sectiontitle">Context</h4>
    <ul id="nas_s_0062__nas_s_0060_ul1684123102616">
     <li id="nas_s_0062__nas_s_0060_li15684153110268">The <span id="nas_s_0062__nas_s_0060_text19278102815148">product</span> supports file-level restoration using backup copies, and copies archived to object storage.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Precautions</h4>
    <p id="nas_s_0062__nas_s_0060_p15863642111819">If a folder or file name contains garbled characters, file-level restoration is not supported. Do not select folders or files of this type. Otherwise, file-level restoration fails.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li>For 1.5.0, the index status of the copy used for file-level restoration must be <span class="uicontrol" id="nas_s_0062__vmware_gud_0060_0_uicontrol483213401802"><b><span id="nas_s_0062__vmware_gud_0060_0_text1711017353520"><strong>Indexed</strong></span></b></span>. For 1.6.0 and later versions, copies whose index status is <span id="nas_s_0062__vmware_gud_0060_0_text413155718286"><strong>Not indexed</strong></span> can also be used for file-level restoration.</li>
     <li>To restore a NAS share to a new location, register the NAS share at the new location or the storage device where the NAS share resides with the <span id="nas_s_0062__nas_s_0061_text5542395141">product</span>. The following lists the supported storage device types.
      <ul id="nas_s_0062__ul571916508480">
       <li id="nas_s_0062__nas_s_0061_nas_s_0059_nas_s_0013_li3415133617174">OceanStor Dorado V7</li>
       <li id="nas_s_0062__nas_s_0061_nas_s_0059_nas_s_0013_li1739551514266">OceanStor V7</li>
       <li id="nas_s_0062__nas_s_0061_nas_s_0059_nas_s_0013_li984654311123">OceanStor Dorado 6.x</li>
       <li id="nas_s_0062__nas_s_0061_nas_s_0059_nas_s_0013_li151737411958">OceanStor 6.x</li>
       <li id="nas_s_0062__nas_s_0061_nas_s_0059_nas_s_0013_li72988389413">OceanProtect 1.3.0 and later versions</li>
      </ul></li>
     <li>If you want to restore a NAS share to a new location, you need to configure the access permission for the <span id="nas_s_0062__nas_s_0061_text11734103918143">product</span> on the new storage device. For details, see <a href="nas_s_0025.html">Step 4: Configuring Access Permissions (Applicable to OceanStor V5/OceanStor Pacific/NetApp ONTAP)</a>. During the operation, configure a client or add a user for the target NAS share.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_uicontrol1387642214492"><b><span id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_text18761122134919"><strong>Explore</strong></span> &gt; <span id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_text087662214918"><strong>Copy Data</strong></span> &gt; File Systems &gt; <span id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_text2897115217345"><strong>NAS Shares</strong></span></b></span>.</span><p></p>
      <div class="note" id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_note1634925131911">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_p1934182521913">For 1.5.0, choose <span class="uicontrol" id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_uicontrol08045348196"><b><span id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_text980410346197"><strong>Explore</strong></span> &gt; <span id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_text4804183441910"><strong>Copy Data</strong></span> &gt; File Services &gt; <span id="nas_s_0062__nas_s_0061_en-us_topic_0000001839142377_text20804834141911"><strong>NAS Shares</strong></span></b></span>.</p>
       </div>
      </div> <p></p></li>
     <li><span>A copy can be searched by NAS share resource or copy. This section uses the NAS share resource as an example.</span><p></p><p id="nas_s_0062__nas_s_0061_p142551140161511">On the <span id="nas_s_0062__nas_s_0061_text6827103218214"><strong>Resources</strong></span> tab page, locate the NAS share to be recovered by NAS share name and click the name.</p> <p></p></li>
     <li><span>On the <span class="uicontrol" id="nas_s_0062__nas_s_0059_uicontrol1812414562220"><b><span id="nas_s_0062__nas_s_0059_text18993182162710"><strong>Copy Data</strong></span></b></span> page, select the year, month, and day in sequence to locate the copy.</span><p></p><p id="nas_s_0062__nas_s_0059_p9918150182310">If <span><img id="nas_s_0062__nas_s_0059_image886000161711" src="en-us_image_0000001792520510.png"></span> is displayed below a month or date, a copy exists in the month or on the day.</p> <p></p></li>
     <li><span>Locate the copy for restoration and click <span class="uicontrol" id="nas_s_0062__nas_s_0060_uicontrol144673212457"><b><span id="nas_s_0062__nas_s_0060_text16241181213589"><strong>More</strong></span> &gt; <span id="nas_s_0062__nas_s_0060_text6433142318586"><strong>File-level Restoration</strong></span></b></span> on the right of the row where the copy is located.</span><p></p>
      <ul id="nas_s_0062__nas_s_0060_ul31458297552">
       <li id="nas_s_0062__nas_s_0060_li1714532925520">For 1.5.0, select the files to be restored from the directory tree.</li>
       <li id="nas_s_0062__nas_s_0060_li1475353185516">For 1.6.0 and later versions, set <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b175942210539">File Obtaining Mode</strong>, which can be <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b29462035125211">Select file paths from the directory tree</strong> or <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b1211515508528">Enter file paths</strong>.
        <div class="note" id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_note1195634711290">
         <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
         <div class="notebody">
          <ul id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_ul14956124712297">
           <li id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_li1095674772911">If the copy contains too many files, the system may time out, preventing you from selecting files to be restored from the directory tree. Therefore, you are advised to enter the paths of files to be restored.</li>
           <li id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_li19956104762918">When entering a file path, enter a complete file path, for example, <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b745510458333">/opt/abc/efg.txt</strong> or <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b186591048123315">C:\abc\efg.txt</strong>. If you enter a folder path, for example, <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b15108423133414">/opt/abc</strong> or <strong id="nas_s_0062__nas_s_0060_vmware_gud_0060_0_b13283162543410">C:\abc</strong>, all files in the folder are restored. The file name in the path is case sensitive.</li>
          </ul>
         </div>
        </div></li>
      </ul> <p></p></li>
     <li><span>Select the object to be restored and the restoration target location.</span><p></p><p>The restoration target location can be <span><strong>Original location</strong></span>, <span><strong>New location</strong></span>, or <span><strong>Local Host Location</strong></span>.</p>
      <ul>
       <li><span><strong>Original location</strong></span>: Restores data to the original file system.</li>
       <li><span><strong>New location</strong></span>: Restores data to the file system of another device.</li>
       <li><span><strong>Local Host Location</strong></span>: Restores data to the <span>product</span>.</li>
      </ul> <p>For details about the parameters of the original location, see <a href="nas_s_0061.html#nas_s_0061__table1452025982813">Table 1</a>. For details about the parameters of a new location, see <a href="nas_s_0059.html#nas_s_0059__table429861453516">Table 3</a> and <a href="nas_s_0059.html#nas_s_0059__table1248313820599">Table 4</a>. <a href="#nas_s_0062__en-us_topic_0000001208774470_table165841851143">Table 1</a> describes the local location parameters.</p>
      <div class="tablenoborder">
       <a name="nas_s_0062__en-us_topic_0000001208774470_table165841851143"></a><a name="en-us_topic_0000001208774470_table165841851143"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="nas_s_0062__en-us_topic_0000001208774470_table165841851143" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Parameters for restoring data to the local location
        </caption>
        <colgroup>
         <col style="width:10.54%">
         <col style="width:89.46%">
        </colgroup>
        <thead align="left">
         <tr id="nas_s_0062__nas_s_0060_row6585115119414">
          <th align="left" class="cellrowborder" valign="top" width="10.54%" id="mcps1.3.5.2.5.2.4.2.3.1.1"><p id="nas_s_0062__nas_s_0060_p1658575112415">Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="89.46%" id="mcps1.3.5.2.5.2.4.2.3.1.2"><p id="nas_s_0062__nas_s_0060_p175851451349">Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr id="nas_s_0062__nas_s_0060_row1158517518417">
          <td class="cellrowborder" valign="top" width="10.54%" headers="mcps1.3.5.2.5.2.4.2.3.1.1 "><p id="nas_s_0062__nas_s_0060_p158517511048"><span id="nas_s_0062__nas_s_0060_text191084227520"><strong>File Systems</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="89.46%" headers="mcps1.3.5.2.5.2.4.2.3.1.2 "><p id="nas_s_0062__nas_s_0060_p15851451646">Select the file system to be restored to the local device. When restoring data to the local location for the first time, you can click <span class="uicontrol" id="nas_s_0062__nas_s_0060_uicontrol124218413611"><b><span id="nas_s_0062__nas_s_0060_text128775231364"><strong>Create</strong></span></b></span> to create a file system. For details about how to create a file system, see <a href="nas_s_0100.html">Creating a File System</a>.</p></td>
         </tr>
         <tr id="nas_s_0062__nas_s_0060_row1358516511642">
          <td class="cellrowborder" valign="top" width="10.54%" headers="mcps1.3.5.2.5.2.4.2.3.1.1 "><p id="nas_s_0062__nas_s_0060_p1725084223716"><span id="nas_s_0062__nas_s_0060_text132479819312"><strong>Overwrite Rule</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="89.46%" headers="mcps1.3.5.2.5.2.4.2.3.1.2 "><p id="nas_s_0062__nas_s_0060_p172501042153719">If files with the same names exist in the restoration path, you can choose to replace or skip existing files.</p>
           <ul id="nas_s_0062__nas_s_0060_ul15250642173717">
            <li id="nas_s_0062__nas_s_0060_li52501642173711"><span id="nas_s_0062__nas_s_0060_text14250042193711"><strong>Replace existing files</strong></span></li>
            <li id="nas_s_0062__nas_s_0060_li15250154283711"><span id="nas_s_0062__nas_s_0060_text52507424377"><strong>Skip existing files</strong></span></li>
            <li id="nas_s_0062__nas_s_0060_li22501342183719"><span id="nas_s_0062__nas_s_0060_text32501742143716"><strong>Only replace the files older than the restoration file</strong></span></li>
           </ul></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol" id="nas_s_0062__nas_s_0061_uicontrol8682163517294"><b><span id="nas_s_0062__nas_s_0061_text923644131319"><strong>OK</strong></span></b></span>. Before you restore data using the copy, ensure that the target location for restoration has sufficient free space for restoration. The confirmation method is as follows:</span><p></p>
      <ul id="nas_s_0062__nas_s_0061_ul698861123720">
       <li id="nas_s_0062__nas_s_0061_li17226131513373">Requirements for restoration using a full copy: Remaining space of the target location ≥ Data volume of the full copy before reduction. To obtain the data volume of the full copy before reduction, view <span class="uicontrol" id="nas_s_0062__nas_s_0061_uicontrol6781131216406"><b>Data Volume Before Reduction</b></span> of the backup job on the <span class="uicontrol" id="nas_s_0062__nas_s_0061_uicontrol18893135113394"><b>Job Information</b></span> tab page.</li>
       <li id="nas_s_0062__nas_s_0061_li3988191115372">Requirements for restoration using forever incremental copies or incremental copies: Remaining space of the target location ≥ Total amount of data of all copies generated in the following period before reduction: from the current permanent incremental copy or incremental copy to the last full copy. To obtain the total amount of data before reduction, add the value of <span class="uicontrol" id="nas_s_0062__nas_s_0061_uicontrol561253514498"><b>Data Volume Before Reduction</b></span> of each backup job on the <span class="uicontrol" id="nas_s_0062__nas_s_0061_uicontrol81551548216"><b>Job Information</b></span> tab page.</li>
      </ul> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="nas_s_0056.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>