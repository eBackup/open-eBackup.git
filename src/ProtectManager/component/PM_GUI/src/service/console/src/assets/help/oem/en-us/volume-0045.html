<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring Volumes">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="volume-0042.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="volume-0045">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring Volumes</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="volume-0045"></a><a name="volume-0045"></a>
  <h1 class="topictitle1">Restoring Volumes</h1>
  <div>
   <p>This section describes how to restore a volume that has been backed up to the original or a new location.</p>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <p>Before restoration, ensure that the remaining space of the data directory at the target location for restoration is greater than the size of the copy used for restoration before reduction. Otherwise, restoration will fail.</p>
   </div>
   <div class="section" id="volume-0045__section205672348712">
    <h4 class="sectiontitle">Precautions</h4>
    <p id="volume-0045__p1521143817711">If the file system version of the target host for restoration is earlier than that of the source host and the source volumes use features of the source host's file system version, the restored volumes on the target host will be unavailable.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="volume-0045__en-us_topic_0000001839142377_uicontrol2728131538"><b><span id="volume-0045__en-us_topic_0000001839142377_text27283316312"><strong>Explore</strong></span> &gt; <span id="volume-0045__en-us_topic_0000001839142377_text157282318317"><strong>Copy Data</strong></span> &gt; File Systems &gt; <span id="volume-0045__en-us_topic_0000001839142377_text198473585287"><strong>Volumes</strong></span></b></span>.</span></li>
     <li><span>You can search for copies by volume resource or copy. This section describes how to search for copies by resource.</span><p></p><p>On the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab page, find the resource to be restored by resource name and click the resource name.</p> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> and select the year, month, and day to find the copy.</span><p></p><p>If <span><img src="en-us_image_0000001798564508.png"></span> is displayed below a month or date, copies exist in the month or on the day.</p> <p></p></li>
     <li><span>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Restore</strong></span></b></span> in the row where the copy is located.</span><p></p><p><a href="#volume-0045__table93951625101715">Table 1</a> describes the related parameters.</p>
      <div class="tablenoborder">
       <a name="volume-0045__table93951625101715"></a><a name="table93951625101715"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="volume-0045__table93951625101715" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Parameters for volume restoration
        </caption>
        <colgroup>
         <col style="width:13.170000000000002%">
         <col style="width:15.7%">
         <col style="width:71.13000000000001%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.4.2.4.2.2.2.4.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" id="mcps1.3.4.2.4.2.2.2.4.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Bare Metal Restore</p></td>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p>This function is disabled by default. Enabling this function will restore the entire OS.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>Bare metal restore (BMR) cannot be enabled for copies with OS backup disabled.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Restore Non-System Volume</p></td>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p>This parameter is displayed after <strong>Bare Metal Restore</strong> is enabled. By default, this function is disabled. After this function is enabled, non-system volumes will be restored.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Restart System After Successful Restoration</p></td>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p>This parameter is displayed after <strong>Bare Metal Restore</strong> is enabled. By default, this function is disabled. After this function is enabled, the system will restart.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Restore type</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>This parameter is available only for the Windows OS.</p>
            </div>
           </div></td>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p>Select a backup type: <strong>Volume</strong> or <strong>Bare Metal</strong>.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>Before performing bare metal restoration, start the target host by referring to <a href="en-us_topic_0000002092966049.html">Starting the Windows OS in WinPE Mode and Then Configuring an IP Address</a>.</li>
              <li>After bare metal restoration is complete, if some disks are in the <strong>Foreign</strong> state, restore the disk status by referring to <a href="en-us_topic_0000002102764745.html">Some Disks Are in the Foreign State After the Windows OS Is Restored</a>.</li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Select Volumes to Be Restored</p></td>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p>Select the volumes to be restored. You can search for the target volume by volume name.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>If <strong>Bare Metal Restore</strong> is enabled, you do not need to select the volumes to be recovered.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" rowspan="2" valign="top" width="13.170000000000002%" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Restoration Configuration</p> <p></p></td>
          <td class="cellrowborder" valign="top" width="15.7%" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Restore To</p></td>
          <td class="cellrowborder" valign="top" width="71.13000000000001%" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p>Location where a volume is to be restored.</p>
           <ul>
            <li><strong>Original location</strong>: Restores data to the original location on the original host.</li>
            <li><strong>New location</strong>: Restores data to a new location. You need to specify the target host for restoration.
             <div class="note">
              <span class="notetitle"> NOTE: </span>
              <div class="notebody">
               <p>If you use LiveCD to start the original host, you cannot restore copy data to the original location. If you want to use the original host configuration, select the original host as the target host for restoration to a new location.</p>
              </div>
             </div></li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Target Host</p></td>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>You need to select the target host for restoration only when data is restored to a new location.</p> <p>You can search for a host name or IP address to select the host.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" rowspan="3" valign="top" width="13.170000000000002%" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Advanced</p></td>
          <td class="cellrowborder" valign="top" width="15.7%" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Script to Run Before Restoration</p></td>
          <td class="cellrowborder" rowspan="3" valign="top" width="71.13000000000001%" headers="mcps1.3.4.2.4.2.2.2.4.1.2 "><p id="volume-0045__p16797181010379">You can execute a custom script before backup, upon backup success, or upon backup failure based on your need.</p> <p id="volume-0045__p1742941713447">Place the execution script in the <strong id="volume-0045__b66555811311">DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty</strong> directory, for example, the relative path of <strong id="volume-0045__b465517816315">/opt/DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty/</strong>.</p> <p id="volume-0045__p525748151117">You can execute custom scripts of the sh type based on your needs before a restoration job is executed, or after the restoration job is successfully or fails to be executed. In addition, place the script in the <strong id="volume-0045__b10333183073218">DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty</strong> directory.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p id="volume-0045__files-0045_Files-0015_0_postgresql-0045_en-us_topic_0000001264099602_p196461512103113">If <span class="uicontrol" id="volume-0045__files-0045_uicontrol4895744101218"><b><span id="volume-0045__files-0045_text289518444121">Script to Run upon Restoration Success</span></b></span> is configured, the status of the restoration job is displayed as <span class="uicontrol" id="volume-0045__files-0045_uicontrol98967449122"><b><span id="volume-0045__files-0045_text2896184412127">Successful</span></b></span> on the <span id="volume-0045__files-0045_text6896154491210">product</span> even if the script fails to be executed. Check whether the job details contain information indicating that a post-processing script fails to be executed. If yes, modify the script in a timely manner.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Script to Run upon Restoration Success</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.2.2.4.1.1 "><p>Script to Run upon Restoration Failure</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="volume-0042.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>