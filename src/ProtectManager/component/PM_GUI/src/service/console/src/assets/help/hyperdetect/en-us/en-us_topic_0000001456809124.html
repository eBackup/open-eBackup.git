<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Replacing the Management Plane Domain Authentication Certificate">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="EN-US_TOPIC_0000001456809124">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Replacing the Management Plane Domain Authentication Certificate</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="EN-US_TOPIC_0000001456809124"></a><a name="EN-US_TOPIC_0000001456809124"></a>
  <h1 class="topictitle1">Replacing the Management Plane Domain Authentication Certificate</h1>
  <div id="body0000001142094068">
   <p id="EN-US_TOPIC_0000001456809124__p13504145516426">When a storage device communicates with an LDAP domain server, the storage device functions as a client and the LDAP domain server functions as a server. In this scenario, a CA certificate must be imported, but a client certificate is optional.</p>
   <div class="section" id="EN-US_TOPIC_0000001456809124__section7135421349">
    <h4 class="sectiontitle">Context</h4>
    <p id="EN-US_TOPIC_0000001456809124__p33477314415">No default certificate exists in the domain authentication scenario.</p>
   </div>
   <div class="section" id="EN-US_TOPIC_0000001456809124__section136381225269">
    <h4 class="sectiontitle">Procedure</h4>
    <ol id="EN-US_TOPIC_0000001456809124__ol682142852620">
     <li id="EN-US_TOPIC_0000001456809124__li5239141322519"><span>Obtain the certificate signing request (CSR) file and private key.</span><p></p>
      <div class="p" id="EN-US_TOPIC_0000001456809124__p15568201392517">
       The following two methods are supported:
       <ul class="subitemlist" id="EN-US_TOPIC_0000001456809124__ul199551854174810">
        <li id="EN-US_TOPIC_0000001456809124__li09558541489"><a name="EN-US_TOPIC_0000001456809124__li09558541489"></a><a name="li09558541489"></a>On DeviceManager, export the CSR file in the domain authentication scenario. The private key is generated in the storage device and saved to a database.
         <ol type="a" id="EN-US_TOPIC_0000001456809124__ol121595321147">
          <li id="EN-US_TOPIC_0000001456809124__li7159123216414">Log in to DeviceManager.
           <ol class="substepthirdol" id="EN-US_TOPIC_0000001456809124__ol8463855369">
            <li id="EN-US_TOPIC_0000001456809124__li10464175173610">Log in to ProtectManager.</li>
            <li id="EN-US_TOPIC_0000001456809124__li929117916361">Choose <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol6877162172315"><b><span id="EN-US_TOPIC_0000001456809124__text38771428238"><strong>System</strong></span> &gt; <span id="EN-US_TOPIC_0000001456809124__text13877152132314"><strong>Infrastructure</strong></span> &gt; <span id="EN-US_TOPIC_0000001456809124__text98779217234"><strong>Local Storage</strong></span></b></span>.</li>
            <li id="EN-US_TOPIC_0000001456809124__li81908119372">Click <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol67691448193413"><b><span id="EN-US_TOPIC_0000001456809124__text1976913489342"><strong>Open the device management platform</strong></span></b></span> in the <strong id="EN-US_TOPIC_0000001456809124__b157701948143411">Local Storage</strong> area. The DeviceManager page is displayed.</li>
           </ol></li>
          <li id="EN-US_TOPIC_0000001456809124__li205759381342">Choose <strong id="EN-US_TOPIC_0000001456809124__b1023119013352">Settings</strong> &gt; <strong id="EN-US_TOPIC_0000001456809124__b92321103351">Certificate</strong> &gt; <strong id="EN-US_TOPIC_0000001456809124__b6232502350">Certificate Management</strong>.</li>
          <li id="EN-US_TOPIC_0000001456809124__li4501819511">Select <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol17934952957"><b>Management plane domain authentication certificate</b></span>.</li>
          <li id="EN-US_TOPIC_0000001456809124__li172011019618">Click <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol47121515611"><b>Export Request File</b></span>.</li>
          <li id="EN-US_TOPIC_0000001456809124__en-us_topic_0232105725_li599144918413">Set <strong id="EN-US_TOPIC_0000001456809124__b2605632154018">Certificate Key Algorithm</strong>.
           <div class="msonormal" id="EN-US_TOPIC_0000001456809124__en-us_topic_0272257835_p20114152695013">
            <a href="#EN-US_TOPIC_0000001456809124__table1471382513466">Table 1</a> describes related parameters. 
            <div class="tablenoborder">
             <a name="EN-US_TOPIC_0000001456809124__table1471382513466"></a><a name="table1471382513466"></a>
             <table cellpadding="4" cellspacing="0" summary="" id="EN-US_TOPIC_0000001456809124__table1471382513466" frame="border" border="1" rules="all">
              <caption>
               <b>Table 1 </b>Exporting CSR file parameters
              </caption>
              <colgroup>
               <col style="width:26.14%">
               <col style="width:73.86%">
              </colgroup>
              <thead align="left">
               <tr id="EN-US_TOPIC_0000001456809124__row1671342554618">
                <th align="left" class="cellrowborder" valign="top" width="26.14%" id="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1"><p id="EN-US_TOPIC_0000001456809124__en-us_topic_0272257835_p511413268502">Parameter</p></th>
                <th align="left" class="cellrowborder" valign="top" width="73.86%" id="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2"><p id="EN-US_TOPIC_0000001456809124__en-us_topic_0272257835_p1211482612509">Description</p></th>
               </tr>
              </thead>
              <tbody>
               <tr id="EN-US_TOPIC_0000001456809124__row16713142554611">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p3713202554616">Certificate Key Algorithm</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p1157151184619">Select an algorithm to generate a CSR file. The default value is <strong id="EN-US_TOPIC_0000001456809124__b15717113215423">RSA_2048</strong>.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row97131525124612">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p97133253463">Country</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p18713102554618">Value of the <strong id="EN-US_TOPIC_0000001456809124__b182941252194716">country</strong> parameter in the <strong id="EN-US_TOPIC_0000001456809124__b1417689114910">subject</strong> field when a CSR file is generated. The default value is <strong id="EN-US_TOPIC_0000001456809124__b1798342764620">CN</strong>.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row15713172584610">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p139761356155511">State or Province</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p471332534612">Value of the <strong id="EN-US_TOPIC_0000001456809124__b52011444125017">state or province name</strong> parameter in the <strong id="EN-US_TOPIC_0000001456809124__b9449144910504">subject</strong> field when a CSR file is generated. No default value.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row117131325194610">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p99761756175519">City or Locality</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p13639184316143">Value of the <strong id="EN-US_TOPIC_0000001456809124__b7205115812502">locality</strong> parameter in the <strong id="EN-US_TOPIC_0000001456809124__b16210185825019">subject</strong> field when a CSR file is generated. No default value.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row16713122518468">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p189761156115514">Organization</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p8997726111311">Value of the <strong id="EN-US_TOPIC_0000001456809124__b2976215165115">organization</strong> parameter in the <strong id="EN-US_TOPIC_0000001456809124__b4981715135119">subject</strong> field when a CSR file is generated. The default value is <strong id="EN-US_TOPIC_0000001456809124__b109457291512">Huawei</strong>.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row4713142516466">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p1097614561558">Organization Unit</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p85601129121312">Value of the <strong id="EN-US_TOPIC_0000001456809124__b12571538105117">organizational unit</strong> parameter in the <strong id="EN-US_TOPIC_0000001456809124__b291514125117">subject</strong> field when a CSR file is generated. The default value is <strong id="EN-US_TOPIC_0000001456809124__b53391048105120">Storage</strong>.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row1713132511461">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p39761756125511">Common Name</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p14401435111318">Value of the <strong id="EN-US_TOPIC_0000001456809124__b159637514517">common name</strong> parameter in the <strong id="EN-US_TOPIC_0000001456809124__b596345116513">subject</strong> field when a CSR file is generated. The default value is <strong id="EN-US_TOPIC_0000001456809124__b0433158145111">ESN</strong>.</p></td>
               </tr>
               <tr id="EN-US_TOPIC_0000001456809124__row16636171725611">
                <td class="cellrowborder" valign="top" width="26.14%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.1 "><p id="EN-US_TOPIC_0000001456809124__p1297635615553">Subject Alternative Name</p></td>
                <td class="cellrowborder" valign="top" width="73.86%" headers="mcps1.3.3.2.1.2.1.1.1.1.5.2.2.2.3.1.2 "><p id="EN-US_TOPIC_0000001456809124__p1563621715569">An extended field in the X509 standard for exporting the CSR file. The SSL certificate that uses the <strong id="EN-US_TOPIC_0000001456809124__b229682514520">Subject Alternative Name</strong> field can extend the domain names supported by the certificate so that one certificate can support the resolution of multiple domain names.</p> <p id="EN-US_TOPIC_0000001456809124__p5584841191110">Examples are as follows:</p> <pre class="screen" id="EN-US_TOPIC_0000001456809124__screen331255111111">DNS:*.huawei.com,IP:10.10.10.10,IP:13::17,email:me@huawei.com,RID:1.2.3.5,URI://my.url.here/,otherName:1.0.0.1;UTF8:some\sother\sidentifier,dirName:dir_name\n[dir_name]\nC=UK\nST=London\nCN=My\sname country=CN state_province=Beijing locality=Chengdu organization=Huawei organizational_unit=Huawei\sStorage common_name=Huawei\sIT@storage</pre></td>
               </tr>
              </tbody>
             </table>
            </div>
            <div class="note" id="EN-US_TOPIC_0000001456809124__en-us_topic_0272257835_note131151026195016">
             <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
             <div class="notebody">
              <p class="text" id="EN-US_TOPIC_0000001456809124__en-us_topic_0272257835_p13115202611506">Parameters such as <strong id="EN-US_TOPIC_0000001456809124__b10125154118523">Country</strong>, <strong id="EN-US_TOPIC_0000001456809124__b1313234145214">State or Province</strong>, and <strong id="EN-US_TOPIC_0000001456809124__b14133341165213">City or Locality</strong> are hidden. You can click <strong id="EN-US_TOPIC_0000001456809124__b1364144965215">Advanced</strong> to display them.</p>
             </div>
            </div>
           </div></li>
          <li id="EN-US_TOPIC_0000001456809124__li32181256278">click <strong id="EN-US_TOPIC_0000001456809124__b369511561527">OK</strong>.</li>
         </ol></li>
        <li id="EN-US_TOPIC_0000001456809124__li1695585410483"><a name="EN-US_TOPIC_0000001456809124__li1695585410483"></a><a name="li1695585410483"></a>Use the OpenSSL tool to generate a private key in plaintext and a CSR file.
         <div class="p" id="EN-US_TOPIC_0000001456809124__p786843114379">
          <a name="EN-US_TOPIC_0000001456809124__li1695585410483"></a><a name="li1695585410483"></a>Run the following command on the CLI of OpenSSL to generate a private key in plaintext and a CSR file:
          <pre class="screen" id="EN-US_TOPIC_0000001456809124__screen39196283">openssl genrsa -out test.key 2048 </pre>
          <pre class="screen" id="EN-US_TOPIC_0000001456809124__screen17222231">openssl req -new -sha256 -days 3650 -subj </pre>
          <pre class="screen" id="EN-US_TOPIC_0000001456809124__screen20782358">/C=XXX/ST=XXXX/L=XXX/O=XXXX/OU=XXXX/CN=XXXX -key test.key -out test.csr     </pre>
         </div> <p class="msonormal" id="EN-US_TOPIC_0000001456809124__p52823495">Parameter description:</p>
         <ul id="EN-US_TOPIC_0000001456809124__ul5649413">
          <li id="EN-US_TOPIC_0000001456809124__li63026925"><strong id="EN-US_TOPIC_0000001456809124__b1566462235312">C</strong>: Country</li>
          <li id="EN-US_TOPIC_0000001456809124__li30371416"><strong id="EN-US_TOPIC_0000001456809124__b44137304530">ST</strong>: State/Province</li>
          <li id="EN-US_TOPIC_0000001456809124__li4907296"><strong id="EN-US_TOPIC_0000001456809124__b1391510312536">L</strong>: City/Town</li>
          <li id="EN-US_TOPIC_0000001456809124__li44165669"><strong id="EN-US_TOPIC_0000001456809124__b19334533125314">O</strong>: Organization</li>
          <li id="EN-US_TOPIC_0000001456809124__li61946704"><strong id="EN-US_TOPIC_0000001456809124__b9229352534">OU</strong>: Organization unit</li>
          <li id="EN-US_TOPIC_0000001456809124__li73621349194516"><strong id="EN-US_TOPIC_0000001456809124__b631816459533">CN</strong>: indicates the name. The ESN of the current storage device is used as the <strong id="EN-US_TOPIC_0000001456809124__b1730610131228">CN</strong>. You can view the ESN on the home page of DeviceManager.</li>
         </ul></li>
       </ul>
      </div> <p></p></li>
     <li id="EN-US_TOPIC_0000001456809124__li095514544488"><span>Send the exported CSR file to a third-party CA center. After the CA center signs it or you use your enterprise's root certificate to sign it, the certificate file is generated.</span><p></p><p class="MsoNormal" id="EN-US_TOPIC_0000001456809124__p57087753"><a href="#EN-US_TOPIC_0000001456809124__table60705275">Table 2</a> describes the common third-party CA centers.</p>
      <div class="tablenoborder">
       <a name="EN-US_TOPIC_0000001456809124__table60705275"></a><a name="table60705275"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="EN-US_TOPIC_0000001456809124__table60705275" frame="border" border="1" rules="all">
        <caption>
         <b>Table 2 </b>Frequently used third-party CA centers
        </caption>
        <colgroup>
         <col style="width:21.43%">
         <col style="width:39.800000000000004%">
         <col style="width:38.78%">
        </colgroup>
        <thead align="left">
         <tr id="EN-US_TOPIC_0000001456809124__row65725252">
          <th align="left" class="cellrowborder" valign="top" width="21.42785721427857%" id="mcps1.3.3.2.2.2.2.2.4.1.1"><p id="EN-US_TOPIC_0000001456809124__p22145185">Name of Third-Party CA Center</p></th>
          <th align="left" class="cellrowborder" valign="top" width="39.796020397960206%" id="mcps1.3.3.2.2.2.2.2.4.1.2"><p id="EN-US_TOPIC_0000001456809124__p48929542">Introduction</p></th>
          <th align="left" class="cellrowborder" valign="top" width="38.77612238776123%" id="mcps1.3.3.2.2.2.2.2.4.1.3"><p id="EN-US_TOPIC_0000001456809124__p3869980">Link</p></th>
         </tr>
        </thead>
        <tbody>
         <tr id="EN-US_TOPIC_0000001456809124__row45032970">
          <td class="cellrowborder" valign="top" width="21.42785721427857%" headers="mcps1.3.3.2.2.2.2.2.4.1.1 "><p id="EN-US_TOPIC_0000001456809124__p23791993">VeriSign</p></td>
          <td class="cellrowborder" valign="top" width="39.796020397960206%" headers="mcps1.3.3.2.2.2.2.2.4.1.2 "><p id="EN-US_TOPIC_0000001456809124__p48103313">Digital certificates of VeriSign are the most complete and support most applications and devices. The supported SSL certificates include Secure Site Pro, Secure Site, Secure Site Pro with EV, and Secure Site with EV.</p></td>
          <td class="cellrowborder" valign="top" width="38.77612238776123%" headers="mcps1.3.3.2.2.2.2.2.4.1.3 "><p id="EN-US_TOPIC_0000001456809124__p4054265"><a href="https://www.verisign.com/" target="_blank" rel="noopener noreferrer">https://www.verisign.com/</a></p></td>
         </tr>
         <tr id="EN-US_TOPIC_0000001456809124__row59960025">
          <td class="cellrowborder" valign="top" width="21.42785721427857%" headers="mcps1.3.3.2.2.2.2.2.4.1.1 "><p id="EN-US_TOPIC_0000001456809124__p24923866">GeoTrust</p></td>
          <td class="cellrowborder" valign="top" width="39.796020397960206%" headers="mcps1.3.3.2.2.2.2.2.4.1.2 "><p id="EN-US_TOPIC_0000001456809124__p5567267">Digital certificates of GeoTrust are mainly SSL certificates, including the following three types:</p>
           <ul id="EN-US_TOPIC_0000001456809124__ul50105411">
            <li id="EN-US_TOPIC_0000001456809124__li48295517">The ultrafast SSL series (QuickSSL Premium, RapidSSL, and Power Server ID) that verifies domain name ownership and does not verify the business license</li>
            <li id="EN-US_TOPIC_0000001456809124__li32006476">Certificate that requires verification of the business license (True Business ID)</li>
            <li id="EN-US_TOPIC_0000001456809124__li19622832">Newly launched EV SSL certificate (True Business ID with EV)</li>
           </ul></td>
          <td class="cellrowborder" valign="top" width="38.77612238776123%" headers="mcps1.3.3.2.2.2.2.2.4.1.3 "><p id="EN-US_TOPIC_0000001456809124__p45945539"><a href="https://www.geotrust.com/ssl/" target="_blank" rel="noopener noreferrer">https://www.geotrust.com/ssl/</a></p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li id="EN-US_TOPIC_0000001456809124__li775722792914"><a name="EN-US_TOPIC_0000001456809124__li775722792914"></a><a name="li775722792914"></a><span>Import the certificate file.</span><p></p>
      <ul id="EN-US_TOPIC_0000001456809124__ul1575182472912">
       <li id="EN-US_TOPIC_0000001456809124__li57512419297">If the CSR file is generated on DeviceManager, import the signed certificate and the CA certificate downloaded from the trusted third-party CA center that signed the LDAP domain server certificate to the storage device on DeviceManager.</li>
       <li id="EN-US_TOPIC_0000001456809124__li1475172416296">If the CSR file is generated using the OpenSSL tool, import the plaintext private key, signed certificate, and CA certificate downloaded from the trusted third-party CA center that signed LDAP domain server certificate to the storage device on DeviceManager.</li>
      </ul>
      <div class="p" id="EN-US_TOPIC_0000001456809124__p1059311316474">
       To import the certificates on DeviceManager, perform the following steps:
       <ol type="a" id="EN-US_TOPIC_0000001456809124__ol1261253064717">
        <li id="EN-US_TOPIC_0000001456809124__li12612183018476">Log in to DeviceManager.
         <ol class="substepthirdol" id="EN-US_TOPIC_0000001456809124__ol0963615245">
          <li id="EN-US_TOPIC_0000001456809124__li59632118245">Log in to ProtectManager.</li>
          <li id="EN-US_TOPIC_0000001456809124__li5963201162420">Choose <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol44189154"><b><span id="EN-US_TOPIC_0000001456809124__text841329212"><strong>System</strong></span> &gt; <span id="EN-US_TOPIC_0000001456809124__text129671772"><strong>Infrastructure</strong></span> &gt; <span id="EN-US_TOPIC_0000001456809124__text442206673"><strong>Local Storage</strong></span></b></span>.</li>
          <li id="EN-US_TOPIC_0000001456809124__li39633112249">Click <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol1729431601"><b><span id="EN-US_TOPIC_0000001456809124__text1671491298"><strong>Open the device management platform</strong></span></b></span> in the <strong id="EN-US_TOPIC_0000001456809124__b37255333">Local Storage</strong> area. The DeviceManager page is displayed.</li>
         </ol></li>
        <li id="EN-US_TOPIC_0000001456809124__li861283014474">Choose <strong id="EN-US_TOPIC_0000001456809124__b1522421514577">Settings</strong> &gt; <strong id="EN-US_TOPIC_0000001456809124__b182252159576">Certificate</strong> &gt; <strong id="EN-US_TOPIC_0000001456809124__b1822514154573">Certificate Management</strong>.</li>
        <li id="EN-US_TOPIC_0000001456809124__li166128308471">Select <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol1711971720578"><b>Management plane domain authentication certificate</b></span>.</li>
        <li id="EN-US_TOPIC_0000001456809124__li10612730164712">Click <strong id="EN-US_TOPIC_0000001456809124__b19245171995710">Import Certificate</strong>.</li>
        <li id="EN-US_TOPIC_0000001456809124__li136121830134712">Select <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol1480134814711"><b>Certificate File</b></span>, <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol1411965834714"><b>CA Certificate File</b></span>, and <span class="uicontrol" id="EN-US_TOPIC_0000001456809124__uicontrol119201939482"><b>Private Key File</b></span>.</li>
        <li id="EN-US_TOPIC_0000001456809124__li9612103013472">click <strong id="EN-US_TOPIC_0000001456809124__b983419574010">OK</strong>.</li>
       </ol>
      </div> <p id="EN-US_TOPIC_0000001456809124__p197581227132914">In the scenario where the server verifies the client, to replace the security certificate on the domain server, the CA certificate of the storage device must be imported to the domain server after the certificate and private key are imported to the storage device.</p> <p></p></li>
    </ol>
   </div>
  </div>
 </body>
</html>