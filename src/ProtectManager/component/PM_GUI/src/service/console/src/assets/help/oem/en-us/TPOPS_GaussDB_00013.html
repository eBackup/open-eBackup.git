<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 2: Enabling the XBSA Backup Whitelist on the TPOPS Node">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="TPOPS_GaussDB_00010.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="TPOPS_GaussDB_00013">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 2: Enabling the XBSA Backup Whitelist on the TPOPS Node</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="TPOPS_GaussDB_00013"></a><a name="TPOPS_GaussDB_00013"></a>
  <h1 class="topictitle1">Step 2: Enabling the XBSA Backup Whitelist on the TPOPS Node</h1>
  <div>
   <p>Before the GaussDB backup function is used, you need to enable the XBSA feature and XBSA PITR feature whitelists to ensure the normal backup function.</p>
   <div class="p" id="TPOPS_GaussDB_00013__p1417941134413">
    You can determine whether to perform operations in this section based on the management platform in use.
    <ul id="TPOPS_GaussDB_00013__ul126368123442">
     <li id="TPOPS_GaussDB_00013__li1963611216447">For the OLTP OPS management platform, perform operations in this section.</li>
     <li id="TPOPS_GaussDB_00013__li5226451173818">For the cloud database GaussDB management platform, skip this section.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <p><strong>Enable the XBSA Feature Whitelist</strong> <strong>(Applicable to Full Backup, Differential Backup, and Log Backup)</strong></p>
    <ol>
     <li><span>Use an SSH tool to log in to the active TPOPS server node as user <strong>root</strong> through the management IP address of the node.</span></li>
     <li><span>On the TPOPS node, run the following command to log in to the TPOPS node as user <strong>dmuser</strong>:</span><p></p><pre class="screen">su - dmuser</pre> <p></p></li>
     <li><span>Run the following commands to connect to the TPOPS metadata database:</span><p></p><pre class="screen">source $HOME/.dmbashrc; 
source $DM_HOME/primdb/.infostore; 
gsql -d primdb -p 22202 -r -U apprim -W <em>dmuser password</em></pre> <p></p></li>
     <li><span>Run the following command to enable the OPENAPI_XBSA_BACKUP_RESOTRE feature:</span><p></p><pre class="screen">UPDATE PUBLIC.GA_COM_TPOPS_FEATURES SET SUPPORT = TRUE WHERE FEATUREID = ( SELECT FEATUREID FROM GA_COM_FILTER_FEATURES WHERE FEATURENAME = 'OPENAPI_XBSA_BACKUP_RESTORE');</pre> <p></p></li>
     <li><span>Run the following command to check whether the corresponding database table is successfully updated:</span><p></p><pre class="screen">SELECT * FROM PUBLIC.GA_COM_TPOPS_FEATURES WHERE FEATUREID = ( SELECT FEATUREID FROM GA_COM_FILTER_FEATURES WHERE FEATURENAME = 'OPENAPI_XBSA_BACKUP_RESTORE');</pre> <p>The following shows a command output example (if the value of <strong>support</strong> is <strong>t</strong>, the whitelist is enabled successfully):</p> <p><span><img src="en-us_image_0000001792346066.png"></span></p> <p></p></li>
     <li><span>If the whitelist fails to be enabled in the preceding steps, run the following command to restore the whitelist status:</span><p></p><pre class="screen">UPDATE PUBLIC.GA_COM_TPOPS_FEATURES SET SUPPORT = FALSE WHERE FEATUREID = ( SELECT FEATUREID FROM GA_COM_FILTER_FEATURES WHERE FEATURENAME = 'OPENAPI_XBSA_BACKUP_RESTORE');</pre> <p></p></li>
     <li id="TPOPS_GaussDB_00013__li471716181933"><span>After the XBSA backup whitelist is modified, log out of the TPOPS management page, and log in again for the configuration to take effect.</span></li>
    </ol>
    <p><strong>Enable the XBSA PITR Whitelist</strong> <strong>(Applicable to Log Backup)</strong></p>
    <ol>
     <li><span>Use an SSH tool to log in to the active TPOPS server node as user <strong>root</strong> through the management IP address of the node.</span></li>
     <li><span>On the TPOPS node, run the following command to log in to the TPOPS node as user <strong>dmuser</strong>:</span><p></p><pre class="screen">su - dmuser</pre> <p></p></li>
     <li><span>Run the following commands to connect to the TPOPS metadata database:</span><p></p><pre class="screen">source $HOME/.dmbashrc; 
source $DM_HOME/primdb/.infostore; 
gsql -d primdb -p 22202 -r -U apprim -W <em>dmuser password</em></pre> <p></p></li>
     <li><span>Run the following command to enable the TPOPS_PITR feature:</span><p></p><pre class="screen">UPDATE PUBLIC.GA_COM_TPOPS_FEATURES SET SUPPORT = TRUE WHERE FEATUREID = ( SELECT FEATUREID FROM GA_COM_FILTER_FEATURES WHERE FEATURENAME = 'TPOPS_PITR');</pre> <p></p></li>
     <li><span>Run the following command to check whether the corresponding database table is successfully updated:</span><p></p><pre class="screen">SELECT * FROM PUBLIC.GA_COM_TPOPS_FEATURES WHERE FEATUREID = ( SELECT FEATUREID FROM GA_COM_FILTER_FEATURES WHERE FEATURENAME = 'TPOPS_PITR');</pre> <p>The following shows a command output example (if the value of <strong>support</strong> is <strong>t</strong>, the whitelist is enabled successfully):</p> <p><span><img src="en-us_image_0000001976838126.png"></span></p> <p></p></li>
     <li><span>If the whitelist fails to be enabled in the preceding steps, run the following command to restore the whitelist status:</span><p></p><pre class="screen">UPDATE PUBLIC.GA_COM_TPOPS_FEATURES SET SUPPORT = FALSE WHERE FEATUREID = ( SELECT FEATUREID FROM GA_COM_FILTER_FEATURES WHERE FEATURENAME = 'TPOPS_PITR');</pre> <p></p></li>
     <li><span>After the XBSA backup whitelist is modified, log out of the TPOPS management page, and log in again for the configuration to take effect.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="TPOPS_GaussDB_00010.html">Backing Up GaussDB Instances</a>
    </div>
   </div>
  </div>
 </body>
</html>