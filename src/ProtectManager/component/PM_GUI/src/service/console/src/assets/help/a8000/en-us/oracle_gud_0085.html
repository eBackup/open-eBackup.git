<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing Databases">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="oracle_gud_0083.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="oracle_gud_0085">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing Databases</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="oracle_gud_0085"></a><a name="oracle_gud_0085"></a>

<h1 class="topictitle1">Managing Databases</h1>
<div><p>The <span>OceanProtect</span> allows you to modify, remove, activate, or disable protection for databases.</p>
<div class="section"><h4 class="sectiontitle">Related Operations</h4><p>You need to log in to the management page, choose <span class="uicontrol"><b><span id="oracle_gud_0085__en-us_topic_0000001666345505_text63901566195"><strong>Protection</strong></span> &gt; Databases &gt; Oracle</b></span>, click the <span class="uicontrol"><b><span><strong>Databases</strong></span></b></span> tab, and find the database on which you want to perform related operations.</p>
</div>
<p><a href="#oracle_gud_0085__table24934294919">Table 1</a> describes the related operations.</p>

<div class="tablenoborder"><a name="oracle_gud_0085__table24934294919"></a><a name="table24934294919"></a><table cellpadding="4" cellspacing="0" summary="" id="oracle_gud_0085__table24934294919" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Database protection operations</caption><colgroup><col style="width:14.360000000000001%"><col style="width:57.91%"><col style="width:27.73%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="14.360000000000003%" id="mcps1.3.4.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="57.910000000000004%" id="mcps1.3.4.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="27.730000000000004%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>When you need to reselect the SLA associated with a database and modify the execution scripts before and after database protection, you can modify the protection plan. After the modification, the next backup will be executed based on the new protection plan.</p>
<p><strong>Note</strong></p>
<p>The modification does not affect the protection job that is being executed. The modified protection plan will take effect in the next protection period.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span> in the row where the database is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Removing protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>If you do not need to protect a database, you can remove the protection from the database. After the removal, the system cannot execute protection jobs for the database. To protect the database again, you need to associate an SLA with the database again.</p>
<p><strong>Note</strong></p>
<p>When a protection job of the database is running, the protection cannot be removed.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span> in the row where the database is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Manual backup</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to perform a backup immediately.</p>
<p><strong>Note</strong></p>
<p>Copies generated by manual backup are retained for the duration defined in the SLA.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span> in the row where the database is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Disabling protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to disable a protection plan when periodic protection on the database is not required. After the protection plan is disabled, the system does not execute the automatic backup job of the database.</p>
<p><strong>Note</strong></p>
<p>Disabling protection does not affect ongoing backup jobs.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span> in the row where the database is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Activating protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to activate a disabled protection plan for a database. After the protection plan is activated, the system backs up the database based on the protection plan.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span> in the row where the database is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Scanning Resources</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>If the information about a registered database changes, for example, the database version changes, you can perform this operation to scan the database and update the information. Otherwise, the execution of backup and recovery jobs may be affected.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span> in the row where the database resides.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Testing connectivity</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to test the connectivity between the <span>OceanProtect</span> and the database.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Test Connectivity</strong></span></b></span> in the row where the database resides.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying database information</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to modify information such as the database name and database authentication mode.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span> in the row where the database resides.</p>
<p></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Deleting a database</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to delete a database that is no longer used.</p>
<p><strong>Note</strong></p>
<p>Before deleting a database, remove protection from the database.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span> in the row where the database resides.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="oracle_gud_0085__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="oracle_gud_0085__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="oracle_gud_0085__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="oracle_gud_0085__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="oracle_gud_0085__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="oracle_gud_0085__en-us_topic_0000001792344090_ul22969251338"><li id="oracle_gud_0085__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="oracle_gud_0085__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="oracle_gud_0085__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="oracle_gud_0085__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="oracle_gud_0085__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="oracle_gud_0085__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="oracle_gud_0085__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="oracle_gud_0085__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="oracle_gud_0085__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="oracle_gud_0085__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="oracle_gud_0085__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.4.2.4.1.1 mcps1.3.4.2.4.1.2 mcps1.3.4.2.4.1.3 "><p>Note: The procedure for restoring Oracle data is the same as that described in <a href="oracle_gud_0055.html">Restoration</a>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="oracle_gud_0083.html">Oracle Database Environment</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>