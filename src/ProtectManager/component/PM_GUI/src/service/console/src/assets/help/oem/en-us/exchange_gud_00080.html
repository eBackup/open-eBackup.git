<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Exchange Single-Node Systems or Availability Groups">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="exchange_gud_00076.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="exchange_gud_00080">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Exchange Single-Node Systems or Availability Groups</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="exchange_gud_00080"></a><a name="exchange_gud_00080"></a>
  <h1 class="topictitle1">Managing Exchange Single-Node Systems or Availability Groups</h1>
  <div>
   <p>You can add, remove, activate, or disable protection for Exchange single-node systems or availability groups on the <span>product</span>. You can also perform operations such as manual backup, resource scanning, modification, and deletion.</p>
   <p>You need to log in to the <span>product</span>, choose <span class="uicontrol"><b><span id="exchange_gud_00080__en-us_topic_0000001839142377_text8644926538"><strong>Protection</strong></span> &gt; <span id="exchange_gud_00080__en-us_topic_0000001839142377_text1310114196546"><strong>Applications</strong></span> &gt; <span id="exchange_gud_00080__en-us_topic_0000001839142377_text2114145125312"><strong>Exchange</strong></span></b></span>, click the <span class="uicontrol"><b>Single-Node Systems/Availability Groups</b></span> tab, and locate the target single-node system or availability group for management.</p>
   <p><a href="#exchange_gud_00080__en-us_topic_0000001506266501_en-us_topic_0000001263933970_table24934294919">Table 1</a> describes the related operations.</p>
   <div class="tablenoborder">
    <a name="exchange_gud_00080__en-us_topic_0000001506266501_en-us_topic_0000001263933970_table24934294919"></a><a name="en-us_topic_0000001506266501_en-us_topic_0000001263933970_table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="exchange_gud_00080__en-us_topic_0000001506266501_en-us_topic_0000001263933970_table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Operations related to Exchange single-node systems/availability groups
     </caption>
     <colgroup>
      <col style="width:18%">
      <col style="width:54%">
      <col style="width:28.000000000000004%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="18%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="54%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="28.000000000000004%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying protection</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can modify a protection plan to associate another SLA with a single-node system/availability group or modify a user-defined script. After the modification, the next backup will be executed based on the new protection plan.</p> <p><strong>Note</strong></p>
        <ul>
         <li>The modification does not affect the protection job that is being executed. The new protection plan will take effect in the next protection period.</li>
         <li>If the SLA associated with a resource changes, you are advised to modify the current SLA on the <span class="uicontrol"><b><span>Protection</span> &gt; <span>SLAs</span></b></span> page. You are advised not to associate the resource with another SLA by modifying protection.<p>When you associate the resource with another SLA by modifying protection, the license capacity may be deducted repeatedly if the copy of the original SLA still exists.</p></li>
        </ul></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Removing protection</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can remove the protection plan if a single-node system/availability group no longer needs to be protected. After the removal, the system cannot execute protection jobs for the single-node system/availability group.</p> <p><strong>Note</strong></p>
        <ul>
         <li>Protection cannot be removed when a protection job of the single-node system/availability group is running.</li>
         <li>If the SLA associated with a resource changes, you are advised to modify the current SLA on the <span class="uicontrol"><b><span>Protection</span> &gt; <span>SLAs</span></b></span> page. You are advised not to associate the resource with another SLA after removing protection.<p>When you associate the resource with another SLA after removing protection, the license capacity may be deducted repeatedly if the copy of the original SLA still exists.</p></li>
        </ul></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Activating protection</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to activate a disabled protection plan for a single-node system/availability group. After the protection plan is activated, the system backs up the single-node system/availability group based on the protection plan.</p> <p><strong>Note</strong></p> <p>You can perform this operation only when the single-node system/availability group is in the <span class="uicontrol"><b>Inactivated</b></span> state.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Disabling protection</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to disable the protection plan of a single-node system/availability group when its periodic protection is not needed. After the protection plan is disabled, the system does not execute the automatic backup job of the single-node system/availability group.</p> <p><strong>Note</strong></p> <p>Disabling protection does not affect ongoing backup jobs.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Performing a manual backup</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Scanning resources</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can scan databases or mailboxes below a registered single-node system/availability group and display them on the <strong>Databases</strong> or <strong>Mailboxes</strong> tab page as objects to be protected.</p> <p><strong>Note</strong></p> <p>No scanning result is displayed when a single-node system/availability group is offline or you do not have the permission.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Connectivity test</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to test the connectivity between the <span>product</span> and resources to be protected.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Test Connectivity</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying a single-node system/availability group</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can refer to this section to modify the name and host node information of a single-node system or availability group.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 "><p>Deleting a single-node system/availability group</p></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>If you no longer need to protect databases of a single-node system or availability group, you can delete the single-node system or availability group. After the deletion, related information cannot be viewed on the <span>product</span>.</p> <p><strong>Note</strong></p> <p>If a database or mailbox below a single-node system or availability group has been associated with an SLA, the single-node system or availability group cannot be deleted.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span> in the row where the single-node system/availability group resides.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Adding a tag
         <div class="note" id="exchange_gud_00080__en-us_topic_0000001792344090_note161679211561">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="exchange_gud_00080__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="exchange_gud_00080__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol" id="exchange_gud_00080__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="exchange_gud_00080__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note" id="exchange_gud_00080__en-us_topic_0000001792344090_note164681316118">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul id="exchange_gud_00080__en-us_topic_0000001792344090_ul22969251338">
            <li id="exchange_gud_00080__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="exchange_gud_00080__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="exchange_gud_00080__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
            <li id="exchange_gud_00080__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="exchange_gud_00080__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="exchange_gud_00080__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="exchange_gud_00080__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="18%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Removing a tag
         <div class="note" id="exchange_gud_00080__en-us_topic_0000001792344090_note4957114117575">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="exchange_gud_00080__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="54%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="exchange_gud_00080__en-us_topic_0000001792344090_b472911291311">Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p></td>
       <td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="exchange_gud_00080__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="exchange_gud_00080__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.4.2.4.1.1 mcps1.3.4.2.4.1.2 mcps1.3.4.2.4.1.3 "><p>Note: The procedure for restoring Exchange resources is the same as that described in <a href="exchange_gud_00057.html">Recovery</a>.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="exchange_gud_00076.html">Managing Exchange</a>
    </div>
   </div>
  </div>
 </body>
</html>