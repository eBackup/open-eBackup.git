<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Viewing TiDB Copy Information">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="TiDB_00054.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="TiDB_00055">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Viewing TiDB Copy Information</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="TiDB_00055"></a><a name="TiDB_00055"></a>

<h1 class="topictitle1">Viewing TiDB Copy Information</h1>
<div><div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="TiDB_00055__tidb_00040_en-us_topic_0000001839142377_uicontrol548619473589"><b><span id="TiDB_00055__tidb_00040_en-us_topic_0000001839142377_text44861447115812"><strong>Explore</strong></span> &gt; <span id="TiDB_00055__tidb_00040_en-us_topic_0000001839142377_text2048614765813"><strong>Copy Data</strong></span> &gt; <span id="TiDB_00055__tidb_00040_en-us_topic_0000001839142377_text163101122194918"><strong>Databases</strong></span> &gt; TiDB</b></span>.</span></li><li><span>Select a copy viewing mode to view the copy information.</span><p><div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="TiDB_00055__en-us_topic_0000001792503826_p189141741162012">Replication copies can be viewed only in the target cluster.</p>
</div></div>
<ul><li>Click the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab to view information about TiDB backup resource copies by resource. If a table in the TiDB cluster is damaged or lost and you must restore the TiDB table, you are advised to click this tab and search for the copy at the specific time point.<p><a href="#TiDB_00055__table20406195873110">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="TiDB_00055__table20406195873110"></a><a name="table20406195873110"></a><table cellpadding="4" cellspacing="0" summary="" id="TiDB_00055__table20406195873110" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Copy parameters</caption><colgroup><col style="width:7.5200000000000005%"><col style="width:24.84%"><col style="width:67.64%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.1.2.2.2.2.1.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.1.2.2.2.2.1.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" rowspan="4" valign="top" width="7.5200000000000005%" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Resources</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="24.84%" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.64%" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.2 "><p>Name of a backup resource.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><strong>Type</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p>Type of the backup resource.</p>
<ul><li>TiDB cluster</li><li>TiDB database</li><li>TiDB table set</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p>Name of the protected TiDB cluster node.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p>Whether the backup resource still exists in the current cluster.</p>
<ul><li>If yes, <span><strong>Status</strong></span> is <span><strong>Existing</strong></span>.</li><li>If no, <span><strong>Status</strong></span> is <span><strong>Not existing</strong></span>.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="3" valign="top" width="7.5200000000000005%" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Copies</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="24.84%" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Quantity</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.64%" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.2 "><p>Total number of copies generated based on the backup resource. The number of replication copies is displayed only in the target cluster.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Replicated Copy SLA</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p>Archive SLA associated with a replication copy.</p>
<p>If an archive SLA has been configured for the replication copy generated by the resource, the SLA name is displayed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Replicated Copy Protection Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.1.3.2.4.1.1 "><div class="p">Whether a replication copy is protected. <ul id="TiDB_00055__en-us_topic_0000001792503826_ul161172118317"><li id="TiDB_00055__en-us_topic_0000001792503826_li11611621173118"><span id="TiDB_00055__en-us_topic_0000001792503826_text16611162123119"><strong>Unprotected</strong></span>: The replication copy is not associated with an SLA, or the replication copy is associated with an SLA but protection is disabled.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li761111211312"><span id="TiDB_00055__en-us_topic_0000001792503826_text161192116317"><strong>Protected</strong></span>: The replication copy is associated with an SLA and protection is enabled.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li2088261163715"><span id="TiDB_00055__en-us_topic_0000001792503826_text12602914182118"><strong>Creating</strong></span>: The replication copy is associated with an SLA and protection is being created. The status is available only in 1.5.0 and earlier versions.</li></ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Click the <span class="uicontrol"><b>Copy</b></span> tab to view information about all copies by TiDB backup resource copy. When restoring data by using a specified copy, you are advised to click this icon and search for the copy.<p><a href="#TiDB_00055__en-us_topic_0000001142182746_table1390718405171">Table 2</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="TiDB_00055__en-us_topic_0000001142182746_table1390718405171"></a><a name="en-us_topic_0000001142182746_table1390718405171"></a><table cellpadding="4" cellspacing="0" summary="" id="TiDB_00055__en-us_topic_0000001142182746_table1390718405171" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Copy parameters</caption><colgroup><col style="width:7.109999999999999%"><col style="width:19.139999999999997%"><col style="width:73.75%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.1.2.2.2.2.2.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.1.2.2.2.2.2.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" rowspan="9" valign="top" width="7.109999999999999%" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copies</strong></span></p>
<p></p>
</td>
<td class="cellrowborder" valign="top" width="19.139999999999997%" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Backup Time</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="73.75%" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.2 "><p>Time when a backup copy is generated.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copy Time</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Time when a copy is generated. </p>
<p>Click <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol1250926125014"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text149471616195614"><strong>Copy Time</strong></span></b></span> to view the expiration time and other information of the copy. If <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol945613251346"><b>--</b></span> is displayed as the expiration time, the copy is retained permanently.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p">Copy status. <ul id="TiDB_00055__en-us_topic_0000001792503826_ul465416347216"><li id="TiDB_00055__en-us_topic_0000001792503826_li1171923612110"><span id="TiDB_00055__en-us_topic_0000001792503826_text24696268516"><strong>Normal</strong></span>: The copy is available.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li6520237152112"><strong id="TiDB_00055__en-us_topic_0000001792503826_b8350164295711">Invalid</strong>: The copy is invalid.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li15384133802118"><span id="TiDB_00055__en-us_topic_0000001792503826_text187219382512"><strong>Deleting</strong></span>: The copy is being deleted.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li875512537417"><span id="TiDB_00055__en-us_topic_0000001792503826_text11749005192"><strong>Restoring</strong></span>: The copy is being restored.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li16555142220324"><span id="TiDB_00055__en-us_topic_0000001792503826_text166661656203220"><strong>Deletion failed</strong></span>: The copy fails to be deleted.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Location of a copy.</p>
<ul><li id="TiDB_00055__en-us_topic_0000001792503826_li18158830181812">For 1.5.0: If <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol62681417105816"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text1836720425583"><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol1290644910582"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text104141920175915"><strong>Backup</strong></span></b></span>, <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol331112516211"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text138114231936"><strong>Cascaded Replication</strong></span></b></span>, <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol119361459832"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text136013196412"><strong>Reverse Replication</strong></span></b></span>, or <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol18347931195912"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text1347085435917"><strong>Replicate</strong></span></b></span>, the copy is stored in the local cluster. In this case, <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol2015813303185"><b>Local</b></span> is displayed.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li123471448182018">For 1.6.0 and later versions, if <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol42507122115"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text16256772112"><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol025178216"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text1925167162117"><strong>Backup</strong></span></b></span>, <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol1226147142113"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text15261078218"><strong>Cascaded Replication</strong></span></b></span>, <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol192607132111"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text226177102110"><strong>Reverse Replication</strong></span></b></span>, or <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol19261571215"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text132697182117"><strong>Replicate</strong></span></b></span>, the copy is stored in the corresponding backup storage unit. In this case, the name of the corresponding backup storage unit is displayed.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li1473912420172">If <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol20454181902"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text945418114019"><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol1943016186013"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text2035114288014"><strong>Object Storage Archive</strong></span></b></span> or <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol123221341402"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text98939521909"><strong>Import</strong></span></b></span>, the copy is stored in an object storage bucket. In this case, the bucket name is displayed.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li154391755171713">If <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol12512936015"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text651210311015"><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol" id="TiDB_00055__en-us_topic_0000001792503826_uicontrol58800591007"><b><span id="TiDB_00055__en-us_topic_0000001792503826_text128561869115"><strong>Tape Archive</strong></span></b></span>, the copy is stored in a tape library. In this case, the media set name is displayed.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Generation Mode</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p">Generation mode of a copy.<ul><li><span id="TiDB_00055__en-us_topic_0000001792503826_text109638183455"><strong>Backup</strong></span>: copies generated by a backup job.</li><li><span id="TiDB_00055__en-us_topic_0000001792503826_text124238155813"><strong>Replication</strong></span>: copies generated by a replication job.</li><li><span id="TiDB_00055__en-us_topic_0000001792503826_text171691481756"><strong>Object Storage Archive</strong></span>: copies generated by an archiving job (object storage as the archive storage).</li><li><span id="TiDB_00055__en-us_topic_0000001792503826_text1762165620514"><strong>Tape Archive</strong></span>: copies generated by an archiving job (tape library as the archive storage).</li><li><span id="TiDB_00055__en-us_topic_0000001792503826_text1953520228352"><strong>Cascaded Replication</strong></span>: copies generated by a cascaded replication job.</li><li><span id="TiDB_00055__en-us_topic_0000001792503826_text1942253020356"><strong>Reverse Replication</strong></span>: copies generated by a reverse replication job.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copy Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Backup type of a generated copy.</p>
<p>The options are <strong>Full</strong> and <strong>Log</strong>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p"><strong id="TiDB_00055__fc_gud_0058_0_en-us_topic_0000001792503826_b916692512">Expired At</strong><div class="note" id="TiDB_00055__fc_gud_0058_0_en-us_topic_0000001792503826_note130943715514"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="TiDB_00055__fc_gud_0058_0_en-us_topic_0000001792503826_en-us_topic_0000001656771385_p108956094911">This parameter is available only in 1.5.0SPC26 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Copy expiration time. If a copy is permanently valid, <strong id="TiDB_00055__fc_gud_0058_0_en-us_topic_0000001792503826_b19549142216517">--</strong> is displayed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span id="TiDB_00055__en-us_topic_0000001792503826_text68691158124911"><strong>WORM</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p">Whether a copy has been set as a WORM copy. <ul id="TiDB_00055__en-us_topic_0000001792503826_ul75853135118"><li id="TiDB_00055__en-us_topic_0000001792503826_li458531175118"><span><img id="TiDB_00055__en-us_topic_0000001792503826_image18812012104311" src="en-us_image_0000001792344150.png"></span>: The copy is not set as a WORM copy, that is, the copy is a common copy.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li994353785116"><span><img id="TiDB_00055__en-us_topic_0000001792503826_image05089185413" src="en-us_image_0000001792344146.png"></span>: The copy has been set as a WORM copy. The WORM copy cannot be deleted and its retention period cannot be shortened.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li373211419517"><span><img id="TiDB_00055__en-us_topic_0000001792503826_image155001184515" src="en-us_image_0000001839143265.png"></span>: The copy is being set as a WORM copy. A copy in this state cannot be deleted and the retention period cannot be modified.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li444514572513"><span><img id="TiDB_00055__en-us_topic_0000001792503826_image17671746527" src="en-us_image_0000001839223221.png"></span>: The copy fails to be set as a WORM copy. A copy that fails to be set as a WORM copy cannot be deleted and the retention period cannot be shortened.</li><li id="TiDB_00055__en-us_topic_0000001792503826_li216064774817"><span><img id="TiDB_00055__en-us_topic_0000001792503826_image9564104794819" src="en-us_image_0000002090948029.png"></span>: The copy has expired. This state is available only in 1.6.0 and later versions. It indicates that the WORM copy has expired. Copies in this state can be deleted.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p"><strong id="TiDB_00055__en-us_topic_0000001792503826_b1952114112010">WORM Expiration Time</strong><div class="note" id="TiDB_00055__en-us_topic_0000001792503826_note866011102212"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="TiDB_00055__en-us_topic_0000001792503826_en-us_topic_0000001792344110_p630385412417">This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p">If a copy is a WORM copy, the WORM expiration time is displayed. Otherwise, <strong id="TiDB_00055__en-us_topic_0000001792503826_b1914333131317">--</strong> is displayed.<div class="note" id="TiDB_00055__en-us_topic_0000001792503826_note173611252434"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="TiDB_00055__en-us_topic_0000001792503826_p10361192510430">The WORM expiration time is based on the WORM file system compliance clock. If the WORM file system compliance clock time is inconsistent with the system time, check the WORM status after the WORM file system compliance clock reaches the set expiration time.</p>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="5" valign="top" width="7.109999999999999%" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Resources</strong></span></p>
<p></p>
</td>
<td class="cellrowborder" valign="top" width="19.139999999999997%" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="73.75%" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.2 "><p>Name of a backup resource.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><strong>Type</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Type of the backup resource to which the copy belongs.</p>
<ul><li>TiDB cluster</li><li>TiDB database</li><li>TiDB table set</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Name of the protected TiDB cluster node.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>Whether the backup resource still exists in the current cluster.</p>
<ul><li>If yes, <span><strong>Status</strong></span> is <span><strong>Existing</strong></span>.</li><li>If no, <span><strong>Status</strong></span> is <span><strong>Not existing</strong></span>.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><div class="p"><strong id="TiDB_00055__en-us_topic_0000001792503826_b347812294118">Tag</strong><div class="note" id="TiDB_00055__en-us_topic_0000001792503826_note1246801219248"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="TiDB_00055__en-us_topic_0000001792503826_en-us_topic_0000001792344110_p630385412417_1">This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.1.2.2.2.2.2.3.2.4.1.1 "><p>If a tag has been added for the resource, the tag name is displayed. Otherwise, <strong id="TiDB_00055__en-us_topic_0000001792344110_b1959642011219">--</strong> is displayed.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li></ul>
</p></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="TiDB_00054.html">Copies</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>