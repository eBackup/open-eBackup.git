<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 2: Registering an openGauss Cluster">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="opengauss-0111.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="opengauss-0112">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 2: Registering an openGauss Cluster</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="opengauss-0112"></a><a name="opengauss-0112"></a>
  <h1 class="topictitle1">Step 2: Registering an openGauss Cluster</h1>
  <div>
   <p>Before backing up and restoring an openGauss database or instance, the system automatically scans for the openGauss database and instance. You do not need to manually register the openGauss cluster with the <span>product</span>.</p>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <p>Before registering an openGauss cluster, install ProtectAgent on all nodes in the openGauss cluster. For details, see the <i><cite>ProtectAgent Installation Guide</cite></i>.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="opengauss-0112__en-us_topic_0000001839142377_uicontrol3409635183613"><b><span id="opengauss-0112__en-us_topic_0000001839142377_text197344403116"><strong>Protection</strong></span> &gt; Databases &gt; openGauss</b></span>.</span></li>
     <li><span>Click the <span class="uicontrol"><b><span><strong>Clusters</strong></span></b></span> tab.</span></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Register</strong></span></b></span> to register an openGauss cluster.</span></li>
     <li><span>Configure the cluster authentication information.</span><p></p><p><a href="#opengauss-0112__en-us_topic_0000001311214069_table241515964115">Table 1</a> describes the related parameters.</p>
      <div class="tablenoborder">
       <a name="opengauss-0112__en-us_topic_0000001311214069_table241515964115"></a><a name="en-us_topic_0000001311214069_table241515964115"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="opengauss-0112__en-us_topic_0000001311214069_table241515964115" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>openGauss cluster registration information
        </caption>
        <colgroup>
         <col style="width:25.629999999999995%">
         <col style="width:74.37%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="25.629999999999995%" id="mcps1.3.3.2.4.2.2.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="74.37%" id="mcps1.3.3.2.4.2.2.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>Custom instance name.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Username</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>Name of the OS user who runs the database.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Type</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>The cluster type can be <strong><span><strong>Single Server</strong></span></strong> or <strong><span><strong>Active/Standby Replication</strong></span></strong>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Environment Variable File Path</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>During database installation, this path must be specified if separated deployment is used.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p id="opengauss-0112__en-us_topic_0000001607543334_p4275815161710">This path is the environment configuration file path set by the user during separated deployment.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Nodes</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>Host where the openGauss instance resides.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>The selected nodes must have the openGauss database installed and belong to the same cluster.</p>
            </div>
           </div></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li>
    </ol>
    <div class="note">
     <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
     <div class="notebody">
      <ul>
       <li>Once the cluster is registered, the system automatically scans it every 5 minutes to gather instance and database information.</li>
      </ul>
      <ul>
       <li>You can also manually scan the cluster. On the <span class="uicontrol"><b><span><strong>Clusters</strong></span></b></span> tab page, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span> in the row of the target cluster to obtain instance and database information.</li>
      </ul>
     </div>
    </div>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="opengauss-0111.html">Backing Up openGauss</a>
    </div>
   </div>
  </div>
 </body>
</html>