<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Live Mounting a NAS File System">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0066">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Live Mounting a NAS File System</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0066"></a><a name="nas_s_0066"></a>

<h1 class="topictitle1">Live Mounting a NAS File System</h1>
<div><p>This section describes how to live mount a copy of a NAS file system to a target client for quick use.</p>
<div class="section" id="nas_s_0066__section2085213010239"><h4 class="sectiontitle">Context</h4><ul id="nas_s_0066__ul147910567512"><li id="nas_s_0066__li19707011103218"><span class="uicontrol" id="nas_s_0066__uicontrol1265863119342"><b>Mount Level</b></span> indicates the mount level of the current backup copy. The initial value of <span class="uicontrol" id="nas_s_0066__uicontrol18145122119307"><b>Mount Level</b></span> for a copy is <strong id="nas_s_0066__b6868182044416">1</strong>.</li><li id="nas_s_0066__li888222519329">If you select a copy whose <span class="uicontrol" id="nas_s_0066__uicontrol69991637153412"><b>Mount Level</b></span> is <strong id="nas_s_0066__b15217101162315">2</strong> for mounting, you need to manually mount it to the target client based on the share path and share IP address. For details, see <a href="nas_s_0108.html">Manually Mounting a NAS File System or NAS Share</a>.</li><li id="nas_s_0066__li127865918467">Backup copies and replication copies can be used for live mount.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="nas_s_0066__en-us_topic_0000001839142377_uicontrol916319276489"><b><span id="nas_s_0066__en-us_topic_0000001839142377_text1016315279483"><strong>Explore</strong></span> &gt; <span id="nas_s_0066__en-us_topic_0000001839142377_text6163122724816"><strong>Live Mount</strong></span> &gt; <span id="nas_s_0066__en-us_topic_0000001839142377_text16896192718403"><strong>NAS File Systems</strong></span></b></span>.</span></li><li id="nas_s_0066__li19268104183219"><span>Click <span class="uicontrol" id="nas_s_0066__uicontrol84017217520"><b><span id="nas_s_0066__text1670816112216"><strong>Create</strong></span></b></span>.</span></li><li><span>On the <span class="uicontrol"><b><span><strong>Select Resource</strong></span></b></span> tab page, select the source file system corresponding to a copy to be mounted, and click <span class="uicontrol"><b><span><strong>Next</strong></span></b></span>.</span></li><li id="nas_s_0066__li18161008334"><span>On the <span class="uicontrol" id="nas_s_0066__uicontrol3329357651"><b><span id="nas_s_0066__text33298579515"><strong>Select Copy</strong></span></b></span> tab page, select the copy to be mounted and click <span class="uicontrol" id="nas_s_0066__uicontrol3890932596"><b><span id="nas_s_0066__text889093220913"><strong>Next</strong></span></b></span>.</span><p><p id="nas_s_0066__p191441659204112">The copy to be live mounted must be in the <span class="uicontrol" id="nas_s_0066__uicontrol1584841912513"><b><span id="nas_s_0066__text10369637192511"><strong>Normal</strong></span></b></span> status.</p>
</p></li><li id="nas_s_0066__li14333597332"><span>On the <span class="uicontrol" id="nas_s_0066__uicontrol8951503336"><b><span id="nas_s_0066__text159570103313"><strong>Mount Options</strong></span></b></span> tab page, configure mount parameters, and click <span class="uicontrol" id="nas_s_0066__uicontrol015073516915"><b><span id="nas_s_0066__text1415010354910"><strong>Next</strong></span></b></span>.</span><p><ol type="a" id="nas_s_0066__ol99770103312"><li id="nas_s_0066__li1897140203316">Configure basic and advanced parameters for live mount.<div class="p" id="nas_s_0066__p1096180103318"><a name="nas_s_0066__li1897140203316"></a><a name="li1897140203316"></a><a href="#nas_s_0066__table89615043315">Table 1</a> and <a href="#nas_s_0066__table4872165714509">Table 2</a> describe the related parameters.<div class="note" id="nas_s_0066__note1795606331"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="nas_s_0066__ul69520163319"><li id="nas_s_0066__li8951207331">You can set either or both of <strong id="nas_s_0066__b1262311915109">Bandwidth (MB/s)</strong> and <strong id="nas_s_0066__b2624519151020">Standard IOPS (8 KB)</strong>. You are advised to set only <strong id="nas_s_0066__b06241619161014">Bandwidth (MB/s)</strong> for bandwidth-intensive services and only <strong id="nas_s_0066__b1862531931017"><strong id="nas_s_0066__b16251192108">Standard</strong> IOPS (8 KB)</strong> for IOPS-intensive services. If you cannot determine the service type or the service type changes frequently, you can set both indicators and the system will control the traffic using the indicator with a smaller value.</li><li id="nas_s_0066__li129517043311">You can query the conversion between the standard IOPS (8 KB) model and real I/O model in the conversion table on the GUI.</li><li id="nas_s_0066__li2027466144713">The maximum traffic of mounted copies is limited to ensure the performance of backup and restoration jobs. In addition, the minimum traffic of mounted copies is set to ensure the minimum performance of mounted copies.</li></ul>
</div></div>

<div class="tablenoborder"><a name="nas_s_0066__table89615043315"></a><a name="table89615043315"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0066__table89615043315" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Basic parameters for live mount</caption><colgroup><col style="width:16.439999999999998%"><col style="width:20.75%"><col style="width:62.81%"></colgroup><thead align="left"><tr id="nas_s_0066__row2095140183312"><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1"><p id="nas_s_0066__p5951007337">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.2"><p id="nas_s_0066__p129580193310">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="nas_s_0066__row187197124910"><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p675725818232"><span id="nas_s_0066__text10898114912720"><strong>File system name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.2 "><p id="nas_s_0066__p18258120132220">Name of a mounted file system.</p>
<p id="nas_s_0066__p1775418582233">The default name prefix is <strong id="nas_s_0066__b5302133724013">mount_</strong>.</p>
</td>
</tr>
<tr id="nas_s_0066__row1323136134919"><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p1401338152614"><span id="nas_s_0066__text12352114652611"><strong>Share Protocol</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.2 "><p id="nas_s_0066__p674685813234">NFS or CIFS.</p>
<p id="nas_s_0066__p497911015212">You can configure NFS and CIFS shares at the same time.</p>
</td>
</tr>
<tr id="nas_s_0066__row1620716414917"><td class="cellrowborder" rowspan="2" valign="top" width="16.439999999999998%" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p15809142154820"><strong id="nas_s_0066__b159901142105512">NFS</strong></p>
</td>
<td class="cellrowborder" valign="top" width="20.75%" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p10303174614247"><span id="nas_s_0066__text355033017227"><strong>Share Path</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.81%" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.2 "><p id="nas_s_0066__p91182035132319">Share path for accessing the file system. The path name is the same as the file system name and cannot be changed.</p>
<p id="nas_s_0066__p9465834103015">Ensure that the backup resource supports the NFS share protocol. Otherwise, the NFS file permission information will not be contained during mounting.</p>
</td>
</tr>
<tr id="nas_s_0066__row45761098516"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p29861355142418"><strong id="nas_s_0066__b1690423195515">Share</strong> <span id="nas_s_0066__text1372627112810"><strong>Client</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><div class="p" id="nas_s_0066__p75836231289">Information about the client that accesses a file system.<ul id="nas_s_0066__ul10853114216166"><li id="nas_s_0066__li141781311161">You can enter share client IP addresses, share client IP address segments, or an asterisk (*) to represent IP addresses of all share clients.</li><li id="nas_s_0066__li717813151615">The IP addresses can be IPv4 addresses, IPv6 addresses, or a combination of IPv4 and IPv6 addresses.</li><li id="nas_s_0066__li2178531151612">You can enter multiple IP addresses or IP address segments and separate them by semicolons (;), spaces, or <strong id="nas_s_0066__b3334203131920">Enter</strong>. Example: <strong id="nas_s_0066__b186852403458">192.168.0.10;192.168.0.0/24;*</strong></li></ul>
</div>
</td>
</tr>
<tr id="nas_s_0066__row55719408316"><td class="cellrowborder" rowspan="3" valign="top" width="16.439999999999998%" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p1540845794614"><strong id="nas_s_0066__b112081952125511">CIFS</strong></p>
<p id="nas_s_0066__p96609782220"></p>
</td>
<td class="cellrowborder" valign="top" width="20.75%" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p7737185193217"><span id="nas_s_0066__text573715563211"><strong>Share Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.81%" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.2 "><p id="nas_s_0066__p53284218538">Share name for accessing a file system.</p>
<p id="nas_s_0066__p1508191143516">The default share name is <strong id="nas_s_0066__b2312834122715">mount_</strong><em id="nas_s_0066__i1731833414275">Timestamp</em>. You can change it as needed.</p>
<p id="nas_s_0066__p1811116591216">Ensure that the backup resource supports the CIFS share protocol. Otherwise, the CIFS file permission information will not be contained during mounting.</p>
</td>
</tr>
<tr id="nas_s_0066__row1957204043111"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p1157294043118"><span id="nas_s_0066__text5328824173218"><strong>Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p11481041133414">Type of the user who accesses a CIFS share.</p>
<ul id="nas_s_0066__ul183616552810"><li id="nas_s_0066__nas_s_0100_li115301547103614"><span id="nas_s_0066__nas_s_0100_text11836124163210"><strong>Everyone</strong></span></li><li id="nas_s_0066__nas_s_0100_li1853094715369"><span id="nas_s_0066__nas_s_0100_text12625493210"><strong>Windows Local Authentication User</strong></span></li><li id="nas_s_0066__nas_s_0100_li58994343227"><span id="nas_s_0066__nas_s_0100_text1018841611228"><strong>Windows Local Authentication User Group</strong></span></li></ul>
</td>
</tr>
<tr id="nas_s_0066__row16660157182210"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p34401812193314"><span id="nas_s_0066__text15227123423313"><strong>Users</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.4.2.4.1.1 "><p id="nas_s_0066__p55721440183113">Username for accessing a CIFS share.</p>
<p id="nas_s_0066__p1441181810353">This parameter is mandatory when <strong id="nas_s_0066__nas_s_0100_b140357142311"><span id="nas_s_0066__nas_s_0100_text66251842152215"><strong>Type</strong></span></strong> is set to <strong id="nas_s_0066__nas_s_0100_b5567205814231"><span id="nas_s_0066__nas_s_0100_text1862524232215"><strong>Windows Local Authentication User</strong></span></strong> or <strong id="nas_s_0066__nas_s_0100_b14169117244"><span id="nas_s_0066__nas_s_0100_text462474214224"><strong>Windows Local Authentication User Group</strong></span></strong>.</p>
<p id="nas_s_0066__p7841427394">If the required user or user group does not exist, create one on DeviceManager.</p>
<ul id="nas_s_0066__ul18859115917267"><li id="nas_s_0066__nas_s_0100_li1134104615181">Perform the following operations for X series backup appliances:<ol id="nas_s_0066__nas_s_0100_ol6503987191"><li id="nas_s_0066__nas_s_0100_li1512122844010">Log in to the management page on another browser page.</li><li id="nas_s_0066__nas_s_0100_en-us_topic_0000001607531760_en-us_topic_0000001311295305_en-us_topic_0000001149078659_li929117916361">Choose <span class="uicontrol" id="nas_s_0066__nas_s_0100_uicontrol16830596269"><b>System</b></span> &gt; <strong id="nas_s_0066__nas_s_0100_b08017182461">Infrastructure</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b680151814615">Cluster Management</strong>.</li><li id="nas_s_0066__nas_s_0100_li19932939164117">On the <span class="uicontrol" id="nas_s_0066__nas_s_0100_uicontrol10974142710269"><b>Backup Clusters</b></span> tab page, click a node name under the <span class="uicontrol" id="nas_s_0066__nas_s_0100_uicontrol88011243202610"><b>Local Cluster Nodes</b></span> area.</li><li id="nas_s_0066__nas_s_0100_li1115217015589">In the <span class="uicontrol" id="nas_s_0066__nas_s_0100_uicontrol17351204276"><b>Node Details</b></span> dialog box that is displayed, click <span class="uicontrol" id="nas_s_0066__nas_s_0100_uicontrol11051616276"><b>Open the device management platform</b></span> to go to DeviceManager.</li><li id="nas_s_0066__nas_s_0100_li56039149190">Choose <strong id="nas_s_0066__nas_s_0100_b19121415125116">Services</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b1127231975119">File Service</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b10123182265110">Authentication Users</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b1321813267515">Windows Users</strong> from the navigation pane. On the <strong id="nas_s_0066__nas_s_0100_b374613855119">Local Authentication Users</strong> or <strong id="nas_s_0066__nas_s_0100_b1793194812289">Local Authentication User Groups</strong> tab page, create a user or user group.</li></ol>
</li><li id="nas_s_0066__nas_s_0100_li64527110138">For OceanProtect E1000, perform the following operations:<ol id="nas_s_0066__nas_s_0100_ol05910119136"><li id="nas_s_0066__nas_s_0100_li9591911151315">Log in to DeviceManager by referring to <a href="nas_s_dm.html">Logging In to DeviceManager</a>.</li><li id="nas_s_0066__nas_s_0100_li6526141413133">Choose <strong id="nas_s_0066__nas_s_0100_b834518383413">Resources</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b163460388413">Access</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b6346173854118">Authentication User</strong> &gt; <strong id="nas_s_0066__nas_s_0100_b13346173817411">Windows Users</strong> from the navigation pane. On the <strong id="nas_s_0066__nas_s_0100_b43468382413">Local Authentication User</strong> or <strong id="nas_s_0066__nas_s_0100_b1834610387414">Local Authentication User Group</strong> tab page, create a user or user group.</li></ol>
</li></ul>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<p id="nas_s_0066__p1432613576503"><a href="#nas_s_0066__table4872165714509">Table 2</a> describes the advanced parameters for live mount.</p>

<div class="tablenoborder"><a name="nas_s_0066__table4872165714509"></a><a name="table4872165714509"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0066__table4872165714509" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Advanced parameters for live mount</caption><colgroup><col style="width:16.439999999999998%"><col style="width:20.75%"><col style="width:62.81%"></colgroup><thead align="left"><tr id="nas_s_0066__row787245785012"><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.3.2.5.2.1.1.3.2.4.1.1"><p id="nas_s_0066__p1787235717507">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.3.2.5.2.1.1.3.2.4.1.2"><p id="nas_s_0066__p13872125712504">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="nas_s_0066__row787311575503"><td class="cellrowborder" rowspan="3" valign="top" width="16.439999999999998%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p1087385713509"><span id="nas_s_0066__text88738571506"><strong>Bandwidth (MB/s)</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="20.75%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p11873105712500"><span id="nas_s_0066__text128731557145011"><strong>Min</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.81%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.2 "><p id="nas_s_0066__p17874185785013">Minimum bandwidth for copy live mount.</p>
</td>
</tr>
<tr id="nas_s_0066__row78741057185015"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p14874057145013"><span id="nas_s_0066__text687465755012"><strong>Max</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p687414575501">Maximum bandwidth for copy live mount.</p>
</td>
</tr>
<tr id="nas_s_0066__row9874105755017"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p1487415719504"><span id="nas_s_0066__text887445715504"><strong>Burst</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p48749571502">Maximum burst bandwidth for copy live mount. The value of <strong id="nas_s_0066__b1535217211204"><span id="nas_s_0066__text1787565992219"><strong>Burst</strong></span></strong> must be greater than that of <strong id="nas_s_0066__b187610562002">Max</strong>.</p>
<p id="nas_s_0066__p6874115711505">The value of <strong id="nas_s_0066__b116506872017"><span id="nas_s_0066__text198741157135020"><strong>Burst</strong></span></strong> can be set only after that of <strong id="nas_s_0066__b88771913142012"><span id="nas_s_0066__text287455755018"><strong>Max</strong></span></strong> is set.</p>
</td>
</tr>
<tr id="nas_s_0066__row1487411576504"><td class="cellrowborder" rowspan="3" valign="top" width="16.439999999999998%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p17874457165011"><span id="nas_s_0066__text14874135710505"><strong>Standard IOPS (8 KB)</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="20.75%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p187419572504"><span id="nas_s_0066__text38741457105012"><strong>Min</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.81%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.2 "><p id="nas_s_0066__p48741357195018">Average minimum number of standard I/O requests per second for copy live mount.</p>
</td>
</tr>
<tr id="nas_s_0066__row15874145712504"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p11874185755013"><span id="nas_s_0066__text15874757205017"><strong>Max</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p17874155715507">Average maximum number of standard I/O requests per second for copy live mount.</p>
</td>
</tr>
<tr id="nas_s_0066__row4874165755014"><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p14874125719507"><span id="nas_s_0066__text287475765012"><strong>Burst</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p4874857185015">Maximum burst IOPS for copy live mount. The value of <strong id="nas_s_0066__b418762042015"><span id="nas_s_0066__text187415718500"><strong>Burst</strong></span></strong> must be greater than that of <strong id="nas_s_0066__b19152319441">Max</strong>.</p>
<p id="nas_s_0066__p58742574504">The value of <strong id="nas_s_0066__b1054522672018"><span id="nas_s_0066__text887405718501"><strong>Burst</strong></span></strong> can be set only after that of <strong id="nas_s_0066__b179021130152016"><span id="nas_s_0066__text48742571506"><strong>Max</strong></span></strong> is set.</p>
</td>
</tr>
<tr id="nas_s_0066__row16874175714503"><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p1787419575501"><span id="nas_s_0066__text16874155775017"><strong>Max. Burst Duration (s)</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.2 "><p id="nas_s_0066__p1587415795019">This parameter needs to be set only when the <strong id="nas_s_0066__b17571153818207"><span id="nas_s_0066__text1666111232117"><strong>Burst</strong></span></strong> parameter of the bandwidth or standard IOPS is set. It indicates the maximum duration of burst bandwidth or burst IOPS.</p>
</td>
</tr>
<tr id="nas_s_0066__row10874155719503"><td class="cellrowborder" valign="top" width="16.439999999999998%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p8874557155018"><span id="nas_s_0066__text7874105735015"><strong>Normalized Latency (8 KB)</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="20.75%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.1 "><p id="nas_s_0066__p148741257155014"><span id="nas_s_0066__text1387465765020"><strong>Max</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.81%" headers="mcps1.3.3.2.5.2.1.1.3.2.4.1.2 "><p id="nas_s_0066__p787465715019">Maximum latency of the local storage system.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li id="nas_s_0066__li1497110143316">Click <span class="uicontrol" id="nas_s_0066__uicontrol137675581017"><b><span id="nas_s_0066__text3376145511105"><strong>Next</strong></span></b></span>.</li></ol>
</p></li><li id="nas_s_0066__li176771615123318"><span>Preview the mount parameters. After confirming that the parameters are set correctly, click <span class="uicontrol" id="nas_s_0066__uicontrol193397923310"><b><span id="nas_s_0066__text418014210248"><strong>Finish</strong></span></b></span>.</span><p><div class="note" id="nas_s_0066__note1821020234309"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0066__p15210102319306">If a copy is used for live mount immediately after it is generated, the live mount job may fail because the copy is still in the initialization state. In this case, try again in 3 minutes.</p>
</div></div>
</p></li><li><span>If you want to access a live mount copy, on the <strong><span><strong>Live Mount</strong></span></strong> &gt; <strong><span><strong>NAS File Systems</strong></span></strong> page, choose <strong><span><strong>More</strong></span></strong> &gt; <strong><span><strong>View</strong></span></strong> in the row that contains the live mount copy to be accessed. Then, obtain such information as the share path, share client, share IP address, share name, and username of the copy.</span></li></ol>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>