<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 3: Registering a Hive Cluster">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="hive_00012.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="hive_00016">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 3: Registering a Hive Cluster</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="hive_00016"></a><a name="hive_00016"></a>
  <h1 class="topictitle1">Step 3: Registering a Hive Cluster</h1>
  <div>
   <p>Before Hive backup and restoration, register the Hive cluster with the <span>product</span>.</p>
   <div class="section" id="hive_00016__section10758914153313">
    <a name="hive_00016__section10758914153313"></a><a name="section10758914153313"></a>
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li>Before registering a Hive cluster, ensure that you have obtained the following files. This section uses FusionInsight 6.5.1 as an example. Operations may vary on different platforms. For details, see the corresponding version of product documentation of the big data platform.
      <ol>
       <li>Log in to FusionInsight Manager using a browser.</li>
       <li>Click <span><img src="en-us_image_0000001792521278.png"></span> on the left of the home page to download the client.</li>
       <li>Decompress the downloaded client to obtain the following files:
        <ul>
         <li>The <strong>../Hive/config/hiveclient.properties</strong> file</li>
         <li>The <strong>../Hive/config/hive-site.xml</strong> file</li>
         <li>The <strong>../Hive/config/core-site.xml</strong> file</li>
         <li>The <strong>../Hdfs/config/hdfs-site.xml</strong> file</li>
        </ul></li>
      </ol>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p>The following describes how to obtain the <strong>jaas.conf</strong> and <strong>hive-site.xml</strong> configuration files when the big data platform is Transwarp Data Hub (TDH). This part uses TDH 8.0.2 as an example. Operations may vary with versions. For details, see the Transwarp Data Hub product documentation.</p>
        <ol>
         <li>Use a browser to log in to TDH.</li>
         <li>Choose <strong>Dashboard</strong> &gt; <strong>Cluster</strong>. On the <strong>Service List</strong> tab page, click the Hive cluster name configured in TDH.</li>
         <li>In the upper right corner of the Hive cluster page, click the <span><img src="en-us_image_0000001839240593.png"></span> icon and click <strong>Download Service Configurations</strong> to obtain <strong>jaas.conf</strong> and <strong>hive-site.xml</strong> from the downloaded package.</li>
         <li>Choose <strong>Dashboard</strong> &gt; <strong>Cluster</strong>. On the <strong>Service List</strong> page, click the HDFS cluster name configured in TDH.</li>
         <li>In the upper right corner of the HDFS cluster page, click the <span><img src="en-us_image_0000001792521282.png"></span> icon and click <strong>Download Service Configurations</strong> to obtain <strong>core-site.xml</strong> and <strong>hdfs-site.xml</strong> from the downloaded package.</li>
         <li>Delete the following information from the <strong>core-site.xml</strong> file:<pre class="screen">&lt;property&gt;
    &lt;name&gt;hadoop.security.group.mapping.ldap.bind.password.file&lt;/name&gt;
    &lt;value&gt;/etc/hdfs1/conf/ldap-conn-pass.txt&lt;/value&gt;
&lt;/property&gt;</pre></li>
        </ol>
       </div>
      </div></li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li id="hive_00016__li0198134611381"><span>Choose <span class="uicontrol" id="hive_00016__en-us_topic_0000001839142377_uicontrol186516504391"><b><span id="hive_00016__en-us_topic_0000001839142377_text8651165012398"><strong>Protection</strong></span> &gt; Big Data &gt; Hive</b></span>.</span></li>
     <li><span>On the <span class="uicontrol"><b><span><strong>Clusters</strong></span></b></span> page, click <span class="uicontrol"><b><span><strong>Register</strong></span></b></span> to register a Hive cluster.</span><p></p><p><a href="#hive_00016__table164432003147">Table 1</a> describes the Hive cluster registration information.</p>
      <div class="tablenoborder">
       <a name="hive_00016__table164432003147"></a><a name="table164432003147"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="hive_00016__table164432003147" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Hive cluster registration information
        </caption>
        <colgroup>
         <col style="width:25.03%">
         <col style="width:74.97%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="25.03%" id="mcps1.3.3.2.2.2.2.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="74.97%" id="mcps1.3.3.2.2.2.2.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Customize a Hive cluster name.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>HiveServer2 Link</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Open the <strong>hiveclient.properties</strong> file obtained in <a href="#hive_00016__section10758914153313">Prerequisites</a>. The value of <span class="uicontrol"><b>zk.quorum</b></span> is the configured value.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>If the big data platform is TDH, obtain the attribute value <strong>transwarp.docker.inceptor</strong> from the <strong>hive-site.xml</strong> file and replace the domain name in the value with the IP address of the corresponding production node. Input example: <strong>127.0.0.0:8000</strong></li>
              <li>If the parameter <strong>ZooKeeper Namespace</strong> is left blank, you can enter only one HiveServer2 link.</li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>ZooKeeper Namespace</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Open the <strong>hiveclient.properties</strong> file obtained in <a href="#hive_00016__section10758914153313">Prerequisites</a>. The value of <span class="uicontrol"><b>zooKeeperNamespace</b></span> is the configured value.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><strong>hive-site.xml</strong></p></td>
          <td class="cellrowborder" rowspan="4" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Upload the <span class="filepath"><b>hive-site.xml</b></span>, <span class="filepath"><b>hdfs-site.xml</b></span>, <span class="filepath"><b>core-site.xml</b></span>, and <span class="filepath"><b>hiveclient.properties</b></span> files obtained in <a href="#hive_00016__section10758914153313">Prerequisites</a> to the management page.</p> <p>This file is used to configure Hive cluster information.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>The <span class="filepath"><b>hiveclient.properties</b></span> file is optional.</li>
              <li>The big data cluster supports only IPv4 addresses. IPv6 addresses are not supported. Check whether configurations in the configuration file are correct.</li>
              <li>The configuration file uploaded to the management page must be the same as that in the big data cluster. Do not modify the configuration file locally.</li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><strong>hdfs-site.xml</strong></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><strong>core-site.xml</strong></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><strong>hiveclient.properties</strong></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><strong>Hive Version</strong></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Hive version number.</p> <p>The following uses FusionInsight 6.5.1 as an example to describe how to check the Hive version:</p>
           <ol type="a">
            <li id="hive_00016__li0580145917135"><a name="hive_00016__li0580145917135"></a><a name="li0580145917135"></a>Log in to FusionInsight Manager using a browser.</li>
            <li>Choose <span class="uicontrol"><b>Cluster &gt; Hive</b></span> to view the Hive version.</li>
           </ol>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>If the big data platform is TDH and the big data platform version is 8.X or 6.X, the Hive version is 1.2.0.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Authentication Mode</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Configure the authentication mode for communication between the Hive cluster and the <span>product</span>. Set this parameter based on the authentication mode configured for the Hive cluster.</p>
           <ul>
            <li><span><strong>Simple Authentication</strong></span>: Select this authentication mode when Kerberos authentication is disabled for the Hive cluster. In this authentication mode, usernames are used for authentication, which is less secure.</li>
            <li><span><strong>Kerberos Authentication</strong></span>: Select this authentication mode when Kerberos authentication is enabled for the Hive cluster. The Kerberos protocol is used for identity authentication between the Hive cluster and the <span>product</span>.</li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Username</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Authentication Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Simple Authentication</strong></span></b></span>.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>The user must have the SHOW_DATABASE permission on the database to be backed up, the SELECT permission on the table to be backed up, the CREATE and SHOW_DATABASE permissions on the database to be restored, and the CREATE, DROP, ALTER, and SELECT permissions on the table to be restored.</li>
              <li>The user must have the read, write, and execution permissions on the temporary metadata storage path specified when the backup set is created, the read and execution permissions on the directory where the table data of the database to be backed up is located as well as the lower-layer files and directories, the read, write, and execution permissions on the temporary storage path of metadata specified during restoration, the read, write, and execution permissions on the directory where the data restored to the target database table is located, and the read, write, and execution permissions on the directory where the table metadata and table data are located.</li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Kerberos</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Authentication Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Kerberos Authentication</strong></span></b></span>.</p> <p>Select the created Kerberos authentication. When you register a Hive cluster for the first time, click <span class="uicontrol"><b>Create</b></span> and configure Kerberos authentication parameters. For details about the parameters, see <a href="#hive_00016__table93413354118">Table 2</a>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>HiveServer2 Principal</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Authentication Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Kerberos Authentication</strong></span></b></span>.</p> <p>Open the <strong>hiveclient.properties</strong> file obtained in <a href="#hive_00016__section10758914153313">Prerequisites</a>. The value of <span class="uicontrol"><b>principal</b></span> is the configured value.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>If the big data platform is TDH, the value of this parameter is the value of principal in the Client module in the <strong>jaas.conf</strong> file.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Agent Host</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>Select the agent host for data protection.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>Do not allocate the same agent host to multiple big data clusters with different Kerberos authentication configurations or authentication modes. Otherwise, the backup or restoration job fails.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Certificate verification</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Authentication Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Kerberos Authentication</strong></span></b></span>. The <span><strong>Certificate verification</strong></span> function is enabled by default.</p> <p>If you want to encrypt data during communication between the <span>product</span> and the big data platform for secure communication, import the certificate and enter the certificate password.</p> <p>Import the JKS certificate generated in <a href="hive_00015.html">Step 2: (Optional) Generating and Obtaining a Certificate</a>. The certificate password is the keystore password set in <a href="hive_00015.html">Step 2: (Optional) Generating and Obtaining a Certificate</a>.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>This configuration item can be enabled only when the big data platform is Cloudera CDH.</li>
              <li>After the <span><strong>Certificate verification</strong></span> function is disabled, security risks may exist in the communication between the system and the big data platform. Exercise caution when performing this operation.</li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.03%" headers="mcps1.3.3.2.2.2.2.2.3.1.1 "><p><span><strong>Security Protocol</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.97%" headers="mcps1.3.3.2.2.2.2.2.3.1.2 "><p>This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Authentication Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Kerberos Authentication</strong></span></b></span>. The <span><strong>Security Protocol</strong></span> function is enabled by default.</p> <p>Whether to enable the TLS protocol.</p>
           <ul>
            <li>If the TLS protocol is disabled, TLS 1.2 is used by default.</li>
            <li>If the TLS protocol is enabled, TLS 1.2 or later is supported.
             <div class="note">
              <span class="notetitle"> NOTE: </span>
              <div class="notebody">
               <ul>
                <li>This configuration item can be enabled only when the big data platform is Cloudera CDH.</li>
                <li>After the <span><strong>Security Protocol</strong></span> function is disabled, security risks may exist in the communication between the system and the big data platform. Exercise caution when performing this operation.</li>
               </ul>
              </div>
             </div></li>
           </ul></td>
         </tr>
        </tbody>
       </table>
      </div> <p>Set Kerberos authentication parameters. For details about related parameters, see <a href="#hive_00016__table93413354118">Table 2</a>.</p>
      <div class="tablenoborder">
       <a name="hive_00016__table93413354118"></a><a name="table93413354118"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="hive_00016__table93413354118" frame="border" border="1" rules="all">
        <caption>
         <b>Table 2 </b>Kerberos authentication parameters
        </caption>
        <colgroup>
         <col style="width:30%">
         <col style="width:70%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="30%" id="mcps1.3.3.2.2.2.4.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="70%" id="mcps1.3.3.2.2.2.4.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 "><p>User-defined Kerberos authentication name.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Principal Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 "><p>Principal name of Kerberos authentication.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>Set this parameter to the principal name that has been configured on the Kerberos server.</li>
              <li>The principal name must have the SHOW_DATABASE permission on the database to be backed up, the SELECT permission on the table to be backed up, the CREATE and SHOW_DATABASE permissions on the database to be restored, and the CREATE, DROP, ALTER, and SELECT permissions on the table to be restored.</li>
              <li>The principal name must have the read, write, and execution permissions on the temporary metadata storage path specified when the backup set is created, the read and execution permissions on the directory where the table data of the database to be backed up is located as well as the lower-layer files and directories, the read, write, and execution permissions on the temporary storage path of metadata specified during restoration, the read, write, and execution permissions on the directory where the data restored to the target database table is located, and the read, write, and execution permissions on the directory where the table metadata and table data are located.</li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Configuration Mode</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 ">
           <div class="p">
            Kerberos authentication mode, which must be the same as that configured on the Kerberos server. Two authentication modes are available:
            <ul>
             <li><span><strong>Password</strong></span>: Use the principal name and password for identity authentication.</li>
             <li><span><strong>Keytab File</strong></span>: Use the principal name and keytab file for identity authentication.
              <div class="note">
               <span class="notetitle"> NOTE: </span>
               <div class="notebody">
                <p>If the big data platform is MRS, Hive cluster registration may fail when password authentication is used due to problems of some MRS versions. Therefore, the keytab file authentication mode is recommended.</p>
               </div>
              </div></li>
            </ul>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Config File</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 "><p><span class="filepath"><b>.conf</b></span> configuration file used during Kerberos authentication (for example, krb5.conf). Contact the Kerberos server administrator to obtain the file from the Kerberos server and click <span><img src="en-us_image_0000001792361534.png"></span> to upload it to the management page.</p>
           <div class="p">
            If <span class="uicontrol"><b>renew_lifetime</b></span> exists in the configuration file, comment it out. Otherwise, the registration may fail. For example:
            <pre class="screen">#renew_lifetime = 7d</pre>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Password</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 "><p>This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Configuration Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Password</strong></span></b></span>.</p> <p>Set this parameter to the password of <span class="uicontrol"><b><span><strong>Principal Name</strong></span></b></span>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Confirm Password</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 "><p>Confirm password of <span class="uicontrol"><b><span><strong>Principal Name</strong></span></b></span>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.3.2.2.2.4.2.3.1.1 "><p><span><strong>Keytab File</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.3.2.2.2.4.2.3.1.2 "><p><span class="filepath"><b>.keytab</b></span> configuration file (for example, <strong>krb5.keytab</strong>) used during Kerberos authentication. This parameter is mandatory only when <span class="uicontrol"><b><span><strong>Configuration Mode</strong></span></b></span> is set to <span class="uicontrol"><b><span><strong>Keytab File</strong></span></b></span>.</p> <p>Contact the Kerberos server administrator to obtain the file from the Kerberos server and click <span><img src="en-us_image_0000001839240589.png"></span> to upload it to the management page.</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="hive_00012.html">Backing Up Hive Backup Sets</a>
    </div>
   </div>
  </div>
 </body>
</html>