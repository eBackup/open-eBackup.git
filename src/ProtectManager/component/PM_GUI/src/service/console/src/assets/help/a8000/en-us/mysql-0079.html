<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing Databases">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="mysql-0077.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="documenttype" content="usermanual">
<meta name="prodname" content="csbs">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="mysql-0079">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing Databases</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="mysql-0079"></a><a name="mysql-0079"></a>

<h1 class="topictitle1">Managing Databases</h1>
<div><p>The <span>OceanProtect</span> allows you to modify, remove, activate, or disable protection for databases.</p>
<p>Log in to the management page, choose <span class="uicontrol"><b><span id="mysql-0079__en-us_topic_0000001839142377_text148961988368"><strong>Protection</strong></span> &gt; Databases &gt; MySQL/MariaDB/GreatSQL</b></span>, click <strong><span><strong>Databases</strong></span></strong>, and find the desired database.</p>
<p><a href="#mysql-0079__en-us_topic_0000001263934006_table24934294919">Table 1</a> describes the related operations.</p>

<div class="tablenoborder"><a name="mysql-0079__en-us_topic_0000001263934006_table24934294919"></a><a name="en-us_topic_0000001263934006_table24934294919"></a><table cellpadding="4" cellspacing="0" summary="" id="mysql-0079__en-us_topic_0000001263934006_table24934294919" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Database protection operations</caption><colgroup><col style="width:14.37%"><col style="width:57.9%"><col style="width:27.73%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="14.370000000000003%" id="mcps1.3.4.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="57.900000000000006%" id="mcps1.3.4.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="27.730000000000004%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to modify a protection plan to associate another SLA with a database or modify user-defined script. After the modification, the next backup will be executed based on the new protection plan.</p>
<p><strong>Note</strong></p>
<ul><li id="mysql-0079__en-us_topic_0000001792344090_li16343131892419">The modification does not affect the protection job that is being executed. The modified protection plan will apply to the next protection period.</li><li id="mysql-0079__en-us_topic_0000001792344090_li140522162517">If the protection policy for a resource in the associated SLA changes, you are advised to modify the current SLA on the <span class="uicontrol" id="mysql-0079__en-us_topic_0000001792344090_uicontrol10405524254"><b><span id="mysql-0079__en-us_topic_0000001792344090_text6405425255"><strong>Protection</strong></span> &gt; <span id="mysql-0079__en-us_topic_0000001792344090_text885333645815"><strong>Protection Policies</strong></span> &gt; <span id="mysql-0079__en-us_topic_0000001792344090_text164051025259"><strong>SLAs</strong></span></b></span> page, rather than associate the resource with another SLA through protection modification.<p id="mysql-0079__en-us_topic_0000001792344090_p34052272519">After you associate the resource with another SLA by modifying protection, if the copy of the original SLA still exists, the license capacity may be deducted repeatedly.</p>
</li></ul>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Removing protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>You can remove the protection plan if the database no longer needs to be protected. After the removal, the system cannot execute protection jobs for the database.</p>
<p><strong>Note</strong></p>
<ul><li id="mysql-0079__en-us_topic_0000001792344090_li194311728491">When a protection job of the database is running, the protection cannot be removed.</li><li id="mysql-0079__en-us_topic_0000001792344090_li79479497198">If the protection policy for a resource in the associated SLA changes, you are advised to modify the associated SLA on the <span class="uicontrol" id="mysql-0079__en-us_topic_0000001792344090_uicontrol5174547101015"><b><span id="mysql-0079__en-us_topic_0000001792344090_en-us_topic_0000001839142377_text71111921162814"><strong>Protection</strong></span> &gt; Protection Policies &gt; SLAs</b></span> page, rather than perform SLA re-association after protection removal.<p id="mysql-0079__en-us_topic_0000001792344090_p636319506192">After you remove protection and perform SLA re-association, if the copy of the original SLA still exists, the license capacity may be deducted repeatedly.</p>
</li></ul>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Performing a manual backup</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to perform a backup immediately.</p>
<p><strong>Note</strong></p>
<p>Copies generated by manual backup are retained for the duration defined in the SLA.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Disabling protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to disable a protection plan when periodic protection on the database is not required. After the protection plan is disabled, the system does not execute the automatic backup job of the database.</p>
<p><strong>Note</strong></p>
<p>Disabling protection does not affect ongoing backup jobs.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Activating protection</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to re-activate the disabled protection plan of a database. After the protection plan is activated, the system backs up the database based on the protection plan.</p>
<p><strong>Note</strong></p>
<p>You can perform this operation only on a database that has been associated with an SLA and is in the <span class="uicontrol"><b><span><strong>Unprotected</strong></span></b></span> state.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="mysql-0079__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="mysql-0079__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="mysql-0079__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="mysql-0079__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="mysql-0079__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="mysql-0079__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="mysql-0079__en-us_topic_0000001792344090_ul22969251338"><li id="mysql-0079__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="mysql-0079__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="mysql-0079__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="mysql-0079__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="mysql-0079__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="mysql-0079__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="mysql-0079__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="mysql-0079__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="mysql-0079__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="mysql-0079__en-us_topic_0000001792344090_b472911291311">Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="mysql-0079__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="mysql-0079__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.4.2.4.1.1 mcps1.3.4.2.4.1.2 mcps1.3.4.2.4.1.3 "><p>Note: The procedure for restoring MySQL/MariaDB/GreatSQL data is the same as that described in <a href="mysql-0049.html">Restoration</a>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="mysql-0077.html">MySQL/MariaDB/GreatSQL Database Environment</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>