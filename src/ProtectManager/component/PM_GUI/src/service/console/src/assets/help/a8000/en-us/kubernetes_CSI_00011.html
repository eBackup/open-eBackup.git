<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 3: Registering a Cluster">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="kubernetes_CSI_00010.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="kubernetes_CSI_00011">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 3: Registering a Cluster</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="kubernetes_CSI_00011"></a><a name="kubernetes_CSI_00011"></a>

<h1 class="topictitle1">Step 3: Registering a Cluster</h1>
<div><p>Before backing up and restoring Kubernetes CSI, register the Kubernetes cluster with the <span>OceanProtect</span>. One set of OceanProtect supports a maximum of eight Kubernetes clusters.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li id="kubernetes_CSI_00011__li0198134611381"><span>Choose <span class="uicontrol" id="kubernetes_CSI_00011__en-us_topic_0000001839142377_uicontrol846217343614"><b><span id="kubernetes_CSI_00011__en-us_topic_0000001839142377_text1046212303610"><strong>Protection</strong></span> &gt; Containers &gt; <span id="kubernetes_CSI_00011__en-us_topic_0000001839142377_text149606591485"><strong>Kubernetes CSI</strong></span></b></span>.</span></li><li><span>Click the <span class="uicontrol"><b><span><strong>Clusters</strong></span></b></span> tab, and then click <span class="uicontrol"><b><span><strong>Register</strong></span></b></span>.</span><p><div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>For 1.6.0 and later versions, you can select the following cluster types: general Kubernetes cluster, CCE cluster, and OpenShift cluster.</p>
</div></div>
</p></li><li><span>Register a cluster through token authentication or kubeconfig authentication.</span><p><div class="p"><a href="#kubernetes_CSI_00011__table613227155112">Table 1</a> or <a href="#kubernetes_CSI_00011__table191041022165015">Table 2</a> describes the Kubernetes cluster registration information.
<div class="tablenoborder"><a name="kubernetes_CSI_00011__table613227155112"></a><a name="table613227155112"></a><table cellpadding="4" cellspacing="0" summary="" id="kubernetes_CSI_00011__table613227155112" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Token authentication and registration information of the Kubernetes cluster</caption><colgroup><col style="width:25.69%"><col style="width:74.31%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="25.69%" id="mcps1.3.2.2.3.2.1.3.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="74.31%" id="mcps1.3.2.2.3.2.1.3.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p id="kubernetes_CSI_00011__p312142713516">User-defined cluster name.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><strong>IP Address</strong> and <strong>Port</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p>Service IP address and port number of the Kubernetes service. To query the address and port number, perform the following steps:</p>
<ul><li>For CCE , FusionCompute, and native Kubernetes:<ol type="a"><li>Use PuTTY to log in to any node where the <strong>kubectl</strong> command can be executed in the Kubernetes cluster.</li><li>Run the following command to query the IP address and port number:<pre class="screen">kubectl cluster-info</pre>
<p>The following command output is displayed, where the IP address and port number are displayed in the line that contains <strong>https://</strong>.</p>
<p><span><img src="en-us_image_0000002055309230.png"></span></p>
</li></ol>
</li><li>For OpenShift:<ol type="a"><li>Use PuTTY to log in to any node where the <strong>oc</strong> command can be executed in the Kubernetes cluster.</li><li>Run the following command to query the port number:<pre class="screen">oc cluster-info</pre>
<p>The following command output is displayed, where <strong>api.ocp4.example.com</strong> is the domain name, and <strong>6443</strong> is the port number.</p>
<p><span><img src="en-us_image_0000002091269621.png"></span></p>
</li><li>Run the following command to query the IP address:<pre class="screen">ping <em>api.ocp4.example.com</em></pre>
</li></ol>
</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><strong>Token Information</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p>Service access token of Kubernetes. The <span>OceanProtect</span> must at least have the token with the minimum permissions to ensure that backup and restoration jobs are normal. For details about how to generate a token with the minimum permissions, see <a href="kubernetes_CSI_00077.html">Step 2: (Optional) Generating a Token with the Minimum Permissions</a>.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><ul><li>For FusionCompute, you can also obtain the value of the <strong>token</strong> field from the <strong>kubeconfig</strong> configuration file. This method is more convenient, but the obtained token has all permissions.</li><li>For OpenShift, obtain the token of the administrator permission by following the instructions provided in <a href="kubernetes_CSI_00078_5.html">Obtaining the Token Information</a>.</li></ul>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><strong>Backup Image Name and Tag</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p id="kubernetes_CSI_00011__p25013413491">Image name and tag of the pod that performs PVC backup. Before the backup, upload the image file of the backup software to the image repository of the Kubernetes cluster. For details about how to upload the image file, see <a href="kubernetes_CSI_00069.html">Uploading the Kubernetes Installation Package to the Image Repository</a>, <a href="kubernetes_CSI_00102.html">Uploading and Updating the Kubernetes Installation Package</a> and <a href="kubernetes_CSI_00078_7.html">Uploading the Kubernetes Installation Package to the Kubernetes Cluster</a>.</p>
<p id="kubernetes_CSI_00011__p48051330164511">After uploading an image file, you can obtain the image name and tag value in the following ways:</p>
<ul id="kubernetes_CSI_00011__ul105295215419"><li id="kubernetes_CSI_00011__li195293284118">If FusionCompute is used as the management platform, choose <strong id="kubernetes_CSI_00011__b5965136156">Container Images</strong> &gt; <strong id="kubernetes_CSI_00011__b1996611362055">Local Repositories</strong> &gt; <strong id="kubernetes_CSI_00011__b49661361056">library</strong> on the <strong id="kubernetes_CSI_00011__b199672362050">Container Management</strong> page, and click the name of the uploaded image file. On the displayed <strong id="kubernetes_CSI_00011__b16967736853">Image Version</strong> tab page, in the row of the target image version, click <strong id="kubernetes_CSI_00011__b10968123611514">Copy Pull Command</strong> on the right. In the copied command, the information similar to the following (the x86 image file is used as an example) refers to the backup image name and tag value:<pre class="screen" id="kubernetes_CSI_00011__screen1712962513211">192.168.1.1:7443/library/k8s_backup_image_x86:latest</pre>
</li><li id="kubernetes_CSI_00011__li56261561908">If CCE is used as the management platform, log in to the CCE console and choose <span class="uicontrol" id="kubernetes_CSI_00011__uicontrol119970145513"><b>Cloud Container Engine &gt; SoftWare Repository for Container &gt; Images</b></span>. Click the name of the uploaded image file and view the image name and tag value in the <span class="uicontrol" id="kubernetes_CSI_00011__uicontrol5997171410517"><b>Image Pull Command</b></span> column of the corresponding image version.</li><li id="kubernetes_CSI_00011__li201625813305">If OpenShift is used as the management platform, you can obtain the image name and tag by referring to <a href="kubernetes_CSI_00078_3.html">Uploading the Kubernetes Installation Package and Obtaining the Image Name and Tag Information</a>.</li><li id="kubernetes_CSI_00011__li0411441268">If native Kubernetes is used as the management platform, run the following command to query the backup image name and tag:<pre class="screen" id="kubernetes_CSI_00011__screen145658515815">crictl image</pre>
<p id="kubernetes_CSI_00011__p167419218117">For example, the obtained backup image name and tag are as follows:</p>
<p id="kubernetes_CSI_00011__p155071212103">docker.io/library/k8s_backup_image_x86</p>
<p id="kubernetes_CSI_00011__p11497144111114">latest</p>
</li></ul>
<p id="kubernetes_CSI_00011__p953851972611">Input example:</p>
<p id="kubernetes_CSI_00011__p4876840195210"><em id="kubernetes_CSI_00011__i209414504482">Image name</em>:<em id="kubernetes_CSI_00011__i39884538482">Tag value</em></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><strong>Backup Jobs on a Single Node</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p>Maximum number of concurrent backup jobs supported by a single node. The default value is 4, and the maximum value is 8. Set a proper number of backup jobs based on the remaining space of the Kubernetes CSI.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><span><strong>Task Timeout Duration</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p id="kubernetes_CSI_00011__p624916214445">Default maximum execution duration of a backup or restoration job. If the backup or restoration job execution time exceeds the configured job timeout interval, the job will fail to be executed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><span><strong>Certificate verification</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p id="kubernetes_CSI_00011__p81197595317">This function is enabled by default. During token authentication, you need to enter the certificate value. When a Kubernetes cluster is registered, its certificate is verified to ensure the security of interaction between the <span id="kubernetes_CSI_00011__text871433918575">OceanProtect</span> and external devices. After this function is disabled, security risks may exist in the communication between the system and Kubernetes. Exercise caution when performing this operation.</p>
<div class="note" id="kubernetes_CSI_00011__note993161378"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="kubernetes_CSI_00011__ul159310241159"><li id="kubernetes_CSI_00011__li18593132419155">If the management platform is DCS-FusionCompute, the certificate value is the parameter value of <strong id="kubernetes_CSI_00011__b13856122317158">certificate-authority-data</strong> of the registered Kubernetes cluster configuration file.</li><li id="kubernetes_CSI_00011__li4979202510155">For details about how to obtain the certificate value when the management platform is CCE, see <a href="kubernetes_CSI_00079.html">Obtaining the Certificate Value During Token Authentication (for CCE)</a>.</li><li id="kubernetes_CSI_00011__li123573391613">If OpenShift is used as the management platform, run the following command on the background page of the Kubernetes cluster. The value of <strong id="kubernetes_CSI_00011__b2074693311453">certificate-authority-data</strong> in the command output is the certificate value.<pre class="screen" id="kubernetes_CSI_00011__screen38231528191012">oc config view --minify --flatten</pre>
</li></ul>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.69%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.1 "><p><span><strong>Node Selector</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.31%" headers="mcps1.3.2.2.3.2.1.3.2.3.1.2 "><p id="kubernetes_CSI_00011__p1716591412491">Used to specify the node for executing pod backup. You can click <strong id="kubernetes_CSI_00011__b339130125714">Add</strong> to specify nodes by using multiple labels. For details about how to query labels, see <a href="kubernetes_CSI_00010_1.html">Step 1: (Optional) Querying Node Labels of the Kubernetes Cluster</a>.</p>
<p id="kubernetes_CSI_00011__p19822309268">Example of the relationship between the <strong id="kubernetes_CSI_00011__b10218181523511">Key</strong> and <strong id="kubernetes_CSI_00011__b12543191933516">Value</strong> of a label: <strong id="kubernetes_CSI_00011__b7865192202320">Key=Value,controller=CTE0.A</strong></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>

<div class="tablenoborder"><a name="kubernetes_CSI_00011__table191041022165015"></a><a name="table191041022165015"></a><table cellpadding="4" cellspacing="0" summary="" id="kubernetes_CSI_00011__table191041022165015" frame="border" border="1" rules="all"><caption><b>Table 2 </b>kubeconfig authentication and registration information of the Kubernetes cluster</caption><colgroup><col style="width:25.590000000000003%"><col style="width:74.41%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="25.590000000000003%" id="mcps1.3.2.2.3.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="74.41%" id="mcps1.3.2.2.3.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>User-defined cluster name.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><strong>kubeconfig File</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>Upload the obtained <strong>kubeconfig</strong> configuration file of Kubernetes CSI.</p>
<ul><li>For CCE, see <a href="kubernetes_CSI_00078.html">Obtaining the kubeconfig Configuration File</a>.</li><li>For FusionCompute, see <a href="kubernetes_CSI_00065.html">Obtaining the kubeconfig Configuration File</a>.</li><li>For details about OpenShift, see <a href="kubernetes_CSI_00078_4.html">Obtaining the kubeconfig Configuration File</a>.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><strong>Backup Image Name and Tag</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>Image name and tag of the pod that performs PVC backup. Before the backup, upload the image file of the backup software to the image repository of the Kubernetes cluster. For details about how to upload the image file, see <a href="kubernetes_CSI_00069.html">Uploading the Kubernetes Installation Package to the Image Repository</a>, <a href="kubernetes_CSI_00102.html">Uploading and Updating the Kubernetes Installation Package</a> and <a href="kubernetes_CSI_00078_7.html">Uploading the Kubernetes Installation Package to the Kubernetes Cluster</a>.</p>
<p>After uploading an image file, you can obtain the image name and tag value in the following ways:</p>
<ul><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li195293284118">If FusionCompute is used as the management platform, choose <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b5965136156">Container Images</strong> &gt; <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b1996611362055">Local Repositories</strong> &gt; <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b49661361056">library</strong> on the <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b199672362050">Container Management</strong> page, and click the name of the uploaded image file. On the displayed <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b16967736853">Image Version</strong> tab page, in the row of the target image version, click <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b10968123611514">Copy Pull Command</strong> on the right. In the copied command, the information similar to the following (the x86 image file is used as an example) refers to the backup image name and tag value:<pre class="screen" id="kubernetes_CSI_00011__kubernetes_csi_00011_screen1712962513211">192.168.1.1:7443/library/k8s_backup_image_x86:latest</pre>
</li><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li56261561908">If CCE is used as the management platform, log in to the CCE console and choose <span class="uicontrol" id="kubernetes_CSI_00011__kubernetes_csi_00011_uicontrol119970145513"><b>Cloud Container Engine &gt; SoftWare Repository for Container &gt; Images</b></span>. Click the name of the uploaded image file and view the image name and tag value in the <span class="uicontrol" id="kubernetes_CSI_00011__kubernetes_csi_00011_uicontrol5997171410517"><b>Image Pull Command</b></span> column of the corresponding image version.</li><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li201625813305">If OpenShift is used as the management platform, you can obtain the image name and tag by referring to <a href="kubernetes_CSI_00078_3.html">Uploading the Kubernetes Installation Package and Obtaining the Image Name and Tag Information</a>.</li><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li0411441268">If native Kubernetes is used as the management platform, run the following command to query the backup image name and tag:<pre class="screen" id="kubernetes_CSI_00011__kubernetes_csi_00011_screen145658515815">crictl image</pre>
<p id="kubernetes_CSI_00011__kubernetes_csi_00011_p167419218117">For example, the obtained backup image name and tag are as follows:</p>
<p id="kubernetes_CSI_00011__kubernetes_csi_00011_p155071212103">docker.io/library/k8s_backup_image_x86</p>
<p id="kubernetes_CSI_00011__kubernetes_csi_00011_p11497144111114">latest</p>
</li></ul>
<p>Input example:</p>
<p><em id="kubernetes_CSI_00011__kubernetes_csi_00011_i209414504482">Image name</em>:<em id="kubernetes_CSI_00011__kubernetes_csi_00011_i39884538482">Tag value</em></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><strong>Backup Jobs on a Single Node</strong></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>Maximum number of concurrent backup jobs supported by a single node. The default value is 4, and the maximum value is 8. Set a proper number of backup jobs based on the remaining space of the Kubernetes CSI.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><span><strong>Task Timeout Duration</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>Default maximum execution duration of a backup or restoration job. If the backup or restoration job execution time exceeds the configured job timeout interval, the job will fail to be executed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><span><strong>Certificate verification</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>This function is enabled by default. During token authentication, you need to enter the certificate value. When a Kubernetes cluster is registered, its certificate is verified to ensure the security of interaction between the <span id="kubernetes_CSI_00011__kubernetes_csi_00011_text871433918575">OceanProtect</span> and external devices. After this function is disabled, security risks may exist in the communication between the system and Kubernetes. Exercise caution when performing this operation.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="kubernetes_CSI_00011__kubernetes_csi_00011_ul159310241159"><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li18593132419155">If the management platform is DCS-FusionCompute, the certificate value is the parameter value of <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b13856122317158">certificate-authority-data</strong> of the registered Kubernetes cluster configuration file.</li><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li4979202510155">For details about how to obtain the certificate value when the management platform is CCE, see <a href="kubernetes_CSI_00079.html">Obtaining the Certificate Value During Token Authentication (for CCE)</a>.</li><li id="kubernetes_CSI_00011__kubernetes_csi_00011_li123573391613">If OpenShift is used as the management platform, run the following command on the background page of the Kubernetes cluster. The value of <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b2074693311453">certificate-authority-data</strong> in the command output is the certificate value.<pre class="screen" id="kubernetes_CSI_00011__kubernetes_csi_00011_screen38231528191012">oc config view --minify --flatten</pre>
</li></ul>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.590000000000003%" headers="mcps1.3.2.2.3.2.2.2.3.1.1 "><p><span><strong>Node Selector</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.41%" headers="mcps1.3.2.2.3.2.2.2.3.1.2 "><p>Used to specify the node for executing pod backup. You can click <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b339130125714">Add</strong> to specify nodes by using multiple labels. For details about how to query labels, see <a href="kubernetes_CSI_00010_1.html">Step 1: (Optional) Querying Node Labels of the Kubernetes Cluster</a>.</p>
<p>Example of the relationship between the <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b10218181523511">Key</strong> and <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b12543191933516">Value</strong> of a label: <strong id="kubernetes_CSI_00011__kubernetes_csi_00011_b7865192202320">Key=Value,controller=CTE0.A</strong></p>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span><p><p>After the registration is successful, the registered Kubernetes CSI information is displayed on the <span><strong>Clusters</strong></span> tab page. The system automatically discovers namespaces in the Kubernetes cluster and displays them on the <span><strong>Namespace</strong></span> tab page.</p>
</p></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="kubernetes_CSI_00010.html">Backing Up Namespaces or Datasets</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>