<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 3: Creating a Logical Port for the Replication Network">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="nas_s_0011.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0014">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 3: Creating a Logical Port for the Replication Network</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0014"></a><a name="nas_s_0014"></a>

<h1 class="topictitle1">Step 3: Creating a Logical Port for the Replication Network</h1>
<div><p>When a storage device is OceanStor Dorado 6.1.3 or later, or OceanStor 6.1.3 or later, NAS file system backup can be implemented using the replication function between the production storage and <span>OceanProtect</span>. In this case, a logical port for the replication network needs to be created separately on the storage device and the <span>OceanProtect</span> for remote replication data transmission. NAS share backup is implemented if other types of storage devices are used, and a logical port is not required for the replication network. In this case, skip this section.</p>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><ul><li>The replication network IP addresses of the primary and secondary storage systems have been planned and are in the same network segment as the backup network IP addresses.</li><li>A logical port whose <strong>Role</strong> is <strong>Management + service</strong> and <strong>Data Protocol</strong> is <strong>NFS + CIFS</strong> has been created on DeviceManager of the OceanProtect. For details, see "Creating a Logical Port" in the <i><cite>Installation Guide</cite></i>.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><p>The following describes how to create a logical port for the replication network on the <span>OceanProtect</span>. This also applies to the creation on a storage device.</p>
<ol><li id="nas_s_0014__en-us_topic_0000001188062683_en-us_topic_0000001149078659_li319522912350"><span>Log in to DeviceManager.</span><p><ol type="a" id="nas_s_0014__ol55051543162413"><li id="nas_s_0014__li7505843162418">The following method applies to OceanProtect X series backup appliances:<ol class="substepthirdol" id="nas_s_0014__en-us_topic_0000001188062683_en-us_topic_0000001149078659_ol8463855369"><li id="nas_s_0014__li7787111161619">Choose <strong id="nas_s_0014__b10128199342">System</strong> &gt; <strong id="nas_s_0014__b41281913410">Infrastructure</strong> &gt; <strong id="nas_s_0014__b13121719143414">Cluster Management</strong>.</li><li id="nas_s_0014__li541671613166">On the <strong id="nas_s_0014__b383014414340">Backup Clusters</strong> tab page, click a node name under the <strong id="nas_s_0014__b18301641143415">Local Cluster Nodes</strong> area.</li><li id="nas_s_0014__li15142820111618">On the displayed <strong id="nas_s_0014__b15587849163415">Node Details</strong> page, click <strong id="nas_s_0014__b15587149123416">Open the device management platform</strong> to go to DeviceManager.</li></ol>
</li><li id="nas_s_0014__li5767154612411">For OceanProtect E1000, log in to DeviceManager by referring to <a href="nas_s_dm.html">Logging In to DeviceManager</a>.</li></ol>
</p></li><li><span>Choose <span class="uicontrol"><b>Services &gt; Network &gt; Logical Ports</b></span>.</span></li><li><span>Click <span class="uicontrol"><b>Create</b></span> to create a logical port.</span><p><p><a href="#nas_s_0014__en-us_topic_0000001188062683_en-us_topic_0000001149078659_table4902520114617">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="nas_s_0014__en-us_topic_0000001188062683_en-us_topic_0000001149078659_table4902520114617"></a><a name="en-us_topic_0000001188062683_en-us_topic_0000001149078659_table4902520114617"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0014__en-us_topic_0000001188062683_en-us_topic_0000001149078659_table4902520114617" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Parameters related to a logical port</caption><colgroup><col style="width:43.55%"><col style="width:56.45%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="43.55%" id="mcps1.3.3.3.3.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="56.45%" id="mcps1.3.3.3.3.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Name</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Name of a replication network logical port, which can be customized.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Role</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Select <span class="uicontrol"><b>Replication</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>IP address type</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>IP address type of a replication network logical port, which can be IPv4 or IPv6.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>IP</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>IP address of a replication network logical port</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Subnet mask</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Configure a subnet mask if the IP address type is IPv4.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Prefix</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Configure a prefix if the IP address type is IPv6.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Gateway</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Gateway used for the IP address of a replication network logical port</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Port type</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Users can create a logical port for the replication network by selecting Ethernet port, bond port, or VLAN.</p>
<p>By default, the port used for replication has been bonded during the initialization of the <span>OceanProtect</span>. Therefore, the bond port needs to be selected if there is no other operation (such as deleting the bond port).</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="43.55%" headers="mcps1.3.3.3.3.2.2.2.3.1.1 "><p>Home port</p>
</td>
<td class="cellrowborder" valign="top" width="56.45%" headers="mcps1.3.3.3.3.2.2.2.3.1.2 "><p>Ethernet port, bond port, or VLAN to which the created logical port belongs.</p>
<p>Select any one of the ports after the system has filtered all ports used for the replication network.</p>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li><li><span>Repeat the preceding steps to configure a logical port for each home port.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="nas_s_0011.html">Backing Up a NAS File System</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>