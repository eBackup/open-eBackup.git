<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Creating a File System">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0100">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Creating a File System</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0100"></a><a name="nas_s_0100"></a>

<h1 class="topictitle1">Creating a File System</h1>
<div><p>Before restoring a NAS share to a local location for the first time, you need to create a file system.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Set the file system name.</span><p><p>By default, the name is prefixed with <strong>Restore_</strong>.</p>
</p></li><li><span>Configure the sharing mode.</span><p><p><a href="#nas_s_0100__table89615043315">Table 1</a> describes the related parameters.</p>
<p>You can configure NFS and CIFS shares at the same time.</p>

<div class="tablenoborder"><a name="nas_s_0100__table89615043315"></a><a name="table89615043315"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0100__table89615043315" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Sharing mode parameters</caption><colgroup><col style="width:16.439999999999998%"><col style="width:20.72%"><col style="width:62.839999999999996%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.2.2.2.2.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.2.2.2.2.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" rowspan="2" valign="top" width="16.439999999999998%" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p>NFS</p>
</td>
<td class="cellrowborder" valign="top" width="20.72%" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p><span><strong>Share Path</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.839999999999996%" headers="mcps1.3.2.2.2.2.3.2.4.1.2 "><p>Share path for accessing the file system. The path name is the same as the file system name and cannot be changed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p><span><strong>Client</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><div class="p">Information about the client that accesses a file system.<ul id="nas_s_0100__nas_s_0066_ul10853114216166"><li id="nas_s_0100__nas_s_0066_li141781311161">You can enter share client IP addresses, share client IP address segments, or an asterisk (*) to represent IP addresses of all share clients.</li><li id="nas_s_0100__nas_s_0066_li717813151615">The IP addresses can be IPv4 addresses, IPv6 addresses, or a combination of IPv4 and IPv6 addresses.</li><li id="nas_s_0100__nas_s_0066_li2178531151612">You can enter multiple IP addresses or IP address segments and separate them by semicolons (;), spaces, or <strong id="nas_s_0100__nas_s_0066_b3334203131920">Enter</strong>. Example: <strong id="nas_s_0100__nas_s_0066_b186852403458">192.168.0.10;192.168.0.0/24;*</strong></li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="3" valign="top" width="16.439999999999998%" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p>CIFS</p>
</td>
<td class="cellrowborder" valign="top" width="20.72%" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p><span><strong>Share Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="62.839999999999996%" headers="mcps1.3.2.2.2.2.3.2.4.1.2 "><p>Share name for accessing a file system.</p>
<p>The default share name is <strong>Restore_</strong><em>Timestamp</em>. You can change it as needed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p><span><strong>Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p>Type of the user who accesses a CIFS share.</p>
<ul id="nas_s_0100__ul35301947103614"><li id="nas_s_0100__li115301547103614"><span id="nas_s_0100__text11836124163210"><strong>Everyone</strong></span></li><li id="nas_s_0100__li1853094715369"><span id="nas_s_0100__text12625493210"><strong>Windows Local Authentication User</strong></span></li><li id="nas_s_0100__li58994343227"><span id="nas_s_0100__text1018841611228"><strong>Windows Local Authentication User Group</strong></span></li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p><span><strong>Users</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.3.2.4.1.1 "><p>Username for accessing a CIFS share.</p>
<p id="nas_s_0100__p862510421222">This parameter is mandatory when <strong id="nas_s_0100__b140357142311"><span id="nas_s_0100__text66251842152215"><strong>Type</strong></span></strong> is set to <strong id="nas_s_0100__b5567205814231"><span id="nas_s_0100__text1862524232215"><strong>Windows Local Authentication User</strong></span></strong> or <strong id="nas_s_0100__b14169117244"><span id="nas_s_0100__text462474214224"><strong>Windows Local Authentication User Group</strong></span></strong>.</p>
<p id="nas_s_0100__p7841427394">If the required user or user group does not exist, create one on DeviceManager.</p>
<ul id="nas_s_0100__ul613494612188"><li id="nas_s_0100__li1134104615181">Perform the following operations for X series backup appliances:<ol type="a" id="nas_s_0100__ol6503987191"><li id="nas_s_0100__li1512122844010">Log in to the management page on another browser page.</li><li id="nas_s_0100__en-us_topic_0000001607531760_en-us_topic_0000001311295305_en-us_topic_0000001149078659_li929117916361">Choose <span class="uicontrol" id="nas_s_0100__uicontrol16830596269"><b>System</b></span> &gt; <strong id="nas_s_0100__b08017182461">Infrastructure</strong> &gt; <strong id="nas_s_0100__b680151814615">Cluster Management</strong>.</li><li id="nas_s_0100__li19932939164117">On the <span class="uicontrol" id="nas_s_0100__uicontrol10974142710269"><b>Backup Clusters</b></span> tab page, click a node name under the <span class="uicontrol" id="nas_s_0100__uicontrol88011243202610"><b>Local Cluster Nodes</b></span> area.</li><li id="nas_s_0100__li1115217015589">In the <span class="uicontrol" id="nas_s_0100__uicontrol17351204276"><b>Node Details</b></span> dialog box that is displayed, click <span class="uicontrol" id="nas_s_0100__uicontrol11051616276"><b>Open the device management platform</b></span> to go to DeviceManager.</li><li id="nas_s_0100__li56039149190">Choose <strong id="nas_s_0100__b19121415125116">Services</strong> &gt; <strong id="nas_s_0100__b1127231975119">File Service</strong> &gt; <strong id="nas_s_0100__b10123182265110">Authentication Users</strong> &gt; <strong id="nas_s_0100__b1321813267515">Windows Users</strong> from the navigation pane. On the <strong id="nas_s_0100__b374613855119">Local Authentication Users</strong> or <strong id="nas_s_0100__b1793194812289">Local Authentication User Groups</strong> tab page, create a user or user group.</li></ol>
</li><li id="nas_s_0100__li64527110138">For OceanProtect E1000, perform the following operations:<ol type="a" id="nas_s_0100__ol05910119136"><li id="nas_s_0100__li9591911151315">Log in to DeviceManager by referring to <a href="nas_s_dm.html">Logging In to DeviceManager</a>.</li><li id="nas_s_0100__li6526141413133">Choose <strong id="nas_s_0100__b834518383413">Resources</strong> &gt; <strong id="nas_s_0100__b163460388413">Access</strong> &gt; <strong id="nas_s_0100__b6346173854118">Authentication User</strong> &gt; <strong id="nas_s_0100__b13346173817411">Windows Users</strong> from the navigation pane. On the <strong id="nas_s_0100__b43468382413">Local Authentication User</strong> or <strong id="nas_s_0100__b1834610387414">Local Authentication User Group</strong> tab page, create a user or user group.</li></ol>
</li></ul>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>Click <strong><span><strong>OK</strong></span></strong>.</span></li></ol>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>