<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring Object Storage">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="object-0044.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="object-0047">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring Object Storage</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="object-0047"></a><a name="object-0047"></a>
  <h1 class="topictitle1">Restoring Object Storage</h1>
  <div>
   <p>This section describes how to restore the object storage that has been backed up to the original location or a new location.</p>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li>Before the restoration, ensure that the bucket with the same name exists in the target location or the conditions of creating the target bucket for restoration are met. Otherwise, the restoration will fail.</li>
    </ul>
    <ul>
     <li>Before restoring data to a new location, ensure that users have the read and write permissions on the AK/SK of the target object storage for restoration.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="object-0047__en-us_topic_0000001839142377_uicontrol11629101161212"><b><span id="object-0047__en-us_topic_0000001839142377_text1629611111214"><strong>Explore</strong></span> &gt; <span id="object-0047__en-us_topic_0000001839142377_text562901114126"><strong>Copy Data</strong></span> &gt; File Systems &gt; <span id="object-0047__en-us_topic_0000001839142377_text518282701311"><strong>Object Storage</strong></span></b></span>.</span></li>
     <li><span>You can search for copies by object storage resource or copy. This section describes how to search for copies by resource.</span><p></p><p>On the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab page, find the resource to be restored by resource name and click the resource name.</p> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> and choose the year, month, and day to find the copy.</span><p></p><p>If <span><img src="en-us_image_0000001801156142.png"></span> is displayed below a month or date, copies exist in the month or on the day.</p> <p></p></li>
     <li><span>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Restore</strong></span></b></span> in the row that contains the copy.</span><p></p><p><a href="#object-0047__Files-0044_table93951625101715">Table 1</a> describes the related parameters.</p>
      <div class="tablenoborder">
       <a name="object-0047__Files-0044_table93951625101715"></a><a name="Files-0044_table93951625101715"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="object-0047__Files-0044_table93951625101715" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Parameters for restoring object storage
        </caption>
        <colgroup>
         <col style="width:23.09%">
         <col style="width:76.91%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="23.09%" id="mcps1.3.3.2.4.2.2.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="76.91%" id="mcps1.3.3.2.4.2.2.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Restore To</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>Location where the object storage is to be restored.</p>
           <ul>
            <li><span><strong>Original location</strong></span>: indicates to restore data to the original object storage location.</li>
            <li><span><strong>New location</strong></span>: indicates to restore data to a new location. In this case, you need to specify the target location and target object storage for restoration.</li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Location</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only when data is restored to the original object storage location.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><strong>Target Object Storage</strong></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only when data is restored to a new location. You need to select the target object storage.</p> <p>You can search for the name of object storage that has been registered with the <span>product</span> to select the object storage.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><strong>Target Bucket</strong></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only when data is restored to a new location.</p>
           <ul>
            <li><strong>Select an existing bucket</strong>: indicates to select an existing bucket of the current user.</li>
            <li><strong>Create a bucket</strong>: indicates to create a bucket for the current user. In this case, data will be restored to the created bucket.</li>
           </ul>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <ul>
              <li>If you select an existing bucket and the ACL data has been backed up in the copy, the ACL of the target bucket will be overwritten during restoration.</li>
              <li>If multiple buckets to be restored have objects with the same name and prefix, only one object can be randomly restored.</li>
              <li>Enter a name for the bucket to be created based on the naming rule of <span class="uicontrol"><b>Type of Owning Object Storage</b></span>.
               <ul>
                <li>OceanStor Pacific: The namespace name consists of 1 to 255 characters, including only letters, digits, hyphens (-), underscores (_), and periods (.). It must contain at least one letter or digit.</li>
                <li>Huawei Cloud Stack Object Storage Service (OBS): The bucket name must be globally unique and cannot be the same as any existing bucket name, including the names of buckets created by other users. Once a bucket is created, its name cannot be changed. Make sure that the bucket name you set is appropriate.<p>Name the bucket in the OBS according to the globally applied DNS naming rules as follows:</p> <p>The name of a deleted bucket can be reused for another bucket or a parallel file system at least in 30 minutes after the deletion.</p> <p>Must contain 3 to 63 characters and supports only lowercase letters, digits, hyphens (-), and periods (.)</p> <p>Cannot start or end with a period (.) or hyphen (-). A period (.) is not allowed to be adjacent with another period (.) or any hyphens (-).</p> <p>Cannot be an IP address.</p> <p>When you access OBS through HTTPS using a virtual host and if the bucket name contains a period (.), the certificate verification will fail. Therefore, you are advised not to use periods (.) in bucket names.</p></li>
                <li>Alibaba Cloud Object Storage Service (OSS): The bucket name must contain 3 to 63 characters, including only lowercase letters, digits, and hyphens (-); must start and end with a lowercase letter or digit.</li>
               </ul></li>
             </ul>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><strong>Prefix</strong></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>Prefix added to the object to be restored.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>After a prefix is added, the prefix length of some objects may exceed the limit of the production end. As a result, the object restoration job fails.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Overwrite Rule</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>If a file with the same name exists in the restoration path, you can choose to replace or skip the existing file.</p>
           <ul>
            <li><span><strong>Replace existing files</strong></span></li>
            <li><span><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li>
            <li><span><strong>Only replace the files older than the restoration file</strong></span>: The latest file with the same name in the target path is retained.</li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><strong>Select Objects to Be Restored</strong></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only for object-level restoration.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>If there are no data changes after the incremental backup, no data will be displayed on the GUI.</p>
            </div>
           </div></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="object-0044.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>