<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 1: Registering the SAP HANA Database (File Backup Mode)">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="SAP_HANA_0012.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="SAP_HANA_0013">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 1: Registering the SAP HANA Database (File Backup Mode)</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="SAP_HANA_0013"></a><a name="SAP_HANA_0013"></a>

<h1 class="topictitle1">Step 1: Registering the SAP HANA Database (File Backup Mode)</h1>
<div><p>Before backing up and restoring the SAP HANA database, you must register the SAP HANA database with the <span>OceanProtect</span>.</p>
<p>In the file backup mode, backup data is written to the file system. Each SAP HANA service writes the backup data to a separate file in the specified target in the file system.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="SAP_HANA_0013__en-us_topic_0000001839142377_uicontrol19391112102819"><b>Protection &gt; Databases &gt; General Databases</b></span>.</span></li><li><span>Click <span class="uicontrol"><b><span><strong>Register</strong></span></b></span> to register the SAP HANA database. <a href="#SAP_HANA_0013__table613227155112">Table 1</a> describes the SAP HANA database registration information.</span><p>
<div class="tablenoborder"><a name="SAP_HANA_0013__table613227155112"></a><a name="table613227155112"></a><table cellpadding="4" cellspacing="0" summary="" id="SAP_HANA_0013__table613227155112" frame="border" border="1" rules="all"><caption><b>Table 1 </b>SAP HANA database registration information</caption><colgroup><col style="width:25.629999999999995%"><col style="width:74.37%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="25.629999999999995%" id="mcps1.3.3.2.2.2.1.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="74.37%" id="mcps1.3.3.2.2.2.1.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>Database type.</p>
<ul><li>For a tenant database, select <span class="uicontrol"><b>Single Server</b></span> or <span class="uicontrol"><b>Distributed cluster</b></span>.</li><li>For a system database, select <span class="uicontrol"><b>Single Server</b></span> or <span class="uicontrol"><b>Active/Standby cluster</b></span>.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>Specifies the name of the database to be protected. For details about how to query the name, see <a href="SAP_HANA_0011.html">Preparations for Backup</a>.</p>
<p><span><img src="en-us_image_0000001839268593.png"></span></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Hosts</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>Host where the database resides.</p>
<p>If the database is deployed in a cluster, select all hosts where the database is distributed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Database Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>Select <span class="uicontrol"><b>SAP HANA</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Authentication Mode</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>Select <span class="uicontrol"><b><span><strong>Database authentication</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Database Username</strong></span></p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>When <strong id="SAP_HANA_0013__sap_hana_0020_b10813448284">Authentication Mode</strong> is set to <span class="uicontrol" id="SAP_HANA_0013__sap_hana_0020_uicontrol7754151012283"><b><span id="SAP_HANA_0013__sap_hana_0020_text1511991692611"><strong>Database authentication</strong></span></b></span>, enter the username and password of the system database. </p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Database Password</strong></span></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Other Authentication Information</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>This parameter is required only when the database to be protected is a tenant database.</p>
<div class="p">The format is as follows:<pre class="screen"><strong>systemDbUser=</strong><em>System database username</em><strong>,systemDbPassword=</strong><em>System database password</em></pre>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.2.2.2.1.2.3.1.1 "><p><span><strong>Customized Parameters</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.2.2.2.1.2.3.1.2 "><p>Set <strong>systemId</strong> and <strong>SQL Port</strong> of the database for successful registration.</p>
<ul><li>You can query <strong>systemId</strong> on the SAP HANA Studio, as shown in the following figure.<p><span><img src="en-us_image_0000001839188633.png"></span></p>
</li><li>SQL Port indicates the SQL port on the master node of the system database. You can query the SQL port on the SAP HANA Studio.<p>In single-tenant mode, the default SQL Port is <strong>30015</strong>. In multi-tenant mode, the default SQL Port is <strong>30013</strong>.</p>
</li></ul>
<p>The format is as follows:</p>
<pre class="screen"><strong>systemId=</strong><em>?</em><strong>,systemDbPort=</strong><em>?</em></pre>
<p>Example:</p>
<pre class="screen">systemId=D00,systemDbPort=30015</pre>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="SAP_HANA_0012.html">Backing Up the SAP HANA Database (General Databases Path)</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>