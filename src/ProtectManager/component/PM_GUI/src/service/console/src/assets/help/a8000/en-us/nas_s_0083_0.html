<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Viewing Copy Information About an NDMP NAS File System">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="nas_s_0082_0.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0083_0">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Viewing Copy Information About an NDMP NAS File System</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0083_0"></a><a name="nas_s_0083_0"></a>

<h1 class="topictitle1">Viewing Copy Information About an NDMP NAS File System</h1>
<div><p>This section describes details about NDMP NAS file system copies.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="nas_s_0083_0__en-us_topic_0000001839142377_uicontrol824614391515"><b><span id="nas_s_0083_0__en-us_topic_0000001839142377_text1324633181516"><strong>Explore</strong></span> &gt; <span id="nas_s_0083_0__en-us_topic_0000001839142377_text92466391512"><strong>Copy Data</strong></span> &gt; File Systems &gt; NDMP</b></span>.</span></li><li><span>Select a copy viewing mode to view the copy information.</span><p><div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0083_0__en-us_topic_0000001792503826_p189141741162012">Replication copies can be viewed only in the target cluster.</p>
</div></div>
<ul><li><span><strong>Resources</strong></span>: View the copy information of file systems by file system. If data in a file system is lost and you want to restore the file system, you are advised to search on this portal for the copy at the corresponding time point.<p><a href="#nas_s_0083_0__table20406195873110">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="nas_s_0083_0__table20406195873110"></a><a name="table20406195873110"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0083_0__table20406195873110" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Copy parameters</caption><colgroup><col style="width:7.5200000000000005%"><col style="width:24.65%"><col style="width:67.83%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.2.2.2.2.2.1.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.2.2.2.2.2.1.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" rowspan="3" valign="top" width="7.5200000000000005%" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Resources</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="24.65%" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.83%" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.2 "><p>Name of a NAS file system.</p>
<p>Click the name of a NAS file system to view its copy data.</p>
<p>You can search for copies by year, month, or day.</p>
<p>If <span><img src="en-us_image_0000001897309126.png"></span> is displayed below the time, a copy at that time exists.</p>
<p>Click <strong><span><strong>Clear Index</strong></span></strong> to delete all indexes of a resource.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><ul><li>If a copy is being indexed, it cannot be deleted until the indexing is complete.</li><li>If <span><strong>Automatic Indexing</strong></span> is enabled for the SLA associated with a resource and <span><strong>Automatic Indexing</strong></span> is enabled for the backup policy, you need to disable <span><strong>Automatic Indexing</strong></span> or disassociate the resource from the SLA before deleting the index. If <span><strong>Automatic Indexing</strong></span> is enabled for the archiving policy, you need to disassociate the resource from the SLA before deleting the index.</li></ul>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p>Location of a NAS file system.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p>Whether the resource where the NAS file system resides is registered with the current management platform.</p>
<ul><li>If yes, <strong><span><strong>Status</strong></span></strong> is <strong><span><strong>Existing</strong></span></strong>.</li><li>If no, <strong><span><strong>Status</strong></span></strong> is <strong><span><strong>Not existing</strong></span></strong>.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="3" valign="top" width="7.5200000000000005%" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Copies</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="24.65%" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Quantity</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.83%" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.2 "><p>Number of copies generated based on the NAS file system. The number of replication copies is displayed only in the target cluster.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Replicated Copy SLA</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p>Archive SLA associated with a replication copy.</p>
<p>If an archive SLA has been configured for the replication copy generated by the resource, the SLA name is displayed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><p><span><strong>Replicated Copy Protection Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.1.3.2.4.1.1 "><div class="p">Whether a replication copy is protected. <ul id="nas_s_0083_0__en-us_topic_0000001792503826_ul161172118317"><li id="nas_s_0083_0__en-us_topic_0000001792503826_li11611621173118"><span id="nas_s_0083_0__en-us_topic_0000001792503826_text16611162123119"><strong>Unprotected</strong></span>: The replication copy is not associated with an SLA, or the replication copy is associated with an SLA but protection is disabled.</li><li id="nas_s_0083_0__en-us_topic_0000001792503826_li761111211312"><span id="nas_s_0083_0__en-us_topic_0000001792503826_text161192116317"><strong>Protected</strong></span>: The replication copy is associated with an SLA and protection is enabled.</li><li id="nas_s_0083_0__en-us_topic_0000001792503826_li2088261163715"><span id="nas_s_0083_0__en-us_topic_0000001792503826_text12602914182118"><strong>Creating</strong></span>: The replication copy is associated with an SLA and protection is being created. The status is available only in 1.5.0 and earlier versions.</li></ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</li><li><span><strong>Copies</strong></span>: View information about all copies by copy. When you need to restore data to the original host or a different host based on a specified copy, you are advised to search for the copy through this portal.<p><a href="#nas_s_0083_0__table1390718405171">Table 2</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="nas_s_0083_0__table1390718405171"></a><a name="table1390718405171"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0083_0__table1390718405171" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Copy parameters</caption><colgroup><col style="width:7.76%"><col style="width:28.79%"><col style="width:63.449999999999996%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.2.2.2.2.2.2.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.2.2.2.2.2.2.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" rowspan="9" valign="top" width="7.76%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copies</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="28.79%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Backup Time</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="63.449999999999996%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.2 "><p>Time when a backup copy is generated.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copy Time</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>Time when a copy is generated. </p>
<p>Click <span class="uicontrol" id="nas_s_0083_0__en-us_topic_0000001792503826_uicontrol1250926125014"><b><span id="nas_s_0083_0__en-us_topic_0000001792503826_text149471616195614"><strong>Copy Time</strong></span></b></span> to view the expiration time and other information of the copy. If <span class="uicontrol" id="nas_s_0083_0__en-us_topic_0000001792503826_uicontrol945613251346"><b>--</b></span> is displayed as the expiration time, the copy is retained permanently.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p">Copy status.<ul id="nas_s_0083_0__vmware_gud_0085_0_ul465416347216"><li id="nas_s_0083_0__vmware_gud_0085_0_li1171923612110"><span id="nas_s_0083_0__vmware_gud_0085_0_text184721240184920"><strong>Normal</strong></span>: The copy is available.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li6520237152112"><span id="nas_s_0083_0__vmware_gud_0085_0_text118991624193016"><strong>Invalid</strong></span>: The copy is invalid.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li15384133802118"><span id="nas_s_0083_0__vmware_gud_0085_0_text27876556498"><strong>Deleting</strong></span>: The copy is being deleted.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li911933972115"><span id="nas_s_0083_0__vmware_gud_0085_0_text127427011501"><strong>Restoring</strong></span>: The copy is being restored or instantly recovered.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li09820392217"><span id="nas_s_0083_0__vmware_gud_0085_0_text117483211199"><strong>Mounting</strong></span>: The copy is being mounted.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li148811240182111"><span id="nas_s_0083_0__vmware_gud_0085_0_text12375115116199"><strong>Unmounting</strong></span>: The copy is being destroyed.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li18577198457"><span id="nas_s_0083_0__vmware_gud_0085_0_text1195181218204"><strong>Mounted</strong></span>: The copy has been mounted.</li><li id="nas_s_0083_0__vmware_gud_0085_0_li16555142220324"><span id="nas_s_0083_0__vmware_gud_0085_0_text166661656203220"><strong>Deletion failed</strong></span>: The copy fails to be deleted.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>Location of a copy.</p>
<ul><li>When <span class="uicontrol"><b><span><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol"><b><span><strong>Backup</strong></span></b></span>, <span class="uicontrol"><b><span><strong>Cascaded Replication</strong></span></b></span>, <span class="uicontrol"><b><span><strong>Reverse Replication</strong></span></b></span>, <span class="uicontrol"><b><span><strong>Replicate</strong></span></b></span>, or <span class="uicontrol"><b><span><strong>Live Mount</strong></span></b></span>, the copy is stored on the local <span>OceanProtect</span>. In this case, <span class="uicontrol"><b>Local</b></span> is displayed.</li><li>If <span class="uicontrol"><b><span><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol"><b><span><strong>Object Storage Archive</strong></span></b></span> or <span class="uicontrol"><b><span><strong>Import</strong></span></b></span>, the copy is stored in a bucket of the object storage. In this case, the bucket name is displayed.</li><li>When <span class="uicontrol"><b><span><strong>Generation Mode</strong></span></b></span> of a copy is <span class="uicontrol"><b><span><strong>Tape Archive</strong></span></b></span>, the copy is stored in the tape library. The media set name is displayed.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Generation Mode</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p">Generation mode of a copy.<ul><li><span id="nas_s_0083_0__en-us_topic_0000001792503826_text124238155813"><strong>Replication</strong></span>: copies generated by a replication job.</li></ul>
<ul><li><span id="nas_s_0083_0__en-us_topic_0000001792503826_text109638183455"><strong>Backup</strong></span>: copies generated by a backup job.</li></ul>
<ul><li><span id="nas_s_0083_0__en-us_topic_0000001792503826_text171691481756"><strong>Object Storage Archive</strong></span>: copies generated by an archiving job (object storage as the archive storage).</li><li><span id="nas_s_0083_0__en-us_topic_0000001792503826_text1762165620514"><strong>Tape Archive</strong></span>: copies generated by an archiving job (tape library as the archive storage).</li><li><span><strong>Live Mount</strong></span>: Copies are generated by live mount jobs.</li><li><span id="nas_s_0083_0__en-us_topic_0000001792503826_text1456012717316"><strong>Import</strong></span>: archive copies imported from archive storage to the <span id="nas_s_0083_0__en-us_topic_0000001792503826_text1546918420312">OceanProtect</span>.</li><li><span><strong>Cascaded Replication</strong></span>: Copies are generated by cascaded replication jobs.</li><li><span><strong>Reverse Replication</strong></span>: Copies are generated by reverse replication jobs.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copy Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>Backup type of a generated copy, which includes <strong>Forever Incremental (Synthetic Full)</strong>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p"><strong id="nas_s_0083_0__en-us_topic_0000001792503826_b916692512">Expired At</strong><div class="note" id="nas_s_0083_0__en-us_topic_0000001792503826_note130943715514"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0083_0__en-us_topic_0000001792503826_en-us_topic_0000001656771385_p108956094911">This parameter is available only in 1.5.0SPC26 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>Copy expiration time. If a copy is permanently valid, <strong id="nas_s_0083_0__en-us_topic_0000001792503826_b19549142216517">--</strong> is displayed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span id="nas_s_0083_0__en-us_topic_0000001792503826_text68691158124911"><strong>WORM</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p">Whether a copy has been set as a WORM copy. <ul id="nas_s_0083_0__en-us_topic_0000001792503826_ul75853135118"><li id="nas_s_0083_0__en-us_topic_0000001792503826_li458531175118"><span><img id="nas_s_0083_0__en-us_topic_0000001792503826_image18812012104311" src="en-us_image_0000001792344150.png"></span>: The copy is not set as a WORM copy, that is, the copy is a common copy.</li><li id="nas_s_0083_0__en-us_topic_0000001792503826_li994353785116"><span><img id="nas_s_0083_0__en-us_topic_0000001792503826_image05089185413" src="en-us_image_0000001792344146.png"></span>: The copy has been set as a WORM copy. The WORM copy cannot be deleted and its retention period cannot be shortened.</li><li id="nas_s_0083_0__en-us_topic_0000001792503826_li373211419517"><span><img id="nas_s_0083_0__en-us_topic_0000001792503826_image155001184515" src="en-us_image_0000001839143265.png"></span>: The copy is being set as a WORM copy. A copy in this state cannot be deleted and the retention period cannot be modified.</li><li id="nas_s_0083_0__en-us_topic_0000001792503826_li444514572513"><span><img id="nas_s_0083_0__en-us_topic_0000001792503826_image17671746527" src="en-us_image_0000001839223221.png"></span>: The copy fails to be set as a WORM copy. A copy that fails to be set as a WORM copy cannot be deleted and the retention period cannot be shortened.</li><li id="nas_s_0083_0__en-us_topic_0000001792503826_li216064774817"><span><img id="nas_s_0083_0__en-us_topic_0000001792503826_image9564104794819" src="en-us_image_0000002090948029.png"></span>: The copy has expired. This state is available only in 1.6.0 and later versions. It indicates that the WORM copy has expired. Copies in this state can be deleted.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p"><strong id="nas_s_0083_0__en-us_topic_0000001792503826_b1952114112010">WORM Expiration Time</strong><div class="note" id="nas_s_0083_0__en-us_topic_0000001792503826_note866011102212"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0083_0__en-us_topic_0000001792503826_en-us_topic_0000001792344110_p630385412417">This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p">If a copy is a WORM copy, the WORM expiration time is displayed. Otherwise, <strong id="nas_s_0083_0__en-us_topic_0000001792503826_b1914333131317">--</strong> is displayed.<div class="note" id="nas_s_0083_0__en-us_topic_0000001792503826_note173611252434"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0083_0__en-us_topic_0000001792503826_p10361192510430">The WORM expiration time is based on the WORM file system compliance clock. If the WORM file system compliance clock time is inconsistent with the system time, check the WORM status after the WORM file system compliance clock reaches the set expiration time.</p>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="7.76%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Copies</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="28.79%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Index</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="63.449999999999996%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.2 "><div class="p">If the <span><strong>Automatic Indexing</strong></span> function is enabled, the system creates indexes for all files in a NAS file system, helping you quickly search for the required files for restoration. Possible states are described as follows:<ul><li><span><strong>Indexed</strong></span>: The system has created an index for a copy.</li><li><span><strong>Indexing</strong></span>: The system is creating an index for a copy after it is generated.</li><li><span><strong>Not indexed</strong></span>: The <strong><span><strong>Automatic Indexing</strong></span></strong> function is disabled in an SLA.</li><li><span><strong>Index deleting</strong></span>: The index is being deleted.</li><li><span><strong>Index failed</strong></span>: The system fails to create an index for a copy.</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="4" valign="top" width="7.76%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Resources</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="28.79%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="63.449999999999996%" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.2 "><p>Name of a NAS file system.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>Location of a NAS file system.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p><span><strong>Status</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>Whether the NAS file system is registered with the current management platform.</p>
<ul><li>If yes, <strong><span><strong>Status</strong></span></strong> is <strong><span><strong>Existing</strong></span></strong>.</li><li>If no, <strong><span><strong>Status</strong></span></strong> is <strong><span><strong>Not existing</strong></span></strong>.</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><div class="p"><strong id="nas_s_0083_0__en-us_topic_0000001792503826_b347812294118">Tag</strong><div class="note" id="nas_s_0083_0__en-us_topic_0000001792503826_note1246801219248"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0083_0__en-us_topic_0000001792503826_en-us_topic_0000001792344110_p630385412417_1">This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.2.2.2.2.3.2.4.1.1 "><p>If a tag has been added for the resource, the tag name is displayed. Otherwise, <strong id="nas_s_0083_0__en-us_topic_0000001792344110_b1959642011219">--</strong> is displayed.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li></ul>
</p></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="nas_s_0082_0.html">Copies</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>