<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 4: Creating a Backup SLA">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="mongodb-0011.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="mongodb-0015">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 4: Creating a Backup SLA</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="mongodb-0015"></a><a name="mongodb-0015"></a>
  <h1 class="topictitle1">Step 4: Creating a Backup SLA</h1>
  <div>
   <div class="section">
    <h4 class="sectiontitle">Context</h4>
    <p>For details about preset SLAs and customized SLAs, see <a href="vmware_gud_000026_0.html">About SLA</a>.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="mongodb-0015__en-us_topic_0000001839142377_uicontrol8111821122820"><b><span id="mongodb-0015__en-us_topic_0000001839142377_text71111921162814"><strong>Protection</strong></span> &gt; Protection Policies &gt; SLAs</b></span>.</span></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Create</strong></span></b></span>.</span></li>
     <li><span>Enter an SLA name.</span></li>
     <li id="mongodb-0015__en-us_topic_0000001337708186_li6674114017375"><span>Select the application type for the SLA to be created.</span><p></p>
      <ol type="a" id="mongodb-0015__en-us_topic_0000001839143189_ol71011061385">
       <li id="mongodb-0015__en-us_topic_0000001839143189_li874192611325">Click the <span class="uicontrol" id="mongodb-0015__en-us_topic_0000001839143189_uicontrol16364345183319"><b><span id="mongodb-0015__en-us_topic_0000001839143189_text1149753619468"><strong>Applications</strong></span></b></span> icon and select the application for the SLA to be created.
        <ul id="mongodb-0015__en-us_topic_0000001839143189_ul157501053341">
         <li id="mongodb-0015__en-us_topic_0000001839143189_li3750135143412"><span id="mongodb-0015__en-us_topic_0000001839143189_text10801323154310"><strong>General SLA</strong></span><p id="mongodb-0015__en-us_topic_0000001839143189_p144377559348">A general SLA can be applied to resources except filesets, SQL Server, PostgreSQL, Kingbase, Redis, ClickHouse, MongoDB, NAS shares, NAS file systems, Huawei Cloud Stack GaussDB, TDSQL, Kubernetes CSI, TiDB, volumes, common shares, Active Directory, GaussDB T single-node systems, object storage, GaussDB (1.6.0 and later versions), and SAP HANA (as an application in 1.6.0 and later versions).</p></li>
         <li id="mongodb-0015__en-us_topic_0000001839143189_li1512516103413"><span id="mongodb-0015__en-us_topic_0000001839143189_text1431211194314"><strong>Specific to application</strong></span><p id="mongodb-0015__en-us_topic_0000001839143189_p1889316245358">Select the application type for the SLA to be created. This section uses <span id="mongodb-0015__text4770426152713"><strong>Specific to application</strong></span> as an example.</p></li>
        </ul></li>
       <li id="mongodb-0015__en-us_topic_0000001839143189_li7586156123316">Click <span class="uicontrol" id="mongodb-0015__uicontrol1580085762710"><b>OK</b></span>.</li>
      </ol> <p></p></li>
     <li><span>Configure a backup policy.</span><p></p>
      <ol type="a">
       <li>Click the <span class="uicontrol"><b><span><strong>Backup Policy</strong></span></b></span> icon.</li>
       <li>Set basic parameters for the backup policy.<p><a href="#mongodb-0015__table2663151516335">Table 1</a> describes the related parameters.</p>
        <div class="p">
         Set the backup interval, copy retention period, and time window based on service requirements. The recommended settings are as follows:
         <ul>
          <li>The backup interval must be longer than the backup duration.</li>
          <li>The copy retention period must be longer than the backup interval.</li>
         </ul>
         <div class="note">
          <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
          <div class="notebody">
           <ul>
            <li>You need to create at least one backup policy. To add other backup policies, click <span><img src="en-us_image_0000002010497921.png"></span> on the right of the page.</li>
            <li>Only one backup policy can be added for log backup. A maximum of four backup policies can be added for other backup types.</li>
           </ul>
          </div>
         </div>
        </div>
        <div class="tablenoborder">
         <a name="mongodb-0015__table2663151516335"></a><a name="table2663151516335"></a>
         <table cellpadding="4" cellspacing="0" summary="" id="mongodb-0015__table2663151516335" frame="border" border="1" rules="all">
          <caption>
           <b>Table 1 </b>Basic parameters of a backup policy
          </caption>
          <colgroup>
           <col style="width:25%">
           <col style="width:25%">
           <col style="width:50%">
          </colgroup>
          <thead align="left">
           <tr>
            <th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.2.2.5.2.1.2.3.2.4.1.1"><p>Parameter</p></th>
            <th align="left" class="cellrowborder" valign="top" id="mcps1.3.2.2.5.2.1.2.3.2.4.1.2"><p>Description</p></th>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td class="cellrowborder" rowspan="2" valign="top" width="25%" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Full backup</strong></span></p> <p></p> <p></p></td>
            <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Name</strong></span></p></td>
            <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.2 "><p>Set the backup policy name.</p></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p>-</p></td>
            <td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 ">
             <div class="p">
              Configure the backup frequency, copy retention period, and backup time window for the full backup.
              <ul>
               <li>Configure the backup interval and copy retention period of the full backup.
                <ul>
                 <li><span><strong>By Year</strong></span><p>Configure the job to be executed once every year on <em>xx</em> (month) <em>xx</em> (day). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p> <p>If the date does not exist in the year, no copy is generated.</p></li>
                 <li><span><strong>By Month</strong></span><p>Configure the job to be executed on <em>xx</em> (day) (multiple days can be selected) or the last day of each month. Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p> <p>If the backup is to be executed on the <em>xx</em>th day of each month, no copy is generated when the date does not exist in the current month.</p></li>
                 <li><span><strong>By Week</strong></span><p>Configure the job to be executed once every Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, or Sunday (you can select multiple options). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p></li>
                 <li><strong>By Day</strong><p>Configure the job to be executed every <em>xx</em> days starting from <em>xx</em> (year) <em>xx</em> (month) <em>xx</em> (day). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p></li>
                 <li><strong>By Hour</strong><p>Configure the job to be executed every <em>xx</em> hours starting from <em>xx</em> (year) <em>xx</em> (month) <em>xx</em> (day). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p></li>
                </ul></li>
               <li>Set the time period for executing full backup. The full backup job will not be scheduled beyond the time range.
                <div class="note">
                 <span class="notetitle"> NOTE: </span>
                 <div class="notebody">
                  <ul id="mongodb-0015__en-us_topic_0000001839143189_ul64461851101420">
                   <li id="mongodb-0015__en-us_topic_0000001839143189_li037445621410">If the end time is earlier than or the same as the start time, the end time is actually the end time of the next day.</li>
                   <li id="mongodb-0015__en-us_topic_0000001839143189_li4446145117142">If the backup job is not completed within the specified time window, the system does not stop the backup job, but reports an event.</li>
                   <li id="mongodb-0015__en-us_topic_0000001839143189_li4684315367">Once the retention period expires, the system automatically deletes the expired copies.</li>
                  </ul>
                 </div>
                </div></li>
              </ul>
             </div></td>
           </tr>
           <tr>
            <td class="cellrowborder" rowspan="2" valign="top" width="25%" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Log Backup</strong></span></p></td>
            <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Name</strong></span></p></td>
            <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.2 "><p>Set the backup policy name.</p></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p>-</p></td>
            <td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.2.1.2.3.2.4.1.1 "><p>Configure the initial backup time, backup interval, and copy retention period for log backup.</p>
             <ul>
              <li><span><strong>By Day</strong></span><p>Configure the job to be executed every <em>xx</em> days starting from <em>xx</em> (year) <em>xx</em> (month) <em>xx</em> (day). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p></li>
              <li><span><strong>By Hour</strong></span><p>Configure the job to be executed every <em>xx</em> hours starting from <em>xx</em> (year) <em>xx</em> (month) <em>xx</em> (day). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p></li>
              <li><span><strong>By Minute</strong></span><p>Configure the job to be executed every <em>xx</em> minutes starting from <em>xx</em> (year) <em>xx</em> (month) <em>xx</em> (day). Configure copies to be retained for <em>xx</em> days, <em>xx</em> weeks, <em>xx</em> months, <em>xx</em> years, or permanently.</p></li>
             </ul>
             <div class="note">
              <span class="notetitle"> NOTE: </span>
              <div class="notebody">
               <ul>
                <li>If the end time is earlier than or the same as the start time, the end time is actually the end time on the next day.</li>
                <li>If the backup job is not completed within the specified time window, the system will not stop the backup job, but report an event.</li>
                <li>Before backing up logs, ensure that a full backup copy exists.</li>
                <li>Once the copy retention period expires, the system automatically deletes the expired copies.</li>
               </ul>
              </div>
             </div></td>
           </tr>
          </tbody>
         </table>
        </div></li>
       <li>For 1.6.0 and later versions, to generate a WORM copy, enable WORM and set a WORM validity period. Otherwise, skip this step.
        <div class="p" id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_p19339194114493">
         For <strong id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_b15358729184918">WORM Validity Period</strong>, you can select <strong id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_b469832194914">Same as the copy retention period</strong> or <strong id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_b4820353104917">Custom validity period</strong>.
         <div class="note" id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_note8945151620502">
          <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
          <div class="notebody">
           <ul id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_ul2550728175518">
            <li id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_li103014415565">WORM copies cannot be deleted or their validity periods cannot be shortened.</li>
            <li id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_li9171132315916">The custom WORM validity period cannot be longer than the copy retention period.</li>
            <li id="mongodb-0015__fc_gud_0018_0_en-us_topic_0000001839143189_li976612319580">A WORM copy cannot be changed to a non-WORM copy within its validity period.</li>
           </ul>
          </div>
         </div>
        </div></li>
      </ol>
      <ol type="a" start="4">
       <li>Set advanced parameters for the backup policy.
        <div class="p">
         <a href="#mongodb-0015__en-us_topic_0000001337708186_table1970194542811">Table 2</a> describes the related parameters. 
         <div class="tablenoborder">
          <a name="mongodb-0015__en-us_topic_0000001337708186_table1970194542811"></a><a name="en-us_topic_0000001337708186_table1970194542811"></a>
          <table cellpadding="4" cellspacing="0" summary="" id="mongodb-0015__en-us_topic_0000001337708186_table1970194542811" frame="border" border="1" rules="all">
           <caption>
            <b>Table 2 </b>Advanced parameters for backing up the database
           </caption>
           <colgroup>
            <col style="width:40%">
            <col style="width:60%">
           </colgroup>
           <thead align="left">
            <tr>
             <th align="left" class="cellrowborder" valign="top" width="40%" id="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1"><p>Parameter</p></th>
             <th align="left" class="cellrowborder" valign="top" width="60%" id="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2"><p>Description</p></th>
            </tr>
           </thead>
           <tbody>
            <tr>
             <td class="cellrowborder" valign="top" width="40%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1 "><p><strong id="mongodb-0015__en-us_topic_0000001839143189_b203081655125718">Specify Target Location</strong></p></td>
             <td class="cellrowborder" valign="top" width="60%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2 ">
              <div class="p">
               Specify the target storage location of backup copies.
               <ul id="mongodb-0015__en-us_topic_0000001839143189_ul3832679403">
                <li id="mongodb-0015__en-us_topic_0000001839143189_li299019528162"><span class="parmvalue" id="mongodb-0015__en-us_topic_0000001839143189_parmvalue20580546155113"><b>Backup Storage Units</b></span>: A backup storage unit corresponds to a backup cluster node. After a backup storage unit is selected, copies are stored to the storage unit. If the capacity of the storage unit is insufficient, the backup job will fail.<p id="mongodb-0015__en-us_topic_0000001839143189_p36749546164">For details about how to create a backup storage unit, see "Managing Backup Storage Units" in the <em id="mongodb-0015__en-us_topic_0000001839143189_i142601350162015">Cluster HA Feature Guide</em>.</p></li>
                <li id="mongodb-0015__en-us_topic_0000001839143189_li168151656101610"><span class="parmvalue" id="mongodb-0015__en-us_topic_0000001839143189_parmvalue142224995111"><b>Backup Storage Unit Groups</b></span>: A backup storage unit group is a collection of backup storage units. After a backup storage unit group is selected, the system automatically selects a target backup storage unit based on the storage policy of the storage unit group.<p id="mongodb-0015__en-us_topic_0000001839143189_p15126135815168">For details about how to create a backup storage unit group, see "(Optional) Creating Backup Storage Unit Groups" in the <em id="mongodb-0015__en-us_topic_0000001839143189_i168179237215">Cluster HA Feature Guide</em>.</p></li>
               </ul>
               <div class="note" id="mongodb-0015__en-us_topic_0000001839143189_note06533915171">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <ul id="mongodb-0015__en-us_topic_0000001839143189_ul2261104811172">
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li182618487176">For 1.5.0, this parameter is displayed only when you have deployed the cluster HA feature. You can leave the target location unspecified. If you do not specify the target location, the system automatically selects the target storage unit through intelligent balancing.</li>
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li1025484315189">For 1.6.0 and later versions, you must specify the target location.</li>
                 </ul>
                </div>
               </div>
              </div></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="40%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1 "><p><span><strong>Rate Limiting Policies</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="60%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2 "><p>Average maximum amount of data that can be transmitted per second in a backup job.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="40%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1 ">
              <div class="p">
               <span id="mongodb-0015__en-us_topic_0000001839143189_text1535624514432"><strong>Source Deduplication</strong></span>
               <div class="note" id="mongodb-0015__en-us_topic_0000001839143189_note1141182723518">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <p id="mongodb-0015__en-us_topic_0000001839143189_p164111271351"></p>
                </div>
               </div>
              </div></td>
             <td class="cellrowborder" valign="top" width="60%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2 ">
              <div class="p">
               After this function is enabled, the amount of data to be transmitted can be reduced, but more client resources will be occupied.
               <div class="note" id="mongodb-0015__en-us_topic_0000001839143189_note8885112682911">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <ul id="mongodb-0015__en-us_topic_0000001839143189_ul5885426132914">
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li10885172616299">An agent host can only be linked with one storage unit. To enable source deduplication, select <span class="parmvalue" id="mongodb-0015__en-us_topic_0000001839143189_parmvalue13417617181719"><b>Backup Storage Unit</b></span> for <span class="parmname" id="mongodb-0015__en-us_topic_0000001839143189_parmname164341313131719"><b>Specify Target Location</b></span>. Otherwise, a storage unit that is not linked with the agent host may be selected through intelligent balancing. As a result, source deduplication fails to be enabled. If different protected objects are associated with different SLAs, where different backup storage units are specified, select different agent hosts for the protected objects. Otherwise, source deduplication of some backup jobs will fail to be enabled.</li>
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li9885182662911">If an agent host has been linked with a storage unit but is not performing a backup job with source deduplication, you can run the <strong id="mongodb-0015__en-us_topic_0000001839143189_b1734071915190">dataturbo delete storage_object storage_name=</strong><em id="mongodb-0015__en-us_topic_0000001839143189_i18291255102216">?</em> command on the agent host to delete the source deduplication link with the storage unit. The value of <strong id="mongodb-0015__en-us_topic_0000001839143189_b9927131312572">storage_name</strong> in the command can be queried by running the <strong id="mongodb-0015__en-us_topic_0000001839143189_b11782052162315">dataturbo show storage_object</strong> command. This way, when the agent host performs a backup job with source deduplication, the link between the agent host and a new storage unit can be established.</li>
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li11885142615297">View the product compatibility to check whether source deduplication is supported by the host OS. If not supported, source deduplication will not take effect after being configured.</li>
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li7883549153219">If a built-in agent (that is, ProtectAgent is co-deployed with the <span id="mongodb-0015__en-us_topic_0000001839143189_text13835155817114">product</span>) is used for backup, source deduplication does not take effect after being configured.</li>
                 </ul>
                </div>
               </div>
              </div></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="40%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1 ">
              <div class="p">
               <strong id="mongodb-0015__en-us_topic_0000001839143189_b693713423122">Job Timeout Alarm</strong>
               <div class="note" id="mongodb-0015__en-us_topic_0000001839143189_note379352823112">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <p id="mongodb-0015__en-us_topic_0000001839143189_en-us_topic_0000001839143213_p8320153562113">This parameter is available only in 1.6.0 and later versions.</p>
                </div>
               </div>
              </div></td>
             <td class="cellrowborder" valign="top" width="60%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2 "><p>After this function is enabled, an alarm is sent if the job execution time exceeds the time window. The alarm needs to be manually cleared.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="40%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1 "><p><span><strong>Job Failure Alarm</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="60%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2 "><p>After this function is enabled, an alarm is sent when a job fails. The alarm is automatically cleared when the next job is successful.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="40%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.1 "><p><span><strong>Automatic Retry</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="60%" headers="mcps1.3.2.2.5.2.2.1.1.2.2.3.1.2 "><p>After this function is enabled, the system automatically retries if a periodic backup job fails.</p>
              <div class="p">
               The number of retries ranges from 1 to 5, and the wait time ranges from 1 to 30 minutes. For example, if the number of retries is set to 3 and the wait time is set to 5 minutes, the system retries for a total of three times at an interval of 5 minutes.
               <div class="note" id="mongodb-0015__en-us_topic_0000001839143189_note139701345152818">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <ul id="mongodb-0015__en-us_topic_0000001839143189_ul264071314279">
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li1864001332715">During automatic retry, the system creates a backup job. The backup job is executed only when it is within the preset time window.</li>
                  <li id="mongodb-0015__en-us_topic_0000001839143189_li116401113172713">This function takes effect only during periodic backup. Automatic retry is not supported if a backup job fails to be manually initiated.</li>
                 </ul>
                </div>
               </div>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
        </div></li>
       <li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li>
      </ol> <p></p></li>
     <li><span>(Optional) Configure an archive policy.</span><p></p><p>The built-in storage space of the <span>product</span> is limited. If you need to retain copy data for a long time, you are advised to archive the copy data to external storage.</p> <p>You can configure an archive policy by clicking the <span class="uicontrol"><b><span><strong>Archive Policy</strong></span></b></span> icon or by modifying the SLA.</p> <p>For details about how to configure an archive policy, see <a href="mongodb-0032.html">Archiving</a>.</p> <p></p></li>
     <li><span>(Optional) Configure a replication policy.</span><p></p><p>The <span>product</span> allows you to replicate local data to a remote data center. If a disaster occurs, data copies in the remote data center can be used to restore production data and services can be taken over.</p> <p>You can configure a replication policy by clicking the <span class="uicontrol"><b><span><strong>Replication Policy</strong></span></b></span> icon or by modifying the SLA.</p> <p>For details about how to configure a replication policy, see <a href="mongodb-0030.html">Step 7: Creating a Replication SLA</a>.</p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p id="mongodb-0015__en-us_topic_0000001839143189_p187474161610">For 1.6.0 and later versions:</p>
        <ul id="mongodb-0015__en-us_topic_0000001839143189_ul1078182475012">
         <li id="mongodb-0015__en-us_topic_0000001839143189_li83901215999">The WORM validity period of the backup policy in the SLA must be shorter than the copy retention period in the replication policy.</li>
         <li id="mongodb-0015__en-us_topic_0000001839143189_li878324105017">If WORM is not configured at the local end but configured at the target end, the WORM validity period is the same as the replication copy retention period.</li>
         <li id="mongodb-0015__en-us_topic_0000001839143189_li07815248507">If WORM is configured at the local end but not at the target end, the WORM validity period of replication copies is the same as that of WORM copies at the local end. The retention period of replication copies is greater than or equal to the WORM validity period of backup copies at the local end.</li>
         <li id="mongodb-0015__en-us_topic_0000001839143189_li1278624195020">If WORM is configured at both the local and target ends and WORM is configured by resource in the WORM configuration of <strong id="mongodb-0015__en-us_topic_0000001839143189_b78231650163717">Data Security</strong> at the target end, all replication copies will have WORM enabled, and the WORM validity period is the same as the replication copy retention period.</li>
         <li id="mongodb-0015__en-us_topic_0000001839143189_li127819245503">At the local end, if you configure local backup copies to be retained permanently and set <strong id="mongodb-0015__en-us_topic_0000001839143189_b20511104361110">WORM Validity Period</strong> to <strong id="mongodb-0015__en-us_topic_0000001839143189_b851224341112">Same as the copy retention period</strong>, the WORM validity period is permanent. After a backup copy is generated, if you modify the SLA policy (by adding a replication policy and setting <strong id="mongodb-0015__en-us_topic_0000001839143189_b115122018172413">Replication and Retention Rules</strong> to <strong id="mongodb-0015__en-us_topic_0000001839143189_b9172163882417">Replicate all copies</strong>), the WORM validity period at the target end is the same as the retention period of replication copies.</li>
        </ul>
       </div>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="mongodb-0011.html">Backing Up MongoDB Databases</a>
    </div>
   </div>
  </div>
 </body>
</html>