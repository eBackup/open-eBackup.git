<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 3: (Optional) Enabling Backup Link Encryption">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="mongodb-0011.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="mongodb-0014">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 3: (Optional) Enabling Backup Link Encryption</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="mongodb-0014"></a><a name="mongodb-0014"></a>

<h1 class="topictitle1">Step 3: (Optional) Enabling Backup Link Encryption</h1>
<div><p id="mongodb-0014__en-us_topic_0000001839143225_p105016314451">To ensure data security, you can enable backup link encryption. This function encrypts the data transmitted during data backup.</p>
<div class="section" id="mongodb-0014__en-us_topic_0000001839143225_section41258319515"><h4 class="sectiontitle">Prerequisites</h4><p id="mongodb-0014__en-us_topic_0000001839143225_p13640951351">Ensure that the NFS Kerberos service has been configured for the storage system and user <strong id="mongodb-0014__en-us_topic_0000001839143225_b1264194445814">rdadmin</strong> has been added to the AD domain. Otherwise, the backup job may fail after backup link encryption is enabled. <span id="mongodb-0014__en-us_topic_0000001839143225_ph527185394510">For details about how to configure the NFS Kerberos service, see "(Optional) Configuring the NFS Kerberos Service" in the <i><cite id="mongodb-0014__en-us_topic_0000001839143225_cite5907115716400">Installation Guide</cite></i> specific to your product model.</span></p>
</div>
<div class="section" id="mongodb-0014__en-us_topic_0000001839143225_section1376817483418"><h4 class="sectiontitle">Precautions</h4><p id="mongodb-0014__en-us_topic_0000001839143225_p1854315715349">After backup link encryption is enabled, the ticket between the agent host and the KDC domain server has a validity period. If the ticket expires, backup jobs may fail. You are advised to prolong the validity period of the ticket in the Kerberos policy on the KDC domain server or configure the ticket to never expire. If the ticket is not configured to never expire, you must run the <strong id="mongodb-0014__en-us_topic_0000001839143225_b195916246015">kinit</strong> or <strong id="mongodb-0014__en-us_topic_0000001839143225_b1659142413011">net</strong> command on the agent host to renew the ticket for each user added to the AD domain before it expires. For details, see "Configuring the Client" in the <i><cite id="mongodb-0014__en-us_topic_0000001839143225_cite890601917412">Installation Guide</cite></i> specific to your product model.</p>
</div>
<div class="section" id="mongodb-0014__en-us_topic_0000001839143225_section4718134149"><h4 class="sectiontitle">Procedure</h4><ol id="mongodb-0014__en-us_topic_0000001839143225_ol171184511428"><li id="mongodb-0014__en-us_topic_0000001839143225_li185661554204220"><span>Choose <span class="uicontrol" id="mongodb-0014__en-us_topic_0000001839143225_en-us_topic_0000001102065552_en-us_topic_0000001085869992_en-us_topic_0000001092505479_uicontrol123381932135316"><b><span id="mongodb-0014__en-us_topic_0000001839143225_en-us_topic_0000001102065552_en-us_topic_0000001085869992_en-us_topic_0000001092505479_text113848306235"><span id="mongodb-0014__en-us_topic_0000001839143225_en-us_topic_0000001102065552_text8949174614917"><strong>System</strong></span></span> &gt; <span id="mongodb-0014__en-us_topic_0000001839143225_en-us_topic_0000001102065552_en-us_topic_0000001085869992_text484521122916"><span id="mongodb-0014__en-us_topic_0000001839143225_en-us_topic_0000001102065552_text2943115917492"><strong>Security</strong></span></span> &gt; <span id="mongodb-0014__en-us_topic_0000001839143225_text6472734351"><strong>Data Security</strong></span></b></span>.</span></li><li id="mongodb-0014__en-us_topic_0000001839143225_li109511195437"><span>In the <strong id="mongodb-0014__en-us_topic_0000001839143225_b538931191418">Encryption Settings</strong> area, click <span class="uicontrol" id="mongodb-0014__en-us_topic_0000001839143225_uicontrol851575420436"><b><span id="mongodb-0014__en-us_topic_0000001839143225_text2172104153514"><strong>Modify</strong></span></b></span> on the right of the page and enable <span class="uicontrol" id="mongodb-0014__en-us_topic_0000001839143225_uicontrol13743170204412"><b><span id="mongodb-0014__en-us_topic_0000001839143225_text451824814419"><strong>Backup Link Encryption</strong></span></b></span>.</span></li><li id="mongodb-0014__en-us_topic_0000001839143225_li178813714443"><span>Click <span class="uicontrol" id="mongodb-0014__en-us_topic_0000001839143225_uicontrol1839019167443"><b><span id="mongodb-0014__en-us_topic_0000001839143225_text1418318566358"><strong>Save</strong></span></b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="mongodb-0011.html">Backing Up MongoDB Databases</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>