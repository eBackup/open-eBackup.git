<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Tenants">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="hcs_gud_0060.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="hcs_gud_0063">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Tenants</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="hcs_gud_0063"></a><a name="hcs_gud_0063"></a>
  <h1 class="topictitle1">Managing Tenants</h1>
  <div>
   <p>After a tenant is added to the <span>product</span>, you can perform operations on the tenant.</p>
   <p>Select <span class="uicontrol"><b><span id="hcs_gud_0063__en-us_topic_0000001839142377_text635183444114"><strong>Protection</strong></span> &gt; Cloud Platforms &gt; Huawei Cloud Stack</b></span>. On the <strong>Tenants</strong> tab page, find the tenant to be operated.</p>
   <p><a href="#hcs_gud_0063__table24934294919">Table 1</a> describes the related operations.</p>
   <div class="tablenoborder">
    <a name="hcs_gud_0063__table24934294919"></a><a name="table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="hcs_gud_0063__table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Tenant-related operations
     </caption>
     <colgroup>
      <col style="width:14.44%">
      <col style="width:57.730000000000004%">
      <col style="width:27.83%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="14.44%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="57.730000000000004%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="27.83%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Authorizing Huawei Cloud Stack resources</p>
        <div class="note">
         <span class="notetitle"> NOTE: </span>
         <div class="notebody">
          <p id="hcs_gud_0063__en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>By default, only the system administrator has the permission to protect resources. If you want to assign this permission to a data protection administrator, authorize the data protection administrator as the system administrator to protect resources.</p> <p><strong>Note</strong></p>
        <ul>
         <li id="hcs_gud_0063__en-us_topic_0000001839223137_li1491782615119">The protection permissions on a resource that has been associated with an SLA cannot be granted. Before permission granting, remove the protection.</li>
         <li id="hcs_gud_0063__en-us_topic_0000001839223137_li14274329155112">The protection permissions on a single resource can be granted only to a single data protection administrator.</li>
        </ul></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target tenant, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Authorize Resource</strong></span></b></span> and select the data protection administrator to be authorized.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Reclaiming Huawei Cloud Stack resources</p>
        <div class="note">
         <span class="notetitle"> NOTE: </span>
         <div class="notebody">
          <p id="hcs_gud_0063__en-us_topic_0000001839223137_p72172194413_1">Only version 1.5.0 supports this function.</p>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to reclaim resources authorized to the data protection administrator. The data protection administration is not permitted to protect the resources that have been reclaimed.</p> <p><strong>Note</strong></p> <p>The protection permissions on a resource that has been associated with an SLA cannot be reclaimed. Before reclamation, remove the protection.</p></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target tenant, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Reclaim Resource</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Scanning resources of a tenant</p></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>When resources of a tenant change, for example, projects/resource sets, ECSs, or EVS disks are added or deleted, you can perform this operation to manually scan resources.</p></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target tenant, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying a tenant</p></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to add a VDC administrator for a tenant, change the password of an existing VDC administrator, or delete an existing VDC administrator.</p></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target tenant, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p> <p>On the <span><strong>Modify</strong></span> page, you can add, modify, and delete VDC administrators.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Deleting a tenant</p></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>Perform this operation when resources of a tenant do not need to be protected.</p></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target tenant, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span> or select the tenants to be deleted and click <span><strong>Delete</strong></span> above the list.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Adding a tag
         <div class="note" id="hcs_gud_0063__en-us_topic_0000001792344090_note161679211561">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="hcs_gud_0063__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol" id="hcs_gud_0063__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="hcs_gud_0063__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note" id="hcs_gud_0063__en-us_topic_0000001792344090_note164681316118">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul id="hcs_gud_0063__en-us_topic_0000001792344090_ul22969251338">
            <li id="hcs_gud_0063__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="hcs_gud_0063__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="hcs_gud_0063__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
            <li id="hcs_gud_0063__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="hcs_gud_0063__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="hcs_gud_0063__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="hcs_gud_0063__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Removing a tag
         <div class="note" id="hcs_gud_0063__en-us_topic_0000001792344090_note4957114117575">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="hcs_gud_0063__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.730000000000004%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p></td>
       <td class="cellrowborder" valign="top" width="27.83%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="hcs_gud_0063__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="hcs_gud_0063__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="hcs_gud_0060.html">Huawei Cloud Stack Environment</a>
    </div>
   </div>
  </div>
 </body>
</html>