<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Restoring Files on an ECS">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="hcs_gud_0046.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="hcs_gud_re1">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Restoring Files on an ECS</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="hcs_gud_re1"></a><a name="hcs_gud_re1"></a>

<h1 class="topictitle1">Restoring Files on an ECS</h1>
<div><p>This section describes how to restore files on an ECS by using the file-level restoration function.</p>
<div class="section"><h4 class="sectiontitle">Context</h4><ul id="hcs_gud_re1__vmware_gud_0060_0_ul1684123102616"><li id="hcs_gud_re1__vmware_gud_0060_0_li15684153110268">The <span id="hcs_gud_re1__vmware_gud_0060_0_text10124544712">OceanProtect</span> supports file-level restoration using backup copies and replication copies. Note that data cannot be restored to the original location when a replication copy is used.</li><li id="hcs_gud_re1__vmware_gud_0060_0_li2871134142617">The types of Linux file systems that can be restored are ext2/ext3/ext4 and XFS. The types of Windows file systems that can be restored are NTFS, FAT, and FAT32.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><ul><li>The IP address of the backup network plane has been configured for the target ECS to which files are to be restored.</li><li>If the target VM runs the Windows OS, ensure that the CIFS service has been enabled on the VM. For details, visit the <a href="https://learn.microsoft.com/en-us/windows-server/storage/file-server/troubleshoot/detect-enable-and-disable-smbv1-v2-v3?tabs=server" target="_blank" rel="noopener noreferrer">Microsoft official website</a>.</li><li>If the target VM runs a non-Windows OS, ensure that rsync has been installed and started on the target VM.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="hcs_gud_re1__en-us_topic_0000001839142377_uicontrol2088361125016"><b><span id="hcs_gud_re1__en-us_topic_0000001839142377_text38837195013"><strong>Explore</strong></span> &gt; <span id="hcs_gud_re1__en-us_topic_0000001839142377_text888319135019"><strong>Copy Data</strong></span> &gt; <span id="hcs_gud_re1__en-us_topic_0000001839142377_text01201142214"><strong>Cloud Platforms</strong></span> &gt; <span id="hcs_gud_re1__en-us_topic_0000001839142377_text2995643181015"><strong>Huawei Cloud Stack</strong></span></b></span>.</span></li><li><span>You can search for a copy by VM or copy. This section uses a VM as an example.</span><p><p id="hcs_gud_re1__vmware_gud_0060_0_p624620403810">Click <strong id="hcs_gud_re1__vmware_gud_0060_0_b1858284474018">Resources</strong> to switch to the corresponding page, search for the VM to be restored by name, and click the VM name.</p>
</p></li><li><span>Click <span class="uicontrol" id="hcs_gud_re1__vmware_gud_0060_0_uicontrol1812414562220"><b><span id="hcs_gud_re1__vmware_gud_0060_0_text2022513145564"><strong>Copy Data</strong></span></b></span> and select the year, month, and day in sequence to find the copy.</span><p><p id="hcs_gud_re1__vmware_gud_0060_0_p9918150182310">If <span><img id="hcs_gud_re1__vmware_gud_0060_0_image99531319193912" src="en-us_image_0000001839187161.png"></span> is displayed below a month or date, copies exist in the month or on the date.</p>
</p></li><li><span>Locate the copy to be restored and choose <span><strong>More</strong></span> &gt; <span><strong>File-level Restoration</strong></span>.</span><p><div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul><li>For 1.5.0, the index status of the copy used for file-level restoration must be <span class="uicontrol" id="hcs_gud_re1__vmware_gud_0060_0_uicontrol483213401802"><b><span id="hcs_gud_re1__vmware_gud_0060_0_text1711017353520"><strong>Indexed</strong></span></b></span>. For 1.6.0 and later versions, copies whose index status is <span id="hcs_gud_re1__vmware_gud_0060_0_text413155718286"><strong>Not indexed</strong></span> can also be used for file-level restoration.</li><li>For 1.5.0, if automatic indexing has been enabled for the SLA associated with the VM corresponding to the copy, the index status of the copy is <span class="uicontrol" id="hcs_gud_re1__vmware_gud_0060_0_uicontrol2988201618593"><b><span id="hcs_gud_re1__vmware_gud_0060_0_text198816168595"><strong>Indexed</strong></span></b></span>, and file-level restoration can be directly performed. If automatic indexing is disabled, click <span class="uicontrol" id="hcs_gud_re1__vmware_gud_0060_0_uicontrol1036516011116"><b><span id="hcs_gud_re1__vmware_gud_0060_0_text31091220421"><strong>Manually Create Index</strong></span></b></span> and then perform file-level restoration.</li><li>A copy that does not contain system disks cannot be used for file-level restoration.</li><li>Reverse replication copies and archive copies cannot be used for file-level restoration.</li><li>Linked files do not support file-level restoration.</li><li>During Huawei Cloud Stack backup, the mount point information that is not written into the <strong>/etc/fstab</strong> file is stored in the memory. The <span>OceanProtect</span> does not back up the information and does not support file-level restoration. If new file systems need to support file-level restoration, write the mount point information of the new file systems to the <strong>/etc/fstab</strong> file.</li><li>If the name of a folder or file contains garbled characters, file-level restoration is not supported. Do not select folders or files of this type. Otherwise, the restoration job fails.</li><li>If the target ECS runs the Windows OS and only the SMBv1 service is enabled, the time of the restored file is the current ECS time.</li></ul>
</div></div>
</p></li><li><span>Select the files to be restored.</span><p><ul id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_ul1521452811317"><li id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_li8386311313">For 1.5.0, select the files to be restored from the directory tree.</li><li id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_li12142286316">For 1.6.0 and later versions, set <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b175942210539">File Obtaining Mode</strong>, which can be <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b29462035125211">Select file paths from the directory tree</strong> or <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b1211515508528">Enter file paths</strong>.<div class="note" id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_note1195634711290"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_ul14956124712297"><li id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_li1095674772911">If the copy contains too many files, the system may time out, preventing you from selecting files to be restored from the directory tree. Therefore, you are advised to enter the paths of files to be restored.</li><li id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_li19956104762918">When entering a file path, enter a complete file path, for example, <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b745510458333">/opt/abc/efg.txt</strong> or <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b186591048123315">C:\abc\efg.txt</strong>. If you enter a folder path, for example, <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b15108423133414">/opt/abc</strong> or <strong id="hcs_gud_re1__fc_gud_re1_0_vmware_gud_0060_0_b13283162543410">C:\abc</strong>, all files in the folder are restored. The file name in the path is case sensitive.</li></ul>
</div></div>
</li></ul>
</p></li><li><span>Select <strong>Original location</strong> or <strong>New location</strong> and set restoration parameters.</span><p><ul><li>Select the original location for restoration, that is, restore data to the original directory on the original ECS.<p><a href="#hcs_gud_re1__table9960822181311">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="hcs_gud_re1__table9960822181311"></a><a name="table9960822181311"></a><table cellpadding="4" cellspacing="0" summary="" id="hcs_gud_re1__table9960822181311" width="90%" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Parameters for restoring data to the original location</caption><colgroup><col style="width:29.9%"><col style="width:70.1%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="29.9%" id="mcps1.3.4.2.6.2.1.1.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="70.1%" id="mcps1.3.4.2.6.2.1.1.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.1 "><p><strong>Cloud Server IP Address</strong></p>
</td>
<td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.2 "><div class="p" id="hcs_gud_re1__p49594222135">Select the configured backup network plane IP address.<div class="note" id="hcs_gud_re1__note13959142281314"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="hcs_gud_re1__p1874334418">If the ECS IP address is not in the list, exit the current page. Then, choose <span class="uicontrol" id="hcs_gud_re1__uicontrol61481615124114"><b><span id="hcs_gud_re1__en-us_topic_0000001839142377_text635183444114"><strong>Protection</strong></span> &gt; Cloud Platforms &gt; Huawei Cloud Stack</b></span> and rescan the virtualization environment.</p>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.1 "><p><strong>Cloud Server Username</strong></p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="70.1%" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.2 "><div class="p" id="hcs_gud_re1__p196016226135">During file-level restoration, the target ECS needs to verify the login credential of the <span id="hcs_gud_re1__text11982215132215">OceanProtect</span>. Enter a username and password for logging in to the target ECS.<ul id="hcs_gud_re1__ul119609220135"><li id="hcs_gud_re1__li796017225136">Windows OS: The default username is <strong id="hcs_gud_re1__b18493022201419">Administrator</strong>.</li><li id="hcs_gud_re1__li1496012225130">Linux OS: The default username is <strong id="hcs_gud_re1__b1096019222138">root</strong>.</li></ul>
<div class="note" id="hcs_gud_re1__note322417291751"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="hcs_gud_re1__ul147340471325"><li id="hcs_gud_re1__li573414474324">The login user must have the read and write permissions on the directory to which data is to be restored on the target ECS.</li><li id="hcs_gud_re1__li198401648113219">After the restoration, the access permission on the file is the same as that of the login user.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.1 "><p><strong>Cloud Server Password</strong></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.1 "><p><strong>Overwrite Rule</strong></p>
</td>
<td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.4.2.6.2.1.1.2.2.3.1.2 "><div class="p">If a file with the same name exists in the restoration path, you can choose to replace or skip the existing file.<ul id="hcs_gud_re1__vmware_gud_0060_0_ul138776497307"><li id="hcs_gud_re1__vmware_gud_0060_0_li1087714914306"><span id="hcs_gud_re1__vmware_gud_0060_0_text2877849173018"><strong>Replace existing files</strong></span></li><li id="hcs_gud_re1__vmware_gud_0060_0_li1287717497308"><span id="hcs_gud_re1__vmware_gud_0060_0_text148771149123013"><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li><li id="hcs_gud_re1__vmware_gud_0060_0_li68771149143011"><span id="hcs_gud_re1__vmware_gud_0060_0_text5877184993017"><strong>Only replace the files older than the restoration file</strong></span><div class="note" id="hcs_gud_re1__vmware_gud_0060_0_note98771049113012"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="hcs_gud_re1__vmware_gud_0060_0_p15877144918305">If the access to the file with the same name on the target VM is denied, the file fails to be restored during file replacement.</p>
</div></div>
</li></ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Select a new location for restoration, that is, restore data to a specified VM.<p><a href="#hcs_gud_re1__table14691181871520">Table 2</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="hcs_gud_re1__table14691181871520"></a><a name="table14691181871520"></a><table cellpadding="4" cellspacing="0" summary="" id="hcs_gud_re1__table14691181871520" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Parameters for restoring data to a new location</caption><colgroup><col style="width:22.75%"><col style="width:77.25%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="22.75%" id="mcps1.3.4.2.6.2.1.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="77.25%" id="mcps1.3.4.2.6.2.1.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Target Cloud Platform</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><p>Select the cloud platform to which the data is restored.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Cloud Platform Tenant</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><p>Select a cloud platform tenant.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Region</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><p>Select the region to which the data is restored.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Project/Resource Set</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><p>Select the project/resource set to which the data is restored.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Cloud Server</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><p>Select the ECS to which the data is restored.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Cloud Server IP Address</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><div class="p">Select the configured backup network plane IP address.<div class="note" id="hcs_gud_re1__hcs_gud_re1_note13959142281314"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="hcs_gud_re1__hcs_gud_re1_p1874334418">If the ECS IP address is not in the list, exit the current page. Then, choose <span class="uicontrol" id="hcs_gud_re1__hcs_gud_re1_uicontrol61481615124114"><b><span id="hcs_gud_re1__hcs_gud_re1_en-us_topic_0000001839142377_text635183444114"><strong>Protection</strong></span> &gt; Cloud Platforms &gt; Huawei Cloud Stack</b></span> and rescan the virtualization environment.</p>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Cloud Server Username</strong></p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><div class="p">During file-level restoration, the target ECS needs to verify the login credential of the <span id="hcs_gud_re1__hcs_gud_re1_text11982215132215">OceanProtect</span>. Enter a username and password for logging in to the target ECS.<ul id="hcs_gud_re1__hcs_gud_re1_ul119609220135"><li id="hcs_gud_re1__hcs_gud_re1_li796017225136">Windows OS: The default username is <strong id="hcs_gud_re1__hcs_gud_re1_b18493022201419">Administrator</strong>.</li><li id="hcs_gud_re1__hcs_gud_re1_li1496012225130">Linux OS: The default username is <strong id="hcs_gud_re1__hcs_gud_re1_b1096019222138">root</strong>.</li></ul>
<div class="note" id="hcs_gud_re1__hcs_gud_re1_note322417291751"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="hcs_gud_re1__hcs_gud_re1_ul147340471325"><li id="hcs_gud_re1__hcs_gud_re1_li573414474324">The login user must have the read and write permissions on the directory to which data is to be restored on the target ECS.</li><li id="hcs_gud_re1__hcs_gud_re1_li198401648113219">After the restoration, the access permission on the file is the same as that of the login user.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Cloud Server Password</strong></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.1 "><p><strong>Overwrite Rule</strong></p>
</td>
<td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.6.2.1.2.2.2.3.1.2 "><div class="p">If a file with the same name exists in the restoration path, you can choose to replace or skip the existing file.<ul id="hcs_gud_re1__vmware_gud_0060_0_ul138776497307_1"><li id="hcs_gud_re1__vmware_gud_0060_0_li1087714914306_1"><span id="hcs_gud_re1__vmware_gud_0060_0_text2877849173018_1"><strong>Replace existing files</strong></span></li><li id="hcs_gud_re1__vmware_gud_0060_0_li1287717497308_1"><span id="hcs_gud_re1__vmware_gud_0060_0_text148771149123013_1"><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li><li id="hcs_gud_re1__vmware_gud_0060_0_li68771149143011_1"><span id="hcs_gud_re1__vmware_gud_0060_0_text5877184993017_1"><strong>Only replace the files older than the restoration file</strong></span><div class="note" id="hcs_gud_re1__vmware_gud_0060_0_note98771049113012_1"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="hcs_gud_re1__vmware_gud_0060_0_p15877144918305_1">If the access to the file with the same name on the target VM is denied, the file fails to be restored during file replacement.</p>
</div></div>
</li></ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</li></ul>
</p></li><li><span>Click <span class="uicontrol" id="hcs_gud_re1__vmware_gud_0060_0_uicontrol18891938492"><b><span id="hcs_gud_re1__vmware_gud_0060_0_text1252310490912"><strong>Test</strong></span></b></span> and ensure that the target VM to be restored is properly connected to the <span id="hcs_gud_re1__vmware_gud_0060_0_text12612599715">OceanProtect</span>.</span></li><li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="hcs_gud_0046.html">Restoration</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>