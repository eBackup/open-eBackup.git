<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring Files">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="acloud_00048.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="acloud_00053">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring Files</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="acloud_00053"></a><a name="acloud_00053"></a>
  <h1 class="topictitle1">Restoring Files</h1>
  <div>
   <p>This section describes how to restore files on a VM by using the file-level restoration function.</p>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="acloud_00053__acloud_00047_en-us_topic_0000001839142377_uicontrol743231135612"><b><span id="acloud_00053__acloud_00047_en-us_topic_0000001839142377_text943133112569"><strong>Explore</strong></span> &gt; <span id="acloud_00053__acloud_00047_en-us_topic_0000001839142377_text20436317563"><strong>Copy Data</strong></span> &gt; <span id="acloud_00053__acloud_00047_en-us_topic_0000001839142377_text114463112568"><strong>Cloud Platforms</strong></span> &gt; Alibaba Cloud</b></span>.</span></li>
     <li><span>Search for copies by resource or copy. This section describes how to search for copies by resource.</span><p></p><p>Click the <strong>Resources</strong> tab, search for the VM to be restored by name, and click the VM name.</p> <p></p></li>
     <li><span>Click the <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> tab and select the year, month, and day in sequence to find the copy.</span><p></p><p>If <span><img src="en-us_image_0000001896388017.png"></span> is displayed below a month or day, a copy is generated in the month or on the day.</p> <p></p></li>
     <li><span>Find the copy used for restoration and click <span class="uicontrol"><b><span><strong>Copy Time</strong></span></b></span>.</span></li>
     <li><span>On the copy information page, choose <span class="uicontrol"><b><span><strong>Operation</strong></span> &gt; <span><strong>File-level Restoration</strong></span></b></span>. Alternatively, on the <strong>Resources</strong> tab page, locate the row that contains the target copy and choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>File-level Restoration</strong></span></b></span>.</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p>Linked files do not support file-level restoration.</p>
       </div>
      </div> <p></p></li>
     <li><span>On the displayed page, click the <span class="wintitle" id="acloud_00053__vmware_gud_0060_0_wintitle15956154715296"><b><span id="acloud_00053__vmware_gud_0060_0_text10956184782910"><strong>File</strong></span></b></span> tab.</span></li>
     <li><span>Set <strong>File Obtaining Mode</strong>, which can be <strong>Select file paths from the directory tree</strong> or <strong>Enter file paths</strong>.</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <ul id="acloud_00053__vmware_gud_0060_0_ul14956124712297">
         <li id="acloud_00053__vmware_gud_0060_0_li1095674772911">If the copy contains too many files, the system may time out, preventing you from selecting files to be restored from the directory tree. Therefore, you are advised to enter the paths of files to be restored.</li>
         <li id="acloud_00053__vmware_gud_0060_0_li19956104762918">When entering a file path, enter a complete file path, for example, <strong id="acloud_00053__vmware_gud_0060_0_b745510458333">/opt/abc/efg.txt</strong> or <strong id="acloud_00053__vmware_gud_0060_0_b186591048123315">C:\abc\efg.txt</strong>. If you enter a folder path, for example, <strong id="acloud_00053__vmware_gud_0060_0_b15108423133414">/opt/abc</strong> or <strong id="acloud_00053__vmware_gud_0060_0_b13283162543410">C:\abc</strong>, all files in the folder are restored. The file name in the path is case sensitive.</li>
        </ul>
       </div>
      </div> <p></p></li>
     <li><span>On the displayed <strong>Restore File</strong> page, select <strong>Original location</strong> or <strong>New location</strong> for <strong>Restore To</strong> and set restoration parameters.</span><p></p><p><a href="#acloud_00053__table1413423417495">Table 1</a> describes the related parameters.</p> <p></p> <p></p>
      <div class="tablenoborder">
       <a name="acloud_00053__table1413423417495"></a><a name="table1413423417495"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="acloud_00053__table1413423417495" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>File-level restoration parameters
        </caption>
        <colgroup>
         <col style="width:29.9%">
         <col style="width:70.1%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="29.9%" id="mcps1.3.2.2.8.2.4.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="70.1%" id="mcps1.3.2.2.8.2.4.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.2.2.8.2.4.2.3.1.1 "><p><span><strong>Target Location</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.2.2.8.2.4.2.3.1.2 "><p>Location of the VM to which the file is restored.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>Ensure that the OS of the target location is the same as that of the VM to be restored.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.2.2.8.2.4.2.3.1.1 "><p><span><strong>Cloud Server IP Address</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.2.2.8.2.4.2.3.1.2 "><p>Select or manually enter the IP address of the target VM.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.2.2.8.2.4.2.3.1.1 "><p><span><strong>Cloud Server Username</strong></span></p></td>
          <td class="cellrowborder" rowspan="2" valign="top" width="70.1%" headers="mcps1.3.2.2.8.2.4.2.3.1.2 ">
           <div class="p">
            During file-level restoration, the target VM needs to verify the login credential of the <span>product</span>. Enter a username and its password for logging in to the target VM.
            <ul>
             <li>Windows OS: The default username is <strong>Administrator</strong>.</li>
             <li>Linux OS: The default username is <strong>root</strong>.</li>
            </ul>
            <div class="note">
             <span class="notetitle"> NOTE: </span>
             <div class="notebody">
              <ul id="acloud_00053__vmware_gud_0060_0_ul871974405518">
               <li id="acloud_00053__vmware_gud_0060_0_li8719444175514">The login user must have the read and write permissions on the directory to which data is to be restored on the target VM.</li>
               <li id="acloud_00053__vmware_gud_0060_0_li231424655513">After the restoration, the access permission on the file is the same as that of the login user.</li>
              </ul>
             </div>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.2.2.8.2.4.2.3.1.1 "><p><span><strong>Cloud Server Password</strong></span></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.2.2.8.2.4.2.3.1.1 "><p><span><strong>OS</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.2.2.8.2.4.2.3.1.2 "><p>Select the OS of the target VM. The options are <strong>Linux</strong> and <strong>Windows</strong>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.2.2.8.2.4.2.3.1.1 "><p><span><strong>Overwrite Rule</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.2.2.8.2.4.2.3.1.2 "><p>If files with the same names exist in the restoration path, you can choose to replace or skip existing files.</p>
           <ul>
            <li><strong>Replace existing files</strong></li>
            <li><strong>Skip existing files</strong></li>
            <li><strong>Only replace the files older than the restoration file</strong></li>
           </ul>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>If the access to the file with the same name on the target VM is denied, the file will fail to be restored during file replacement.</p>
            </div>
           </div></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Test</strong></span></b></span> and ensure that the network connection between the target VM to be restored and the <span>product</span> is normal.</span></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="acloud_00048.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>