<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Availability Zones, Resource Sets, or Cloud Servers">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="acloud_00064.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="acloud_00067">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Availability Zones, Resource Sets, or Cloud Servers</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="acloud_00067"></a><a name="acloud_00067"></a>
  <h1 class="topictitle1">Managing Availability Zones, Resource Sets, or Cloud Servers</h1>
  <div>
   <p>The <span>product</span> allows you to modify, remove, activate, or disable protection for AZs, resource sets, or cloud servers. This section uses an AZ as an example to describe related operations. For resource sets or cloud servers, the operations are the same.</p>
   <p>You need to log in to the management page and choose <span class="uicontrol"><b><span id="acloud_00067__en-us_topic_0000001839142377_text4527517191713"><strong>Protection</strong></span> &gt; Cloud Platforms &gt; Alibaba Cloud</b></span>. On the <span class="uicontrol"><b>AZs</b></span> tab page, locate the target AZ.</p>
   <p><a href="#acloud_00067__table24934294919">Table 1</a> describes the related operations.</p>
   <div class="tablenoborder">
    <a name="acloud_00067__table24934294919"></a><a name="table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="acloud_00067__table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Project protection plan operations
     </caption>
     <colgroup>
      <col style="width:14.44%">
      <col style="width:57.78%">
      <col style="width:27.779999999999998%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="14.44%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="57.78%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="27.779999999999998%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying protection</p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can modify a protection plan to re-associate a project with an SLA or modify agent host information. After the modification, the next backup will be performed based on the new protection plan.</p> <p><strong>Note</strong></p>
        <ul>
         <li id="acloud_00067__hcs_gud_0064_li11841562511">The modification does not affect the protection job that is being executed. The modified protection plan will take effect in the next protection period.</li>
         <li id="acloud_00067__hcs_gud_0064_li18622582511">If a backup job has been performed, reconfigure the SLA policy (change to the SLA with copy verification enabled) when modifying the protection plan. After the modification, the initial backup job will be converted to a full backup job if it is a non-full backup job.</li>
         <li id="acloud_00067__hcs_gud_0064_li12711557142017">If the SLA parameter associated with the resource changes (from copy verification disabled to copy verification enabled) after a backup job has been performed, when the protection plan for the resource is implemented after the change, the initial backup job will be converted to a full backup job if it is a non-full backup job.</li>
        </ul></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span> in the row that contains the target AZ.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Removing protection</p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can remove protection for an AZ that no longer needs to be protected. After the removal, the system cannot execute protection jobs for the AZ. To protect the AZ again, you need to associate an SLA with the AZ again.</p> <p><strong>Note</strong></p> <p>If a protection job of the AZ is in progress, protection cannot be removed.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span> in the row that contains the target AZ.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Performing a manual backup</p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span> in the row that contains the target AZ.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Activating protection</p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to activate a disabled protection plan for an AZ. After the protection plan is activated, the system executes backup, archiving, or replication for the AZ based on the plan.</p> <p><strong>Note</strong></p> <p>This operation can be performed only for an AZ that has been associated with an SLA and is in the <span class="uicontrol"><b><span><strong>Unprotected</strong></span></b></span> status.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span> in the row that contains the target AZ.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Disabling protection</p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to disable the protection plan of an AZ for which periodic protection is not required. After the protection plan is disabled, the system does not execute automatic backup, archiving, or replication for the AZ.</p> <p><strong>Note</strong></p> <p>Disabling a protection plan does not affect ongoing protection jobs.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span> in the row that contains the target AZ.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Adding a tag
         <div class="note" id="acloud_00067__en-us_topic_0000001792344090_note161679211561">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="acloud_00067__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="acloud_00067__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol" id="acloud_00067__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="acloud_00067__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note" id="acloud_00067__en-us_topic_0000001792344090_note164681316118">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul id="acloud_00067__en-us_topic_0000001792344090_ul22969251338">
            <li id="acloud_00067__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="acloud_00067__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="acloud_00067__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
            <li id="acloud_00067__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="acloud_00067__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="acloud_00067__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="acloud_00067__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Removing a tag
         <div class="note" id="acloud_00067__en-us_topic_0000001792344090_note4957114117575">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="acloud_00067__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="acloud_00067__en-us_topic_0000001792344090_b472911291311">Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="acloud_00067__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="acloud_00067__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p>Scanning Alibaba Cloud AZs</p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>The <span>product</span> automatically scans AZs every hour. If any projects or cloud servers on the registered Alibaba Cloud platform change, the change will be automatically updated to the <span>product</span> after the scan. This operation enables you to immediately update the changed information to the <span>product</span>.</p> <p><strong>Note</strong></p> <p>If you do not select to apply an SLA to the newly created cloud server when creating a periodic backup job, the system will not associate the newly discovered cloud server with an SLA after the scan.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span>.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="acloud_00064.html">Alibaba Cloud Environment Information</a>
    </div>
   </div>
  </div>
 </body>
</html>