<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing the GaussDB (DWS) Cluster">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="DWS_00059.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="DWS_00061">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing the GaussDB (DWS) Cluster</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="DWS_00061"></a><a name="DWS_00061"></a>

<h1 class="topictitle1">Managing the GaussDB (DWS) Cluster</h1>
<div><p>On the <span>OceanProtect</span>, you can modify or delete registered GaussDB (DWS) clusters and schema sets or table sets in the clusters.</p>
<div class="section"><h4 class="sectiontitle">Related Operations</h4><p>Log in to the management page, choose <span class="uicontrol"><b><span id="DWS_00061__en-us_topic_0000001839142377_text4651950123919"><strong>Protection</strong></span> &gt; Big Data &gt; GaussDB (DWS)</b></span>, click the <span class="uicontrol"><b><span><strong>Clusters</strong></span></b></span>, <span class="uicontrol"><b><span><strong>Schema Sets</strong></span></b></span>, or <span class="uicontrol"><b><span><strong>Table Sets</strong></span></b></span> tab, and find the cluster on which you want to perform related operations.</p>
<p><a href="#DWS_00061__en-us_topic_0000001397321761_table24934294919">Table 1</a> describes the related operations.</p>
</div>

<div class="tablenoborder"><a name="DWS_00061__en-us_topic_0000001397321761_table24934294919"></a><a name="en-us_topic_0000001397321761_table24934294919"></a><table cellpadding="4" cellspacing="0" summary="" id="DWS_00061__en-us_topic_0000001397321761_table24934294919" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Operations related to the GaussDB (DWS) cluster</caption><colgroup><col style="width:20%"><col style="width:53.42%"><col style="width:26.58%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="20%" id="mcps1.3.3.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="53.42%" id="mcps1.3.3.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="26.58%" id="mcps1.3.3.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Modifying cluster information</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation allows you to modify the username, environment variable file path, node, and agent host of a cluster.</p>
<p><strong>Note</strong></p>
<ul><li>The cluster name cannot be modified.</li><li>If the cluster is associated with a schema or table object, only the node and agent host can be modified.</li><li>If the cluster is not running or is abnormal during the modification, the cluster can be modified, but the SLA policy cannot be added.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p>
<p>For details about GaussDB (DWS) cluster parameters, see <a href="DWS_00014.html">Step 1: Registering a GaussDB (DWS) Cluster</a>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Removing protection</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to remove protection for databases or table sets in a GaussDB (DWS) cluster when the protection is no longer required. After the removal, the system cannot execute protection jobs for the GaussDB (DWS) cluster. To protect the backup set again, associate an SLA with it again.</p>
<p><strong>Note</strong></p>
<p>When the protection job of the database is running, protection cannot be removed.</p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Deleting cluster information</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to delete a GaussDB (DWS) cluster if you no longer need to protect databases or table sets in it or restore data to it.</p>
<p><strong>Note</strong></p>
<ul><li>A cluster cannot be deleted if a backup or restoration job is being executed.</li><li>A cluster cannot be deleted if the cluster or a database, schema set, or table set in the cluster has been associated with an SLA.</li><li>Deletion of a cluster will delete all its databases, schemas, and tables.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Scanning resources</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>The OceanProtect Appliance automatically scans the GaussDB (DWS) environment every 5 minutes. If the registered GaussDB (DWS) changes, the change is automatically updated to the OceanProtect Appliance after scanning. You can perform this operation to immediately update the changed information to the OceanProtect Appliance.</p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <strong>More</strong> &gt; <strong>Resource Scan</strong>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Testing connectivity</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>Manual connectivity test of the registered GaussDB (DWS) environment is used to check the connectivity between the OceanProtect Appliance and the agent host and between the OceanProtect Appliance and the production environment.</p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <strong>More</strong> &gt; <strong>Test Connectivity</strong>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Modifying a schema set or table set</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to modify a schema set or table set.</p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row where the schema set or table set resides, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><p>Deleting a schema set or table set</p>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to delete a schema set or table set if you no longer need to protect it.</p>
<p><strong>Note</strong></p>
<p>A schema set or table set associated with an SLA cannot be deleted.</p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>In the row where the schema set or table set resides, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="DWS_00061__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="DWS_00061__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong id="DWS_00061__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="DWS_00061__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="DWS_00061__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="DWS_00061__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="DWS_00061__en-us_topic_0000001792344090_ul22969251338"><li id="DWS_00061__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="DWS_00061__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="DWS_00061__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="DWS_00061__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="DWS_00061__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="DWS_00061__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="DWS_00061__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="20%" headers="mcps1.3.3.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="DWS_00061__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="DWS_00061__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="53.42%" headers="mcps1.3.3.2.4.1.2 "><p><strong id="DWS_00061__en-us_topic_0000001792344090_b1495945913127_1">Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
<p></p>
</td>
<td class="cellrowborder" valign="top" width="26.58%" headers="mcps1.3.3.2.4.1.3 "><p>Choose <span class="uicontrol" id="DWS_00061__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="DWS_00061__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="DWS_00059.html">GaussDB (DWS) Cluster Environment</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>