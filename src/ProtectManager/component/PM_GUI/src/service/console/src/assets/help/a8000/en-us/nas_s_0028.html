<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 8: Enabling the NFSv4.1 Service (Applicable to X Series Backup Appliances)">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="nas_s_0020.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0028">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 8: Enabling the NFSv4.1 Service (Applicable to X Series Backup Appliances)</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0028"></a><a name="nas_s_0028"></a>

<h1 class="topictitle1">Step 8: Enabling the NFSv4.1 Service (Applicable to X Series Backup Appliances)</h1>
<div><p>Before performing NAS backup, you must enable the NFSv4.1 service. Otherwise, the backup will fail.</p>
<div class="section"><h4 class="sectiontitle">Logging In to DeviceManager</h4><ol><li><span>Choose <strong>System</strong> &gt; <strong>Infrastructure</strong> &gt; <strong>Cluster Management</strong>.</span></li><li><span>On the <strong>Backup Clusters</strong> tab page, click a node name under the <strong>Local Cluster Nodes</strong> area.</span></li><li><span>On the displayed <strong>Node Details</strong> page, click <strong>Open the device management platform</strong> to go to DeviceManager.</span></li></ol>
</div>
<div class="section" id="nas_s_0028__section16941161934519"><h4 class="sectiontitle">Enabling the NFSv4.1 Service on DeviceManager</h4><ol id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_ol9474161314358"><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li6474813183518"><span>Choose <strong id="nas_s_0028__b1438153784513">Settings</strong> &gt; <strong id="nas_s_0028__b19439203734515">File Service</strong> &gt; <strong id="nas_s_0028__b044011377457">NFS Service</strong>.</span></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li0195181917353"><span>In the <strong id="nas_s_0028__b7153132674613">vStore</strong> drop-down box in the upper left, select the vStore for which you want to enable the NFSv4 service.</span></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li184362365364"><span>Click <strong id="nas_s_0028__b124793342464">Modify</strong> in the upper right.</span><p><p id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_p8880155893611">The page for configuring the NFS service is displayed.</p>
<p id="nas_s_0028__en-us_topic_0000001292005288_p1648016318135"><span><img id="nas_s_0028__en-us_topic_0000001292005288_image369813213135" src="en-us_image_0000001792360766.png"></span></p>
<div class="note" id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_en-us_topic_0000001138838698_en-us_topic_0274231677_en-us_topic_0272257898_en-us_topic_0164720663_note12780141103215"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_en-us_topic_0000001138838698_en-us_topic_0274231677_en-us_topic_0272257898_en-us_topic_0164720663_p7780114114325">The screenshot is for reference only and the actual displayed information may vary.</p>
</div></div>
</p></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li8991419123516"><span>Enable the NFSv4.1 service.</span></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li1475916319252"><span>Set <strong id="nas_s_0028__b1772955715586">Domain Name</strong> to the name of the storage domain.</span><p><div class="note" id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_note685462219540"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_ul8782423175419"><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li478222345416">NFSv4.1 uses a username + domain name mapping mechanism, enhancing the security of clients' access to shared resources.</li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li97824238541">In a non-domain or LDAP environment, retain the default domain name <strong id="nas_s_0028__b45212351013">localdomain</strong>.</li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li778292385410">In an NIS environment, the entered information must be the same as the domain name in the <strong id="nas_s_0028__b1363975210011">/etc/idmapd.conf</strong> file on the Linux client that accesses the share. (You are advised to set both of them to the NIS domain name.)</li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li878212312549">The domain name contains a maximum of 64 characters.</li></ul>
</div></div>
</p></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li622882011356"><span>Click <strong id="nas_s_0028__b1418883215213">Save</strong>.</span><p><p id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_p136851159913">A <strong id="nas_s_0028__b18780435324">Danger</strong> dialog box is displayed.</p>
<div class="notice" id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_note182429551426"><span class="noticetitle"><img src="public_sys-resources/notice_3.0-en-us.png"> </span><div class="noticebody"><p id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_p424210551026">If a host is accessing the shares of the storage system, enabling or disabling the NFS service may interrupt services. Exercise caution when performing this operation.</p>
</div></div>
</p></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li11450192018359"><span>Confirm the information in the dialog box and select <strong id="nas_s_0028__b10203412155812">I have read and understand the consequences associated with performing this operation</strong>.</span></li><li id="nas_s_0028__en-us_topic_0000001292005288_en-us_topic_0000001223878385_li129656181229"><span>Click <strong id="nas_s_0028__b14384834637">OK</strong>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="nas_s_0020.html">Backing Up a NAS Share</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>