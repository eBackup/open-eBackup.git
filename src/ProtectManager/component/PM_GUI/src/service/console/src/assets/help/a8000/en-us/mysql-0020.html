<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 7: (Optional) Enabling Backup Link Encryption">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="mysql-0011.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="documenttype" content="usermanual">
<meta name="prodname" content="csbs">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="mysql-0020">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 7: (Optional) Enabling Backup Link Encryption</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="mysql-0020"></a><a name="mysql-0020"></a>

<h1 class="topictitle1">Step 7: (Optional) Enabling Backup Link Encryption</h1>
<div><p>You need to enable backup link encryption if required.</p>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><p>Ensure that the NFS Kerberos service has been configured for the storage system, and the username used for <a href="mysql-0018.html">resource registration</a> has been added to the AD domain. Otherwise, the backup job may fail after backup link encryption is enabled. <span>For details about how to configure the NFS Kerberos service, see "(Optional) Configuring the NFS Kerberos Service" in the <i><cite id="mysql-0020__en-us_topic_0000001839143225_cite5907115716400">Installation Guide</cite></i> specific to your product model.</span></p>
</div>
<div class="section"><h4 class="sectiontitle">Precautions</h4><p id="mysql-0020__en-us_topic_0000001839143225_p1854315715349">After backup link encryption is enabled, the ticket between the agent host and the KDC domain server has a validity period. If the ticket expires, backup jobs may fail. You are advised to prolong the validity period of the ticket in the Kerberos policy on the KDC domain server or configure the ticket to never expire. If the ticket is not configured to never expire, you must run the <strong id="mysql-0020__en-us_topic_0000001839143225_b195916246015">kinit</strong> or <strong id="mysql-0020__en-us_topic_0000001839143225_b1659142413011">net</strong> command on the agent host to renew the ticket for each user added to the AD domain before it expires. For details, see "Configuring the Client" in the <i><cite id="mysql-0020__en-us_topic_0000001839143225_cite890601917412">Installation Guide</cite></i> specific to your product model.</p>
</div>
<div class="section"><h4 class="sectiontitle">Creating a Local UNIX Authentication User Group</h4><p>Before enabling backup link encryption, if the current environment is not added to any domain environment, you need to add the user group to the local authentication user group.</p>
<ol><li><span>Log in to DeviceManager.</span><p><ul><li>Perform the following operations for OceanProtect X series backup appliances:<ol type="a"><li>Choose <strong id="mysql-0020__en-us_topic_0000001839223165_b124231410468">System</strong> &gt; <strong id="mysql-0020__en-us_topic_0000001839223165_b233321694610">Infrastructure</strong> &gt; <strong id="mysql-0020__en-us_topic_0000001839223165_b1744421834613">Cluster Management</strong>.</li><li>On the <strong id="mysql-0020__en-us_topic_0000001839223165_b4329457314">Backup Clusters</strong> tab page, click a node name under the <strong id="mysql-0020__en-us_topic_0000001839223165_b1632917571116">Local Cluster Nodes</strong> area.</li><li>On the displayed <strong id="mysql-0020__en-us_topic_0000001839223165_b0285722121516">Node Details</strong> page, click <strong id="mysql-0020__en-us_topic_0000001839223165_b142851122171511">Open the device management platform</strong> to go to DeviceManager.</li></ol>
</li></ul>
<ul><li>For OceanProtect E1000, log in to DeviceManager by referring to <a href="mysql-00820.html">Logging In to DeviceManager</a>.</li></ul>
</p></li><li><span>Choose <strong>Services</strong> &gt; <strong>File Service</strong> &gt; <strong>Authentication Users</strong>.</span></li><li><span>On the <span class="uicontrol"><b>UNIX Users</b></span> tab page, select <span class="uicontrol"><b>Local Authentication User Group</b></span>.</span></li><li><span>Click <span class="uicontrol"><b>Create</b></span>.</span></li><li><span>Configure local UNIX authentication user group parameters.</span><p><ul><li><strong>Name</strong>: Group to which the user belongs, which is set during database installation.</li><li><strong>ID</strong>: Run the <strong>cat /etc/group</strong> command to view the ID of the group to which the user belongs.</li></ul>
</p></li></ol>
</div>
<div class="section"><h4 class="sectiontitle">Enabling Backup Link Encryption</h4><ol><li><span>Choose <span class="uicontrol" id="mysql-0020__en-us_topic_0000001839143225_en-us_topic_0000001102065552_en-us_topic_0000001085869992_en-us_topic_0000001092505479_uicontrol123381932135316"><b><span id="mysql-0020__en-us_topic_0000001839143225_en-us_topic_0000001102065552_en-us_topic_0000001085869992_en-us_topic_0000001092505479_text113848306235"><span id="mysql-0020__en-us_topic_0000001839143225_en-us_topic_0000001102065552_text8949174614917"><strong>System</strong></span></span> &gt; <span id="mysql-0020__en-us_topic_0000001839143225_en-us_topic_0000001102065552_en-us_topic_0000001085869992_text484521122916"><span id="mysql-0020__en-us_topic_0000001839143225_en-us_topic_0000001102065552_text2943115917492"><strong>Security</strong></span></span> &gt; <span id="mysql-0020__en-us_topic_0000001839143225_text6472734351"><strong>Data Security</strong></span></b></span>.</span></li><li><span>In the <strong id="mysql-0020__en-us_topic_0000001839143225_b538931191418">Encryption Settings</strong> area, click <span class="uicontrol" id="mysql-0020__en-us_topic_0000001839143225_uicontrol851575420436"><b><span id="mysql-0020__en-us_topic_0000001839143225_text2172104153514"><strong>Modify</strong></span></b></span> on the right of the page and enable <span class="uicontrol" id="mysql-0020__en-us_topic_0000001839143225_uicontrol13743170204412"><b><span id="mysql-0020__en-us_topic_0000001839143225_text451824814419"><strong>Backup Link Encryption</strong></span></b></span>.</span></li><li><span>Click <span class="uicontrol" id="mysql-0020__en-us_topic_0000001839143225_uicontrol1839019167443"><b><span id="mysql-0020__en-us_topic_0000001839143225_text1418318566358"><strong>Save</strong></span></b></span>.</span></li></ol>
</div>
<p></p>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="mysql-0011.html">Backing Up the MySQL/MariaDB/GreatSQL Database</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>