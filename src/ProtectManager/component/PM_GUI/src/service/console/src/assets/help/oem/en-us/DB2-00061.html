<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing DB2 Databases or Tablespace Sets">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="DB2-00058.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="DB2-00061">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing DB2 Databases or Tablespace Sets</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="DB2-00061"></a><a name="DB2-00061"></a>
  <h1 class="topictitle1">Managing DB2 Databases or Tablespace Sets</h1>
  <div>
   <p>The <span>product</span> allows you to modify, remove, activate, or disable protection for DB2 databases or tablespace sets.</p>
   <div class="section">
    <h4 class="sectiontitle">Related Operations</h4>
    <p>Log in to the management page, choose <span class="uicontrol"><b><span id="DB2-00061__en-us_topic_0000001839142377_text4379114911434"><strong>Protection</strong></span> &gt; Databases &gt; DB2</b></span>, click the <span class="uicontrol"><b><span><strong>Databases</strong></span></b></span> or <span class="uicontrol"><b><span><strong>Tablespace Set</strong></span></b></span> tab, and locate the database or tablespace set on which you want to perform related operations.</p>
   </div>
   <p><a href="#DB2-00061__table24934294919">Table 1</a> describes the related operations.</p>
   <div class="tablenoborder">
    <a name="DB2-00061__table24934294919"></a><a name="table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="DB2-00061__table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Operations performed on database/tablespace set protection plans
     </caption>
     <colgroup>
      <col style="width:14.35%">
      <col style="width:57.92%">
      <col style="width:27.73%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="14.35%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="57.92%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="27.73%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Modify Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Context</strong></p> <p>If you need to reselect an SLA associated with a database or tablespace set, you can modify the protection plan. After the modification, the next backup will be performed based on the new protection plan.</p> <p><strong>Note</strong></p> <p>The modification does not affect the protection job that is being performed. The modified protection plan will take effect in the next protection period.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database or tablespace set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Remove Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Context</strong></p> <p>This operation enables you to remove protection for a database or tablespace set when it does not need to be protected. After the removal, the system cannot perform protection jobs for the database or tablespace set. To protect the database or tablespace set again, associate an SLA with it again.</p> <p><strong>Note</strong></p> <p>Protection cannot be removed when a protection job of the database or tablespace set is running.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database or tablespace set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Activate Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Context</strong></p> <p>This operation enables you to activate a disabled protection plan of a database or tablespace set. After the protection plan is activated, the system backs up the database or tablespace set based on the protection plan.</p> <p><strong>Note</strong></p> <p>You can perform this operation only on a database or tablespace set that has been associated with an SLA and is in the <span class="uicontrol"><b><span><strong>Unprotected</strong></span></b></span> state.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database or tablespace set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Disable Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Context</strong></p> <p>This operation enables you to disable the protection plan for a database or tablespace set when its periodic protection is not needed. After the protection plan is disabled, the system does not perform the automatic backup job for the database or tablespace set.</p> <p><strong>Note</strong></p> <p>Disabling protection does not affect ongoing backup jobs.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database or tablespace set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Manual Backup</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Context</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target database or tablespace set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Adding a tag
         <div class="note" id="DB2-00061__en-us_topic_0000001792344090_note161679211561">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="DB2-00061__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol" id="DB2-00061__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="DB2-00061__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note" id="DB2-00061__en-us_topic_0000001792344090_note164681316118">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul id="DB2-00061__en-us_topic_0000001792344090_ul22969251338">
            <li id="DB2-00061__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="DB2-00061__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="DB2-00061__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
            <li id="DB2-00061__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="DB2-00061__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="DB2-00061__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="DB2-00061__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.35%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Removing a tag
         <div class="note" id="DB2-00061__en-us_topic_0000001792344090_note4957114117575">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="DB2-00061__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="57.92%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p></td>
       <td class="cellrowborder" valign="top" width="27.73%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="DB2-00061__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="DB2-00061__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.4.2.4.1.1 mcps1.3.4.2.4.1.2 mcps1.3.4.2.4.1.3 "><p>Note: The procedure for restoring DB2 databases or tablespace sets is similar to that in <a href="DB2-00047.html">Restoring DB2 Databases or Tablespace Sets</a> and is not described here.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="DB2-00058.html">DB2 Cluster Environment</a>
    </div>
   </div>
  </div>
 </body>
</html>