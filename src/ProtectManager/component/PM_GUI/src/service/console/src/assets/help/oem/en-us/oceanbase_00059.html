<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Clusters">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="oceanbase_00057.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="oceanbase_00059">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Clusters</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="oceanbase_00059"></a><a name="oceanbase_00059"></a>
  <h1 class="topictitle1">Managing Clusters</h1>
  <div>
   <p>The <span>product</span> allows you to modify, remove, activate, or disable protection for clusters.</p>
   <div class="section">
    <h4 class="sectiontitle">Related Operations</h4>
    <p>Log in to the management page, choose <span class="uicontrol"><b><span id="oceanbase_00059__en-us_topic_0000001839142377_text19536165671619"><strong>Protection</strong></span> &gt; Databases &gt; OceanBase</b></span>, click <span><strong>Clusters</strong></span>, and find the target cluster.</p>
    <p><a href="#oceanbase_00059__en-us_topic_0000001263934006_table24934294919">Table 1</a> describes the related operations.</p>
    <div class="tablenoborder">
     <a name="oceanbase_00059__en-us_topic_0000001263934006_table24934294919"></a><a name="en-us_topic_0000001263934006_table24934294919"></a>
     <table cellpadding="4" cellspacing="0" summary="" id="oceanbase_00059__en-us_topic_0000001263934006_table24934294919" frame="border" border="1" rules="all">
      <caption>
       <b>Table 1 </b>Cluster protection operations
      </caption>
      <colgroup>
       <col style="width:25%">
       <col style="width:50%">
       <col style="width:25%">
      </colgroup>
      <thead align="left">
       <tr>
        <th align="left" class="cellrowborder" valign="top" width="25%" id="mcps1.3.2.4.2.4.1.1"><p>Operation</p></th>
        <th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.2.4.2.4.1.2"><p>Description</p></th>
        <th align="left" class="cellrowborder" valign="top" width="25%" id="mcps1.3.2.4.2.4.1.3"><p>Navigation Path</p></th>
       </tr>
      </thead>
      <tbody>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Modify Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to modify a protection plan to perform SLA association again for a cluster or modify user-defined script information. After the modification, the next backup will be performed based on the new protection plan.</p> <p><strong>Note</strong></p>
         <ul>
          <li id="oceanbase_00059__en-us_topic_0000001792344090_li16343131892419">The modification does not affect the protection job that is being executed. The modified protection plan will apply to the next protection period.</li>
          <li id="oceanbase_00059__en-us_topic_0000001792344090_li140522162517">If the protection policy for a resource in the associated SLA changes, you are advised to modify the current SLA on the <span class="uicontrol" id="oceanbase_00059__en-us_topic_0000001792344090_uicontrol10405524254"><b><span id="oceanbase_00059__en-us_topic_0000001792344090_text6405425255"><strong>Protection</strong></span> &gt; <span id="oceanbase_00059__en-us_topic_0000001792344090_text885333645815"><strong>Protection Policies</strong></span> &gt; <span id="oceanbase_00059__en-us_topic_0000001792344090_text164051025259"><strong>SLAs</strong></span></b></span> page, rather than associate the resource with another SLA through protection modification.<p id="oceanbase_00059__en-us_topic_0000001792344090_p34052272519">After you associate the resource with another SLA by modifying protection, if the copy of the original SLA still exists, the license capacity may be deducted repeatedly.</p></li>
         </ul></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Remove Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove the protection plan for a cluster when it does not need to be protected. After removal, the system cannot execute protection jobs for the cluster.</p> <p><strong>Note</strong></p>
         <ul>
          <li id="oceanbase_00059__en-us_topic_0000001792344090_li194311728491">When a protection job of the database is running, the protection cannot be removed.</li>
          <li id="oceanbase_00059__en-us_topic_0000001792344090_li79479497198">If the protection policy for a resource in the associated SLA changes, you are advised to modify the associated SLA on the <span class="uicontrol" id="oceanbase_00059__en-us_topic_0000001792344090_uicontrol5174547101015"><b><span id="oceanbase_00059__en-us_topic_0000001792344090_en-us_topic_0000001839142377_text71111921162814"><strong>Protection</strong></span> &gt; Protection Policies &gt; SLAs</b></span> page, rather than perform SLA re-association after protection removal.<p id="oceanbase_00059__en-us_topic_0000001792344090_p636319506192">After you remove protection and perform SLA re-association, if the copy of the original SLA still exists, the license capacity may be deducted repeatedly.</p></li>
         </ul></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Disable Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to disable the protection plan of a cluster for which periodic protection is not required. After the protection plan is disabled, the system does not perform automatic backup jobs for the cluster.</p> <p><strong>Note</strong></p> <p>Disabling protection does not affect ongoing backup jobs.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Activate Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to activate a disabled protection plan of a cluster. After the protection plan is activated, the system backs up the cluster based on the protection plan.</p> <p><strong>Note</strong></p> <p>This operation can be performed only on instances that have been associated with an SLA and are in the <span class="uicontrol"><b><span><strong>Unprotected</strong></span></b></span> state.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Manual Backup</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Test Connectivity</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to test the connectivity between the <span>product</span> and cluster.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Test Connectivity</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p>Modifying cluster information</p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to modify the information such as the cluster name, OBClient host, and OBServer host.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p> <p></p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p>Deleting a cluster</p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to delete a cluster that is no longer used.</p> <p><strong>Note</strong></p> <p>Before deleting a cluster, remove protection from the cluster.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 ">
         <div class="p">
          Adding a tag
          <div class="note" id="oceanbase_00059__en-us_topic_0000001792344090_note161679211561">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <p id="oceanbase_00059__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
           </div>
          </div>
         </div></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong id="oceanbase_00059__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 ">
         <div class="p">
          Choose <span class="uicontrol" id="oceanbase_00059__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="oceanbase_00059__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
          <div class="note" id="oceanbase_00059__en-us_topic_0000001792344090_note164681316118">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <ul id="oceanbase_00059__en-us_topic_0000001792344090_ul22969251338">
             <li id="oceanbase_00059__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="oceanbase_00059__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="oceanbase_00059__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
             <li id="oceanbase_00059__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="oceanbase_00059__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="oceanbase_00059__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="oceanbase_00059__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
            </ul>
           </div>
          </div>
         </div></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 ">
         <div class="p">
          Removing a tag
          <div class="note" id="oceanbase_00059__en-us_topic_0000001792344090_note4957114117575">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <p id="oceanbase_00059__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
           </div>
          </div>
         </div></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong id="oceanbase_00059__en-us_topic_0000001792344090_b1495945913127_1">Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p> <p></p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="oceanbase_00059__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="oceanbase_00059__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.2.4.2.4.1.1 mcps1.3.2.4.2.4.1.2 mcps1.3.2.4.2.4.1.3 "><p>Note:</p>
         <ul>
          <li>For details about how to restore OceanBase clusters, see <a href="oceanbase_00044.html">Restoring OceanBase</a>.</li>
          <li>For details about protection operations, see <a href="oceanbase_00015.html">Step 7: Performing Backup</a>.</li>
         </ul></td>
       </tr>
      </tbody>
     </table>
    </div>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="oceanbase_00057.html">OceanBase Cluster Environment</a>
    </div>
   </div>
  </div>
 </body>
</html>