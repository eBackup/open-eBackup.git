<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring One or More Databases in an SQL Server Instance">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="sql-0044.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="sql-0152">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring One or More Databases in an SQL Server Instance</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="sql-0152"></a><a name="sql-0152"></a>
  <h1 class="topictitle1">Restoring One or More Databases in an SQL Server Instance</h1>
  <div>
   <p>This section describes how to restore one or more databases of a backed-up SQL Server instance to the original or a different host using copies.</p>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li>Before restoring data to a different host, ensure that a database instance exists on the target host.</li>
     <li>Before restoration, ensure that the remaining space of the data directory at the target location for restoration is greater than the size of the copy used for restoration before reduction. Otherwise, restoration will fail.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="sql-0152__en-us_topic_0000001839142377_uicontrol17392202272717"><b><span id="sql-0152__en-us_topic_0000001839142377_text1939210224272"><strong>Explore</strong></span> &gt; <span id="sql-0152__en-us_topic_0000001839142377_text439314220278"><strong>Copy Data</strong></span> &gt; <span id="sql-0152__en-us_topic_0000001839142377_text14652358184918"><strong>Databases</strong></span> &gt; <span id="sql-0152__en-us_topic_0000001839142377_text6385151219337"><strong>SQL Server</strong></span></b></span>.</span></li>
     <li><span>On the <span class="uicontrol"><b>Copies</b></span> tab page, find the copy for restoration based on the copy information.</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p>Database-level restoration supports the restoration of backup copies and replication copies of an instance.</p>
       </div>
      </div> <p></p></li>
     <li><span>In the row of the desired copy, choose <span class="uicontrol"><b>More &gt; Database-level Restoration</b></span>.</span></li>
     <li><span>On the <span class="uicontrol"><b>Database-level Restoration</b></span> tab page, select the database objects to be restored in the left pane and the target location for restoration in the right pane.</span></li>
     <li><span>Set the parameters for database-level restoration. For details, see <a href="#sql-0152__sql-0047_table93951625101715">Table 1</a>.</span><p></p>
      <div class="tablenoborder">
       <a name="sql-0152__sql-0047_table93951625101715"></a><a name="sql-0047_table93951625101715"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="sql-0152__sql-0047_table93951625101715" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Parameters for database-level restoration
        </caption>
        <colgroup>
         <col style="width:32.53%">
         <col style="width:67.47%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="32.53%" id="mcps1.3.3.2.5.2.1.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="67.47%" id="mcps1.3.3.2.5.2.1.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="32.53%" headers="mcps1.3.3.2.5.2.1.2.3.1.1 "><p><span id="sql-0152__sql-0047_text15471356412"><strong>Restore To</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="67.47%" headers="mcps1.3.3.2.5.2.1.2.3.1.2 "><p>Select either <span class="uicontrol" id="sql-0152__sql-0047_uicontrol84961241181211"><b><span id="sql-0152__sql-0047_text19159183316300"><strong>Original location</strong></span></b></span> or <span class="uicontrol" id="sql-0152__sql-0047_uicontrol12496341161217"><b><span id="sql-0152__sql-0047_text17983138143120"><strong>New location</strong></span></b></span>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="32.53%" headers="mcps1.3.3.2.5.2.1.2.3.1.1 "><p><span id="sql-0152__sql-0047_text105489481943"><strong>Location</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="67.47%" headers="mcps1.3.3.2.5.2.1.2.3.1.2 "><p>If <strong id="sql-0152__sql-0047_b29321215184518">Restore To</strong> is set to <span class="uicontrol" id="sql-0152__sql-0047_uicontrol5674181313310"><b><span id="sql-0152__sql-0047_text48352563919"><strong>Original location</strong></span></b></span>, the original location is displayed by default.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="32.53%" headers="mcps1.3.3.2.5.2.1.2.3.1.1 "><p><strong>Tag</strong></p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>This parameter is available only in 1.6.0 and later versions.</p>
            </div>
           </div></td>
          <td class="cellrowborder" valign="top" width="67.47%" headers="mcps1.3.3.2.5.2.1.2.3.1.2 "><p>Select tag names to filter the target host or cluster.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="32.53%" headers="mcps1.3.3.2.5.2.1.2.3.1.1 "><p><span id="sql-0152__sql-0047_text329817453610"><strong>Target Host/Cluster</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="67.47%" headers="mcps1.3.3.2.5.2.1.2.3.1.2 "><p>If <strong id="sql-0152__sql-0047_b1120593512444">Restore To</strong> is set to <span class="uicontrol" id="sql-0152__sql-0047_uicontrol63818461965"><b><span id="sql-0152__sql-0047_text625115598103"><strong>New location</strong></span></b></span>, select the instance on the target host.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>Before performing restoration, ensure that you have exited Microsoft SQL Server Management Studio. During restoration, the target database is shut down.</p>
            </div>
           </div></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="sql-0044.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>