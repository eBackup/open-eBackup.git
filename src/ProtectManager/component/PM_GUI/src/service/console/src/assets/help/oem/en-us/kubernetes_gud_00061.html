<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Namespaces and StatefulSets">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="kubernetes_gud_00058.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="kubernetes_gud_00061">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Namespaces and StatefulSets</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="kubernetes_gud_00061"></a><a name="kubernetes_gud_00061"></a>
  <h1 class="topictitle1">Managing Namespaces and StatefulSets</h1>
  <div>
   <p>The <span>product</span> allows you to modify, remove, activate, or disable protection for namespaces or StatefulSets.</p>
   <div class="section">
    <h4 class="sectiontitle">Related Operations</h4>
    <p>You need to log in to the WebUI, choose <span class="uicontrol"><b><span id="kubernetes_gud_00061__en-us_topic_0000001839142377_text194759416387"><strong>Protection</strong></span> &gt; Containers &gt; Kubernetes FlexVolume</b></span>, and find the namespace to be operated on the <span><strong>Namespace</strong></span> tab page.</p>
    <p><a href="#kubernetes_gud_00061__table24934294919">Table 1</a> describes the related operations.</p>
    <div class="tablenoborder">
     <a name="kubernetes_gud_00061__table24934294919"></a><a name="table24934294919"></a>
     <table cellpadding="4" cellspacing="0" summary="" id="kubernetes_gud_00061__table24934294919" frame="border" border="1" rules="all">
      <caption>
       <b>Table 1 </b>Operations related to a namespace protection plan
      </caption>
      <colgroup>
       <col style="width:25%">
       <col style="width:50%">
       <col style="width:25%">
      </colgroup>
      <thead align="left">
       <tr>
        <th align="left" class="cellrowborder" valign="top" width="25%" id="mcps1.3.2.4.2.4.1.1"><p>Operation</p></th>
        <th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.2.4.2.4.1.2"><p>Description</p></th>
        <th align="left" class="cellrowborder" valign="top" width="25%" id="mcps1.3.2.4.2.4.1.3"><p>Navigation Path</p></th>
       </tr>
      </thead>
      <tbody>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Modify Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to modify a protection plan to re-associate a namespace with an SLA, modify an agent host, or configure an SLA application policy. After the modification, the next backup will be executed based on the modified protection plan.</p> <p><strong>Note</strong></p>
         <ul>
          <li>The modification does not affect the protection job that is being executed. The modified protection plan will take effect in the next protection period.</li>
          <li id="kubernetes_gud_00061__li1675404419448">If a backup job has been performed, reconfigure the SLA policy (change to the SLA with copy verification enabled) when modifying the protection plan. After the modification, the initial backup job will be converted to a full backup job if it is a non-full backup job.</li>
          <li id="kubernetes_gud_00061__li969242021917">If the SLA parameter associated with the resource changes (from copy verification disabled to copy verification enabled) after a backup job has been performed, when the protection plan for the resource is implemented after the change, the initial backup job will be converted to a full backup job if it is a non-full backup job.</li>
         </ul></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target namespace, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Remove Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove the protection for a namespace when it does not need to be protected. After the removal, the system cannot execute protection jobs for the namespace. To protect the namespace again, associate an SLA with it again.</p> <p><strong>Note</strong></p> <p>When the protection job of the namespace is running, the protection cannot be removed.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target namespace, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Manual Backup</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target namespace, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Activate Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to activate a disabled protection plan for a namespace. After the protection plan is activated, the system backs up, archives, or replicates the namespace based on the protection plan.</p> <p><strong>Note</strong></p> <p>You can perform this operation only on a namespace that has been associated with an SLA and is in the <span class="uicontrol"><b><span><strong>Unprotected</strong></span></b></span> state.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target namespace, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 "><p><span><strong>Disable Protection</strong></span></p></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to disable a protection plan if the namespace no longer needs to be periodically protected. After the protection plan is disabled, the system does not perform automatic backup, archive, or replication jobs for the namespace.</p> <p><strong>Note</strong></p> <p>Disabling a protection plan does not affect protection jobs that are running.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>In the row of the target namespace, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 ">
         <div class="p">
          Adding a tag
          <div class="note" id="kubernetes_gud_00061__en-us_topic_0000001792344090_note161679211561">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <p id="kubernetes_gud_00061__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
           </div>
          </div>
         </div></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 ">
         <div class="p">
          Choose <span class="uicontrol" id="kubernetes_gud_00061__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="kubernetes_gud_00061__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
          <div class="note" id="kubernetes_gud_00061__en-us_topic_0000001792344090_note164681316118">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <ul id="kubernetes_gud_00061__en-us_topic_0000001792344090_ul22969251338">
             <li id="kubernetes_gud_00061__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
             <li id="kubernetes_gud_00061__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
            </ul>
           </div>
          </div>
         </div></td>
       </tr>
       <tr>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.1 ">
         <div class="p">
          Removing a tag
          <div class="note" id="kubernetes_gud_00061__en-us_topic_0000001792344090_note4957114117575">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <p id="kubernetes_gud_00061__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
           </div>
          </div>
         </div></td>
        <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.2.4.2.4.1.2 "><p><strong id="kubernetes_gud_00061__en-us_topic_0000001792344090_b472911291311">Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p></td>
        <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.2.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="kubernetes_gud_00061__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="kubernetes_gud_00061__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
       </tr>
       <tr>
        <td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.2.4.2.4.1.1 mcps1.3.2.4.2.4.1.2 mcps1.3.2.4.2.4.1.3 "><p>Note: The system supports restoration for only StatefulSets. For details, see <a href="kubernetes_gud_00044.html">Restoration</a>.</p></td>
       </tr>
      </tbody>
     </table>
    </div>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="kubernetes_gud_00058.html">Cluster, Namespace, and StatefulSet</a>
    </div>
   </div>
  </div>
 </body>
</html>