<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing Storage Device Information">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="nas_s_0088.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0090">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing Storage Device Information</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0090"></a><a name="nas_s_0090"></a>

<h1 class="topictitle1">Managing Storage Device Information</h1>
<div><p>After a storage device is added to the <span>OceanProtect</span>, you can perform operations on the device.</p>
<p>Choose <span class="uicontrol"><b><span id="nas_s_0090__en-us_topic_0000001839142377_text19671936024"><strong>Protection</strong></span> &gt; File Systems &gt; Storage Devices</b></span> to go to the <span class="wintitle"><b><span><strong>Storage Device</strong></span></b></span> page.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0090__en-us_topic_0000001839142377_p77283132179">For 1.5.0, choose <span class="uicontrol" id="nas_s_0090__en-us_topic_0000001839142377_uicontrol13191103461714"><b><span id="nas_s_0090__en-us_topic_0000001839142377_text121911134171715"><strong>Protection</strong></span> &gt; File Services &gt; Storage Device</b></span>.</p>
</div></div>
<p><a href="#nas_s_0090__table51069596392">Table 1</a> describes the operations.</p>

<div class="tablenoborder"><a name="nas_s_0090__table51069596392"></a><a name="table51069596392"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0090__table51069596392" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Operations on the storage device</caption><colgroup><col style="width:21.279999999999998%"><col style="width:47.38%"><col style="width:31.34%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="21.279999999999998%" id="mcps1.3.5.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="47.38%" id="mcps1.3.5.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="31.34%" id="mcps1.3.5.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.5.2.4.1.1 "><p>Modifying storage device information</p>
</td>
<td class="cellrowborder" valign="top" width="47.38%" headers="mcps1.3.5.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>When the IP address, port number, username, or password of an added storage device is changed, perform this operation to synchronize the change.</p>
<p><strong>Note</strong></p>
<ul><li>Modify the storage device information when no backup or restoration job is in progress. Otherwise, the job fails.</li><li>You can only change the IP address to another management IP address of the same storage device.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="31.34%" headers="mcps1.3.5.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p>
<p><a href="nas_s_0013.html#nas_s_0013__table20829151954610">Table 1</a> describes the storage device parameters.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.5.2.4.1.1 "><p>Deleting a storage device</p>
</td>
<td class="cellrowborder" valign="top" width="47.38%" headers="mcps1.3.5.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>If you do not need to protect NAS file systems or NAS shares on a storage device or use the device as the target of restoration, you can delete the storage device.</p>
<p><strong>Note</strong></p>
<ul><li>If a backup or scanning job is in progress, the storage device cannot be deleted. Wait until the job is complete.</li><li>After a storage device is deleted, NAS file systems and NAS shares associated with the storage device are also deleted.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="31.34%" headers="mcps1.3.5.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p>
<p>To delete storage devices in batches, select multiple storage devices and click <span class="uicontrol"><b><span><strong>Delete</strong></span></b></span> in the upper part of the page.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.5.2.4.1.1 "><p>Rescanning storage devices</p>
</td>
<td class="cellrowborder" valign="top" width="47.38%" headers="mcps1.3.5.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>The <span>OceanProtect</span> automatically scans storage devices every hour. If NAS file systems or NAS shares are added, deleted, or modified on a registered storage device, data is updated to the <span>OceanProtect</span> after the scanning. This operation immediately updates changed NAS file systems or NAS shares to the <span>OceanProtect</span>. By default, the system automatically refreshes storage devices every hour.</p>
<p><strong>Note</strong></p>
<p>The <span>OceanProtect</span> does not automatically associate newly discovered NAS file systems or NAS shares with SLAs.</p>
</td>
<td class="cellrowborder" valign="top" width="31.34%" headers="mcps1.3.5.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.5.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="nas_s_0090__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0090__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="47.38%" headers="mcps1.3.5.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="31.34%" headers="mcps1.3.5.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="nas_s_0090__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="nas_s_0090__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="nas_s_0090__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="nas_s_0090__en-us_topic_0000001792344090_ul22969251338"><li id="nas_s_0090__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="nas_s_0090__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="nas_s_0090__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="nas_s_0090__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="nas_s_0090__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="nas_s_0090__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="nas_s_0090__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.5.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="nas_s_0090__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0090__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="47.38%" headers="mcps1.3.5.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
</td>
<td class="cellrowborder" valign="top" width="31.34%" headers="mcps1.3.5.2.4.1.3 "><p>Choose <span class="uicontrol" id="nas_s_0090__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="nas_s_0090__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="nas_s_0088.html">Storage Device Information</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>