<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Viewing NAS Share Information">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="nas_s_0094.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="nas_s_0095">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Viewing NAS Share Information</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="nas_s_0095"></a><a name="nas_s_0095"></a>
  <h1 class="topictitle1">Viewing NAS Share Information</h1>
  <div>
   <p>After a NAS share is registered with the <span>product</span>, you can view details about the NAS share.</p>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol"><b><span id="nas_s_0095__en-us_topic_0000001839142377_text99081414144119"><strong>Protection</strong></span> &gt; File Systems &gt; NAS Shares</b></span>.</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p id="nas_s_0095__en-us_topic_0000001839142377_p5978112341818">For 1.5.0, choose <span class="uicontrol" id="nas_s_0095__en-us_topic_0000001839142377_uicontrol129783237180"><b><span id="nas_s_0095__en-us_topic_0000001839142377_text10978172391818"><strong>Protection</strong></span> &gt; File Services &gt; NAS Shares</b></span>.</p>
       </div>
      </div> <p><a href="#nas_s_0095__table2382789197">Table 1</a> describes NAS share information.</p>
      <div class="tablenoborder">
       <a name="nas_s_0095__table2382789197"></a><a name="table2382789197"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="nas_s_0095__table2382789197" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>NAS share information
        </caption>
        <colgroup>
         <col style="width:19.98%">
         <col style="width:80.02%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="19.98%" id="mcps1.3.2.2.1.2.3.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="80.02%" id="mcps1.3.2.2.1.2.3.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>NAS share name.</p> <p>Click the name of a NAS share to view its overview, copy data, and jobs.</p>
           <ul>
            <li>Overview<p>The name, SLA information, job execution time, and total number of copies are displayed.</p></li>
            <li>Copy data<p>You can search for copies by year, month, or day.</p> <p>If <span><img src="en-us_image_0000001792520506.png"></span> is displayed below the time, a copy at that time exists.</p> <p>Click <strong id="nas_s_0095__nas_s_0092_b0934328123913"><span id="nas_s_0095__nas_s_0092_text6231156172516"><strong>Clear Index</strong></span></strong> to delete all indexes of a resource.</p>
             <div class="note">
              <span class="notetitle"> NOTE: </span>
              <div class="notebody">
               <ul id="nas_s_0095__nas_s_0092_ul183831433163112">
                <li id="nas_s_0095__nas_s_0092_li1838353343113">If a copy is being indexed, it cannot be deleted until the indexing is complete.</li>
                <li id="nas_s_0095__nas_s_0092_li12383153315316">If <span id="nas_s_0095__nas_s_0092_text1910522642811"><strong>Automatic Indexing</strong></span> is enabled for the SLA associated with a resource and <span id="nas_s_0095__nas_s_0092_text1157112002916"><strong>Automatic Indexing</strong></span> is enabled for the backup policy, you need to disable <span id="nas_s_0095__nas_s_0092_text1914650122916"><strong>Automatic Indexing</strong></span> or disassociate the resource from the SLA before deleting the index.</li>
               </ul>
              </div>
             </div></li>
            <li>Job<p>All jobs, including backup and restoration jobs, are displayed.</p></li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>FQDN/IP</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>FQDN or service IP address for accessing the NAS share.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>Storage Device</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>Name of the storage device to which the NAS share belongs.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>Share Protocol</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>NFS or CIFS.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>SLAs</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>Name of the SLA associated with the NAS share.</p> <p>Click an SLA name to view its basic information and associated resources.</p>
           <ul>
            <li>Basic information<p>The backup policy and advanced settings are displayed.</p></li>
            <li>Associated resources<p>Resources associated with the SLA are displayed.</p></li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>SLA Compliance</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 ">
           <div class="p">
            Whether a backup job is successfully executed based on the backup interval or backup window set in the SLA.
            <ul id="nas_s_0095__en-us_topic_0000001792344110_en-us_topic_0000001792344094_ul1823814519293">
             <li id="nas_s_0095__en-us_topic_0000001792344110_en-us_topic_0000001792344094_li9238184572919"><strong id="nas_s_0095__en-us_topic_0000001792344110_en-us_topic_0000001792344094_b1092516394564">Met</strong>: If the backup time of a backup job (except for the first full backup) meets the backup time window or backup interval requirements and the backup job is executed successfully, SLA compliance is met.</li>
             <li id="nas_s_0095__en-us_topic_0000001792344110_en-us_topic_0000001792344094_li189142052112912"><strong id="nas_s_0095__en-us_topic_0000001792344110_en-us_topic_0000001792344094_b541520442564">Missed</strong>: If the backup time does not meet the backup time window or backup interval requirements, for example, if the backup interval set in the SLA for a backup job is once every half an hour but the backup time exceeds half an hour, SLA compliance is missed. In this case, you are advised to adjust the backup interval or backup window.</li>
            </ul>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 "><p><span><strong>Protection Status</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>Indicates whether the NAS share is protected.</p>
           <ul>
            <li><span><strong>Unprotected</strong></span>: The NAS share is not associated with an SLA, or the NAS share is associated with an SLA but protection is disabled.</li>
            <li><span><strong>Protected</strong></span>: The NAS share has been associated with an SLA and protection has been activated.</li>
            <li><span><strong>Creating</strong></span>: The NAS share has been associated with an SLA and protection is being created.</li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="19.98%" headers="mcps1.3.2.2.1.2.3.2.3.1.1 ">
           <div class="p">
            <strong id="nas_s_0095__en-us_topic_0000001792344110_b827118125517">Tag</strong>
            <div class="note" id="nas_s_0095__en-us_topic_0000001792344110_note1330311543247">
             <span class="notetitle"> NOTE: </span>
             <div class="notebody">
              <p id="nas_s_0095__en-us_topic_0000001792344110_p630385412417">This parameter is available only in 1.6.0 and later versions.</p>
             </div>
            </div>
           </div></td>
          <td class="cellrowborder" valign="top" width="80.02%" headers="mcps1.3.2.2.1.2.3.2.3.1.2 "><p>If a tag has been added for the resource, the tag name is displayed. Otherwise, <strong id="nas_s_0095__en-us_topic_0000001792344110_b1959642011219">--</strong> is displayed.</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="nas_s_0094.html">NAS Shares</a>
    </div>
   </div>
  </div>
 </body>
</html>