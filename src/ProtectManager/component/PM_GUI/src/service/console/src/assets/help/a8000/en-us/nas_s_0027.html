<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 7: Creating a Backup SLA">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="nas_s_0020.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="nas_s_0027">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 7: Creating a Backup SLA</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="nas_s_0027"></a><a name="nas_s_0027"></a>

<h1 class="topictitle1">Step 7: Creating a Backup SLA</h1>
<div><p>Customize an SLA to protect resources based on your requirements.</p>
<div class="section"><h4 class="sectiontitle">Context</h4><p id="nas_s_0027__nas_s_0016_p134761436101310">For details about customizing an SLA, see <a href="nas_s_0079.html">About SLA</a>. For details about the constraints, see <a href="nas_s_r.html">Constraints</a>.</p>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="nas_s_0027__en-us_topic_0000001839142377_uicontrol8111821122820"><b><span id="nas_s_0027__en-us_topic_0000001839142377_text71111921162814"><strong>Protection</strong></span> &gt; Protection Policies &gt; SLAs</b></span>.</span></li><li><span>Click <span class="uicontrol" id="nas_s_0027__en-us_topic_0000001839143189_uicontrol12915192943311"><b><span id="nas_s_0027__en-us_topic_0000001839143189_text39091229123315"><strong>Create</strong></span></b></span>.</span></li><li><span>Enter an SLA name. The default name is <strong id="nas_s_0027__en-us_topic_0000001839143189_b694314191141">SLA_</strong><em id="nas_s_0027__en-us_topic_0000001839143189_i1829310311138">Current timestamp</em>.</span></li><li><span>Select an application type for the SLA to be created.</span><p><ol type="a"><li>Click <span class="uicontrol"><b><span><strong>Application</strong></span></b></span> and select the application for the SLA to be created.<ul><li><span id="nas_s_0027__en-us_topic_0000001839143189_text10801323154310"><strong>General SLA</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p144377559348">A general SLA can be applied to resources except filesets, SQL Server, PostgreSQL, Kingbase, Redis, ClickHouse, MongoDB, NAS shares, NAS file systems, Huawei Cloud Stack GaussDB, TDSQL, Kubernetes CSI, TiDB, volumes, common shares, Active Directory, GaussDB T single-node systems, object storage, GaussDB (1.6.0 and later versions), and SAP HANA (as an application in 1.6.0 and later versions).</p>
</li><li><span><strong>Specific to application</strong></span><p>Select an application type for the SLA to be created, for example, NAS share. This section assumes that an SLA of the <span><strong>Specific to application</strong></span> type is created.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p></p>
</div></div>
</li></ul>
</li><li>Click <span class="uicontrol"><b>OK</b></span>.</li></ol>
</p></li><li><span>Configure a backup policy.</span><p><ol type="a"><li>Click the <span class="uicontrol"><b><span><strong>Backup Policy</strong></span></b></span> icon.</li><li>Configure basic parameters for a backup policy.<p><a href="#nas_s_0027__en-us_topic_0000001387748585_table2663151516335">Table 1</a> describes the related parameters.</p>
<div class="p">Set the backup interval, copy retention period, and backup time window based on service requirements. The recommended settings are as follows:<ul><li>The backup interval must be longer than the backup duration.</li><li>The retention period of a full backup copy must be longer than that of an incremental backup copy.</li><li>The retention period must be longer than the backup interval.</li></ul>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="nas_s_0027__en-us_topic_0000001839143189_ul192116591633"><li id="nas_s_0027__en-us_topic_0000001839143189_li921115591430">At least one backup policy must be created. To add another backup policy, click <span><img id="nas_s_0027__en-us_topic_0000001839143189_image178791955235" src="en-us_image_0000001939082533.png"></span> on the right of the page.</li><li id="nas_s_0027__en-us_topic_0000001839143189_li621119590319">A maximum of four backup policies can be added for each backup type.</li></ul>
</div></div>
</div>

<div class="tablenoborder"><a name="nas_s_0027__en-us_topic_0000001387748585_table2663151516335"></a><a name="en-us_topic_0000001387748585_table2663151516335"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0027__en-us_topic_0000001387748585_table2663151516335" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Basic parameters of a backup policy</caption><colgroup><col style="width:11.899999999999999%"><col style="width:15.22%"><col style="width:72.88%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.3.2.5.2.1.2.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.3.2.5.2.1.2.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" rowspan="2" valign="top" width="11.899999999999999%" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Full backup</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="15.22%" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="72.88%" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.2 "><p>Name of a backup policy.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p>-</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><div class="p">Configure the backup interval, copy retention period, and backup time window for full backup.<ul id="nas_s_0027__en-us_topic_0000001839143189_ul06281446133611"><li id="nas_s_0027__en-us_topic_0000001839143189_li5628184619364">Configure the backup interval and copy retention period for full backup.<ul id="nas_s_0027__en-us_topic_0000001839143189_ul3912541183319"><li id="nas_s_0027__en-us_topic_0000001839143189_li1491311419338"><span id="nas_s_0027__en-us_topic_0000001839143189_text881714512382"><strong>By Year</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p7913141173316">Configure the job to be executed once every year on <em id="nas_s_0027__en-us_topic_0000001839143189_i8695125961819">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i8695205941811">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i269613594180">xx</em> days, weeks, months, years, or permanently.</p>
<p id="nas_s_0027__en-us_topic_0000001839143189_p52204181864">If the date does not exist in the year, no copy is generated.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li18913134153311"><span id="nas_s_0027__en-us_topic_0000001839143189_text9120524389"><strong>By Month</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p19131141133318">Configure the job to be executed on <em id="nas_s_0027__en-us_topic_0000001839143189_i3137640154710">xx</em> (day) (multiple days can be selected) or the last day of each month. Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i20137114020478">xx</em> days, weeks, months, years, or permanently.</p>
<p id="nas_s_0027__en-us_topic_0000001839143189_p131866516465">If the backup job is set to be performed on the <em id="nas_s_0027__en-us_topic_0000001839143189_i638819152110">xx</em>th day of each month, no copy is generated when the date does not exist in the current month.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li79137410332"><span id="nas_s_0027__en-us_topic_0000001839143189_text107501579380"><strong>By Week</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p091313410338">Configure the job to be executed every Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, or Sunday (you can select multiple options). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i10839155815217">xx</em> days, weeks, months, years, or permanently.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li18498191724615"><span id="nas_s_0027__en-us_topic_0000001839143189_text199032391683"><strong>By Day</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p46219385469">Configure the job to be executed every <em id="nas_s_0027__en-us_topic_0000001839143189_i563934782212">xx</em> days starting from <em id="nas_s_0027__en-us_topic_0000001839143189_i11639154719227">xx</em> (year) <em id="nas_s_0027__en-us_topic_0000001839143189_i14640194713225">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i176406479225">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i1164014713226">xx</em> days, weeks, months, years, or permanently.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li426962116468"><span id="nas_s_0027__en-us_topic_0000001839143189_text14231841203315"><strong>By Hour</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p6350614134914">Configure the job to be executed every <em id="nas_s_0027__en-us_topic_0000001839143189_i3449105714464">xx</em> hours starting from <em id="nas_s_0027__en-us_topic_0000001839143189_i1644985754613">xx</em> (year) <em id="nas_s_0027__en-us_topic_0000001839143189_i1044916576462">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i15449257134618">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i64501457104615">xx</em> days, weeks, months, years, or permanently.</p>
</li></ul>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li1881682412376">Set the time period for performing full backup. The full backup job will not be scheduled beyond the time range.<div class="note" id="nas_s_0027__en-us_topic_0000001839143189_note3335182113214"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="nas_s_0027__en-us_topic_0000001839143189_ul64461851101420"><li id="nas_s_0027__en-us_topic_0000001839143189_li037445621410">If the end time is earlier than or the same as the start time, the end time is actually the end time of the next day.</li><li id="nas_s_0027__en-us_topic_0000001839143189_li4446145117142">If the backup job is not completed within the specified time window, the system does not stop the backup job, but reports an event.</li><li id="nas_s_0027__en-us_topic_0000001839143189_li4684315367">Once the retention period expires, the system automatically deletes the expired copies.</li></ul>
</div></div>
</li></ul>
</div>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="3" valign="top" width="11.899999999999999%" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Incremental backup</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="15.22%" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="72.88%" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.2 "><p>Name of a backup policy.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p><strong>Backup</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p>Select <strong>Set as Forever Incremental (Synthetic Full) Backup</strong> to set the incremental backup policy as a forever incremental (synthetic full) backup policy.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><p>-</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.2.3.2.4.1.1 "><div class="p">Configure the backup interval, copy retention period, and backup time window for incremental backup. <span id="nas_s_0027__en-us_topic_0000001839143189_ph1527624175915">Incremental backup and differential backup cannot coexist.</span><ul id="nas_s_0027__en-us_topic_0000001839143189_ul167919791414"><li id="nas_s_0027__en-us_topic_0000001839143189_li1079119781413">Configure the backup interval and copy retention period for incremental backup.<ul id="nas_s_0027__en-us_topic_0000001839143189_ul1679113751412"><li id="nas_s_0027__en-us_topic_0000001839143189_li177910751413"><span id="nas_s_0027__en-us_topic_0000001839143189_text9791187121410"><strong>By Year</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p7791117131413">Configure the job to be executed once every year on <em id="nas_s_0027__en-us_topic_0000001839143189_i17543191313474">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i15543191324714">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i2054391324718">xx</em> days, weeks, months, years, or permanently.</p>
<p id="nas_s_0027__en-us_topic_0000001839143189_p62586191379">If the date does not exist in the year, no copy is generated.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li1279112715144"><span id="nas_s_0027__en-us_topic_0000001839143189_text979110717147"><strong>By Month</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p179113721413">Configure the job to be executed on <em id="nas_s_0027__en-us_topic_0000001839143189_i137808035">xx</em> (day) (multiple days can be selected) or the last day of each month. Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i119969764">xx</em> days, weeks, months, years, or permanently.</p>
<p id="nas_s_0027__en-us_topic_0000001839143189_p125877114419">If the backup job is set to be performed on the <em id="nas_s_0027__en-us_topic_0000001839143189_i18258105512473">xx</em>th day of each month, no copy is generated when the date does not exist in the current month.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li879112716144"><span id="nas_s_0027__en-us_topic_0000001839143189_text27913720144"><strong>By Week</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p1079111741416">Configure the job to be executed every Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, or Sunday (you can select multiple options). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i5259123154812">xx</em> days, weeks, months, years, or permanently.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li251355615915"><span id="nas_s_0027__en-us_topic_0000001839143189_text0935195916812"><strong>By Day</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p175137561491">Configure the job to be executed every <em id="nas_s_0027__en-us_topic_0000001839143189_i1716654814490">xx</em> days starting from <em id="nas_s_0027__en-us_topic_0000001839143189_i19166114815493">xx</em> (year) <em id="nas_s_0027__en-us_topic_0000001839143189_i416610485494">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i1316764824912">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i1116704854913">xx</em> days, weeks, months, years, or permanently.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li17513125616918"><span id="nas_s_0027__en-us_topic_0000001839143189_text11653155316339"><strong>By Hour</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p451312561795">Configure the job to be executed every <em id="nas_s_0027__en-us_topic_0000001839143189_i13179161374716">xx</em> hours starting from <em id="nas_s_0027__en-us_topic_0000001839143189_i18179151310474">xx</em> (year) <em id="nas_s_0027__en-us_topic_0000001839143189_i8179113154714">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i1417918132471">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i117911316470">xx</em> days, weeks, months, years, or permanently.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li108498361016"><span id="nas_s_0027__en-us_topic_0000001839143189_text4119140183416"><strong>By Minute</strong></span><p id="nas_s_0027__en-us_topic_0000001839143189_p178491339103">Configure the job to be executed every <em id="nas_s_0027__en-us_topic_0000001839143189_i103229137503">xx</em> minutes starting from <em id="nas_s_0027__en-us_topic_0000001839143189_i832217134501">xx</em> (year) <em id="nas_s_0027__en-us_topic_0000001839143189_i11323131375017">xx</em> (month) <em id="nas_s_0027__en-us_topic_0000001839143189_i63231113195016">xx</em> (day). Configure copies to be retained for <em id="nas_s_0027__en-us_topic_0000001839143189_i1932361355019">xx</em> days, weeks, months, years, or permanently.</p>
</li></ul>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li1679107171411">Set the time period for performing incremental backup. The incremental backup job will not be scheduled beyond the time range.<div class="note" id="nas_s_0027__en-us_topic_0000001839143189_note77917711419"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="nas_s_0027__en-us_topic_0000001839143189_en-us_topic_0000001839143189_ul64461851101420"><li id="nas_s_0027__en-us_topic_0000001839143189_en-us_topic_0000001839143189_li037445621410">If the end time is earlier than or the same as the start time, the end time is actually the end time of the next day.</li><li id="nas_s_0027__en-us_topic_0000001839143189_en-us_topic_0000001839143189_li4446145117142">If the backup job is not completed within the specified time window, the system does not stop the backup job, but reports an event.</li><li id="nas_s_0027__en-us_topic_0000001839143189_en-us_topic_0000001839143189_li4684315367">Once the retention period expires, the system automatically deletes the expired copies.</li></ul>
</div></div>
</li></ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>For 1.6.0 and later versions, to generate a WORM copy, enable WORM and set a WORM validity period. Otherwise, skip this step.<div class="p" id="nas_s_0027__en-us_topic_0000001839143189_p19339194114493">For <strong id="nas_s_0027__en-us_topic_0000001839143189_b15358729184918">WORM Validity Period</strong>, you can select <strong id="nas_s_0027__en-us_topic_0000001839143189_b469832194914">Same as the copy retention period</strong> or <strong id="nas_s_0027__en-us_topic_0000001839143189_b4820353104917">Custom validity period</strong>.<div class="note" id="nas_s_0027__en-us_topic_0000001839143189_note8945151620502"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="nas_s_0027__en-us_topic_0000001839143189_ul2550728175518"><li id="nas_s_0027__en-us_topic_0000001839143189_li103014415565">WORM copies cannot be deleted or their validity periods cannot be shortened.</li><li id="nas_s_0027__en-us_topic_0000001839143189_li9171132315916">The custom WORM validity period cannot be longer than the copy retention period.</li><li id="nas_s_0027__en-us_topic_0000001839143189_li976612319580">A WORM copy cannot be changed to a non-WORM copy within its validity period.</li></ul>
</div></div>
</div>
</li><li>Configure advanced parameters for the backup policy.<div class="p"><a href="#nas_s_0027__en-us_topic_0000001387748585_table4195113413815">Table 2</a> describes the related parameters.
<div class="tablenoborder"><a name="nas_s_0027__en-us_topic_0000001387748585_table4195113413815"></a><a name="en-us_topic_0000001387748585_table4195113413815"></a><table cellpadding="4" cellspacing="0" summary="" id="nas_s_0027__en-us_topic_0000001387748585_table4195113413815" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Advanced parameters of a backup policy</caption><colgroup><col style="width:44.46%"><col style="width:55.54%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="44.46%" id="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="55.54%" id="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><p><span><strong>Specify Target Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><div class="p">Specify the target storage location of backup copies.<ul id="nas_s_0027__en-us_topic_0000001839143189_ul3832679403"><li id="nas_s_0027__en-us_topic_0000001839143189_li299019528162"><span class="parmvalue" id="nas_s_0027__en-us_topic_0000001839143189_parmvalue20580546155113"><b>Backup Storage Units</b></span>: A backup storage unit corresponds to a backup cluster node. After a backup storage unit is selected, copies are stored to the storage unit. If the capacity of the storage unit is insufficient, the backup job will fail.<p id="nas_s_0027__en-us_topic_0000001839143189_p36749546164">For details about how to create a backup storage unit, see "Managing Backup Storage Units" in the <i><cite id="nas_s_0027__en-us_topic_0000001839143189_cite35358248206">OceanProtect DataBackup 1.5.0-1.6.0 Cluster HA Feature Guide</cite></i>.</p>
</li><li id="nas_s_0027__en-us_topic_0000001839143189_li168151656101610"><span class="parmvalue" id="nas_s_0027__en-us_topic_0000001839143189_parmvalue142224995111"><b>Backup Storage Unit Groups</b></span>: A backup storage unit group is a collection of backup storage units. After a backup storage unit group is selected, the system automatically selects a target backup storage unit based on the storage policy of the storage unit group.<p id="nas_s_0027__en-us_topic_0000001839143189_p15126135815168">For details about how to create a backup storage unit group, see "(Optional) Creating Backup Storage Unit Groups" in the <i><cite id="nas_s_0027__en-us_topic_0000001839143189_cite961778916">OceanProtect DataBackup 1.5.0-1.6.0 Cluster HA Feature Guide</cite></i>.</p>
</li></ul>
<div class="note" id="nas_s_0027__en-us_topic_0000001839143189_note06533915171"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="nas_s_0027__en-us_topic_0000001839143189_ul2261104811172"><li id="nas_s_0027__en-us_topic_0000001839143189_li182618487176">For 1.5.0, this parameter is displayed only when you have deployed the cluster HA feature. You can leave the target location unspecified. If you do not specify the target location, the system automatically selects the target storage unit through intelligent balancing.</li><li id="nas_s_0027__en-us_topic_0000001839143189_li1025484315189">For 1.6.0 and later versions, you must specify the target location.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><p><span><strong>Rate Limiting Policies</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><p>Average maximum amount of data that can be transmitted per second in a backup job. </p>
<p>Select the rate limiting policy created in <a href="nas_s_0015.html">(Optional) Step 4: Creating a Rate Limiting Policy</a>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><p><span><strong>Automatic Indexing</strong></span></p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is available only in 1.5.0.</p>
</div></div>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><div class="p">After this function is enabled, you can search for files, directories, or links in copies.<div class="note" id="nas_s_0027__nas_s_0016_note05471921141918"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0027__nas_s_0016_p0743109201911">Indexes occupy backup storage space. If the number of index files in the system exceeds 1 billion, perform operations by referring to "Failed to Create a Copy Index" in the <i><cite id="nas_s_0027__nas_s_0016_cite23814569190">OceanProtect DataBackup 1.5.0-1.6.0 Troubleshooting</cite></i>.</p>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><p><span><strong>Target Deduplication</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><p>When the average file size is greater than or equal to 32 KB, you are advised to enable this function to save backup storage space.</p>
<p>After target deduplication is enabled, service data is deduplicated before being written to the storage space, saving storage space and improving backup efficiency.</p>
<p></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><div class="p"><strong id="nas_s_0027__en-us_topic_0000001839143189_b693713423122">Job Timeout Alarm</strong><div class="note" id="nas_s_0027__en-us_topic_0000001839143189_note379352823112"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="nas_s_0027__en-us_topic_0000001839143189_en-us_topic_0000001839143213_p8320153562113">This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><p>After this function is enabled, an alarm is sent if the job execution time exceeds the time window. The alarm needs to be manually cleared.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><p><span><strong>Job Failure Alarm</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><p>After this function is enabled, an alarm is automatically sent if a job fails and is automatically cleared if the subsequent job is successful.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="44.46%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.1 "><p><span><strong>Automatic Retry</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="55.54%" headers="mcps1.3.3.2.5.2.1.4.1.2.2.3.1.2 "><p>After this function is enabled, the system automatically retries a backup job if it fails.</p>
<div class="p">The number of retries ranges from 1 to 5, and the wait time ranges from 1 to 30 minutes. For example, if the number of retries is set to 3 and the wait time to 5 minutes, the system makes 3 retries and each retry every 5 minutes.<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>During automatic retry, the system creates a backup job. If the backup job is not within the backup time window, the job is not executed and fails.</p>
</div></div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</p></li><li><span>(Optional) Configure archive policies.</span><p><p id="nas_s_0027__nas_s_0016_p372431134">The built-in storage space of the <span id="nas_s_0027__nas_s_0016_text19690193371312">OceanProtect</span> is limited. If you need to retain copy data for a long time, you are advised to archive the copy data to external storage.</p>
<p id="nas_s_0027__nas_s_0016_p1430810361667">You can configure the archive policies by clicking <span class="uicontrol" id="nas_s_0027__nas_s_0016_uicontrol12308113618612"><b><span id="nas_s_0027__nas_s_0016_text95301130194917"><strong>Archive Policy</strong></span></b></span> or by modifying the SLA.</p>
<p id="nas_s_0027__nas_s_0016_p56570221159">For details about how to configure the archive policies, see <a href="nas_s_0052.html">Step 2: Creating an Archive SLA for Backup Copies</a>.</p>
</p></li><li><span>(Optional) Configure replication policies.</span><p><p id="nas_s_0027__nas_s_0016_p149315199820">The <span id="nas_s_0027__nas_s_0016_text520613431316">OceanProtect</span> allows you to replicate local data to a remote data center. In case a disaster occurs, copy data in the remote data center can be used to restore production data and services can be taken over, achieving remote data protection.</p>
<p id="nas_s_0027__nas_s_0016_p1713317158910">You can configure the replication policies by clicking <span class="uicontrol" id="nas_s_0027__nas_s_0016_uicontrol111335151290"><b><span id="nas_s_0027__nas_s_0016_text1466823185112"><strong>Replication Policy</strong></span></b></span> or by modifying the SLA.</p>
<p id="nas_s_0027__nas_s_0016_p9133115290">For details about how to configure the replication policies, see <a href="nas_s_0043.html">Step 7: Creating a Replication SLA</a>.</p>
<div class="note" id="nas_s_0027__nas_s_0016_note2098918181115"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_p187474161610">For 1.6.0 and later versions:</p>
<ul id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_ul1078182475012"><li id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_li83901215999">The WORM validity period of the backup policy in the SLA must be shorter than the copy retention period in the replication policy.</li><li id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_li878324105017">If WORM is not configured at the local end but configured at the target end, the WORM validity period is the same as the replication copy retention period.</li><li id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_li07815248507">If WORM is configured at the local end but not at the target end, the WORM validity period of replication copies is the same as that of WORM copies at the local end. The retention period of replication copies is greater than or equal to the WORM validity period of backup copies at the local end.</li><li id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_li1278624195020">If WORM is configured at both the local and target ends and WORM is configured by resource in the WORM configuration of <strong id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_b78231650163717">Data Security</strong> at the target end, all replication copies will have WORM enabled, and the WORM validity period is the same as the replication copy retention period.</li><li id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_li127819245503">At the local end, if you configure local backup copies to be retained permanently and set <strong id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_b20511104361110">WORM Validity Period</strong> to <strong id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_b851224341112">Same as the copy retention period</strong>, the WORM validity period is permanent. After a backup copy is generated, if you modify the SLA policy (by adding a replication policy and setting <strong id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_b115122018172413">Replication and Retention Rules</strong> to <strong id="nas_s_0027__nas_s_0016_en-us_topic_0000001839143189_b9172163882417">Replicate all copies</strong>), the WORM validity period at the target end is the same as the retention period of replication copies.</li></ul>
</div></div>
</p></li><li><span>Click <span class="uicontrol" id="nas_s_0027__nas_s_0016_uicontrol65831059121713"><b><span id="nas_s_0027__nas_s_0016_text659554117329"><strong>OK</strong></span></b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="nas_s_0020.html">Backing Up a NAS Share</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>