<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Live Mounting Filesets">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="Files-0049">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Live Mounting Filesets</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="Files-0049"></a><a name="Files-0049"></a>

<h1 class="topictitle1">Live Mounting Filesets</h1>
<div><p>Live mount can be used to mount fileset copies to target hosts that use data.</p>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><p>ProtectAgent and other related software have been installed on the target host to which the copy is mounted.</p>
<p>For details, see <i><cite>OceanProtect DataBackup 1.5.0-1.6.0 ProtectAgent Installation Guide</cite></i>.</p>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="Files-0049__en-us_topic_0000001839142377_uicontrol81551121183717"><b><span id="Files-0049__en-us_topic_0000001839142377_text91553217374"><strong>Explore</strong></span> &gt; <span id="Files-0049__en-us_topic_0000001839142377_text101552021103718"><strong>Live Mount</strong></span> &gt; <span id="Files-0049__en-us_topic_0000001839142377_text42471249173719"><strong>Filesets</strong></span></b></span>.</span></li><li><span>Click <span class="uicontrol"><b><span><strong>Create</strong></span></b></span>.</span></li><li><span>On the <span class="uicontrol"><b><span><strong>Select Resource</strong></span></b></span> tab page, search for or select the resource corresponding to the copy to be mounted, and click <span class="uicontrol"><b><span><strong>Next</strong></span></b></span>.</span></li><li><span>On the <span class="uicontrol"><b><span><strong>Select Copy</strong></span></b></span> tab page, perform the following operations:</span><p><ol type="a"><li>Select the copy to be mounted.<ul><li>The copy to be live mounted must be in the <span class="uicontrol"><b><span><strong>Normal</strong></span></b></span> status.</li><li>Copies with small file aggregation enabled do not support live mount.</li></ul>
</li><li>Select the created mount update policy.<p>If the mount update policy is not selected in this step, you can configure the mount update policy by <a href="Files-0053.html">modifying mount information</a> later.</p>
</li><li>Click <span class="uicontrol"><b><span><strong>Next</strong></span></b></span>.</li></ol>
</p></li><li><span>On the <span class="uicontrol"><b><span><strong>Mount Options</strong></span></b></span> tab page, set mounting parameters and click <span class="uicontrol"><b><span><strong>Next</strong></span></b></span>.</span><p><ol type="a"><li>Configure basic and advanced parameters for live mount.<div class="p"><a href="#Files-0049__en-us_topic_0000001387707949_table89615043315">Table 1</a> describes the related parameters.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul><li>You can configure either or both of <strong>Bandwidth (MB/s)</strong> and <strong>Normalized IOPS (8 KB)</strong>. You are advised to configure only <strong>Bandwidth (MB/s)</strong> for bandwidth-intensive services and only <strong>Normalized IOPS (8 KB)</strong> for IOPS-intensive services. If you cannot determine the service type or the service type changes frequently, you can configure both parameters and the system will control the traffic using the parameter with a smaller value.</li><li>You can query the conversion between the standard IOPS (8 KB) model and real I/O model in the conversion table on the GUI.</li><li>The maximum traffic of mounted copies is limited to ensure the performance of backup and restoration jobs. In addition, the minimum traffic of mounted copies is set to ensure the minimum performance of mounted copies.</li></ul>
</div></div>

<div class="tablenoborder"><a name="Files-0049__en-us_topic_0000001387707949_table89615043315"></a><a name="en-us_topic_0000001387707949_table89615043315"></a><table cellpadding="4" cellspacing="0" summary="" id="Files-0049__en-us_topic_0000001387707949_table89615043315" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Live mount parameters</caption><colgroup><col style="width:16.439999999999998%"><col style="width:20.78%"><col style="width:62.78%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p><span><strong>Target Host</strong></span></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>Target host to which the copy is mounted.</p>
<p>You can search for a host name or IP address to select the host.</p>
</td>
</tr>
<tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p><strong>CIFS Share Name</strong></p>
<p></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>Share name that accesses a file system.</p>
<p>The default share name is <strong>mount_</strong><em>timestamp</em>. You can also change the share name.</p>
<p>Ensure that the backup resource supports the CIFS share protocol. Otherwise, the CIFS file permission information will not be contained during the mounting.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is displayed only for the Windows OS.</p>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p><strong>Type</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>Type of the user who accesses a CIFS share.</p>
<ul><li><span><strong>Everyone</strong></span></li><li><span><strong>Windows Local Authentication User</strong></span></li><li><span><strong>Windows Local Authentication User Group</strong></span></li></ul>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is displayed only for the Windows OS.</p>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p><strong>Users</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>Select the required users or user groups.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><ul><li>If the required user or user group does not exist, create it on DeviceManager.</li><li>This parameter is displayed only for the Windows OS.</li></ul>
</div></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</li><li>Click <span class="uicontrol"><b><span><strong>Next</strong></span></b></span>.</li></ol>
</p></li><li><span>Preview and confirm the mounting parameters, and click <span class="uicontrol"><b><span><strong>Finish</strong></span></b></span>.</span><p><div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>If a copy is used for live mount immediately after it is generated, the live mount job may fail because the copy is still in the initialization status. In this case, try again in 3 minutes.</p>
</div></div>
</p></li></ol>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>