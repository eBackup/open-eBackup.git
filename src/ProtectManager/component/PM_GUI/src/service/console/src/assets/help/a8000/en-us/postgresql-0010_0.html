<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 2: Enabling the Archive Mode">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="postgresql-0010-2.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="postgresql-0010_0">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 2: Enabling the Archive Mode</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="postgresql-0010_0"></a><a name="postgresql-0010_0"></a>

<h1 class="topictitle1">Step 2: Enabling the Archive Mode</h1>
<div><p>Before backing up the database, you must enable the archive mode. Otherwise, the backup fails.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ul><li><strong>Method 1: Enabling the archive mode by modifying configuration file parameters</strong><ol><li>Use PuTTY to log in to the PostgreSQL database host.</li><li>Create a path for storing archive logs (WALs). The <strong>/mnt/server/archivedir/</strong> path is used as an example in subsequent operations.<pre class="screen">mkdir -p<em> /mnt/server/archivedir/</em></pre>
</li><li>Grant the read and write permissions to the database management user <strong>postgres</strong>.<pre class="screen">chmod 750 <em>/mnt/server/archivedir/</em>
chown postgres:postgres <em>/mnt/server/archivedir/</em></pre>
</li><li>Run the <strong>su postgres</strong> command to switch to the database management user <strong>postgres</strong>. Information similar to the following is displayed:<pre class="screen">[root@pg_102_129 ~]# su postgres
[postgres@pg_102_129 root]$<em>  </em></pre>
</li><li>Log in to the database as user <strong>postgres</strong>.<pre class="screen">cd <em>/usr/local/pgsql/bin</em>
./psql</pre>
</li><li>Run the <strong>show config_file</strong><strong>;</strong> command to query the path to the <strong>postgresql.conf</strong> file. Information similar to the following is displayed:<pre class="screen">postgres=# show config_file;
              config_file
---------------------------------------
 /usr/local/pgsql/data/postgresql.conf
(1 row)</pre>
</li><li>Press <span class="uicontrol"><b>Ctrl+D</b></span> to log out of the database management user <strong>postgres</strong> and log in to the PostgreSQL database host.</li><li>Open the <strong>postgresql.conf</strong> file. The <strong>/usr/local/pgsql/data/postgresql.conf</strong> path is used as an example.<pre class="screen">vi <em>/usr/local/pgsql/data/postgresql.conf</em></pre>
</li><li>Find and modify the <strong>wal_level</strong>, <strong>archive_mode</strong>, and <strong>archive_command</strong> parameters in the <strong>postgresql.conf</strong> file as follows:<pre class="screen">wal_level = replica                     # minimal, replica, or logical
archive_mode = on               # enables archiving; off, on, or always
archive_command = 'cp %p /home/postgres/archivedir/%f'          # command to use to archive a logfile segment</pre>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul><li>Set <strong>wal_level</strong> to <strong>archive</strong> (<strong>replica</strong> for PostgreSQL 9.6 or later).</li><li>Set <strong>archive_mode</strong> to <strong>on</strong>.</li><li>Set <strong>archive_command</strong> to <strong>'cp %p /mnt/server/archivedir/%f'</strong> and ensure that the archive log path is a single path.</li><li>When modifying the <strong>postgresql.conf</strong> file, change the values of existing fields in the file. Do not add the same fields to the file. Otherwise, the restoration job will be affected.</li><li>After the archive mode is enabled, you need to manually delete logs. Otherwise, the database cannot run properly. For details about how to manually delete logs, see <a href="en-us_topic_0000002135383169.html">Manually Deleting Archive Logs</a>.</li></ul>
</div></div>
</li></ol>
</li></ul>
<ul><li id="postgresql-0010_0__en-us_topic_0000001607842332_li125261454163017"><strong id="postgresql-0010_0__en-us_topic_0000001607842332_b520795215418">Method 2: Enabling the archive mode by running database commands</strong><ol id="postgresql-0010_0__en-us_topic_0000001607842332_ol1431724414327"><li id="postgresql-0010_0__en-us_topic_0000001607842332_li387595019325">Use PuTTY to log in to the PostgreSQL database host.</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li11497105712322">Create a path for storing archive logs (WALs). The <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b10471813124416">/mnt/server/archivedir/</strong> path is used as an example in subsequent operations.<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen2049712570326">mkdir -p /mnt/server/archivedir/</pre>
</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li157953814338">Grant the read and write permissions to the operating system user who runs the database.<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen11795158133313">chmod 750 /mnt/server/archivedir/
chown postgres:postgres /mnt/server/archivedir/</pre>
</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li296831915334">Run the <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b11924143585419">su postgres</strong> command to switch to the database administrator <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b792413510549">postgres</strong>. Information similar to the following is displayed:<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen7968719103320">[root@pg_102_129 ~]# su postgres
[postgres@pg_102_129 root]$<em id="postgresql-0010_0__en-us_topic_0000001607842332_i5968151915333">  </em></pre>
</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li1141133713334">Log in to the database as user <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b2565192674317">postgres</strong>.<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen14412163743318">cd /usr/local/pgsql/bin
./psql</pre>
</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li1012154817339">Run the <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b1352119595363">show config_file</strong><strong id="postgresql-0010_0__en-us_topic_0000001607842332_b2521175943617">;</strong> command to query the path to the <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b3521159123618">postgresql.conf</strong> file. Information similar to the following is displayed:<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen14121134853313">postgres=# show config_file;
              config_file
---------------------------------------
 /usr/local/pgsql/data/postgresql.conf
(1 row)</pre>
</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li945175818339">On the PostgreSQL database host, run the following commands in sequence to enable the database archive mode:<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen1645125843316">alter system set wal_level= 'replica';
alter system set archive_mode= 'on';
alter system set archive_command ='cp %p /mnt/server/archivedir/%f'</pre>
<div class="note" id="postgresql-0010_0__en-us_topic_0000001607842332_note48201149191716"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="postgresql-0010_0__en-us_topic_0000001607842332_p0821449131719">The <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b6885173119340">archive_command</strong> parameter supports only the <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b2885193113349">cp</strong> command.</p>
</div></div>
</li><li id="postgresql-0010_0__en-us_topic_0000001607842332_li587338113413">In cluster scenarios, run the following command to restart the pgpool service: <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b955211234532">/usr/local/pgpool/bin/pgpool</strong> indicates the PostgreSQL installation path.<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen2823323174014"><em id="postgresql-0010_0__en-us_topic_0000001607842332_i376417247500">/usr/local/pgpool/bin/pgpool</em> -n &gt; /dev/null 2&gt;&amp;1 &amp;</pre>
<p id="postgresql-0010_0__en-us_topic_0000001607842332_p387319813415">In a single-node system, run the following command to restart the database. <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b20667619135416">/usr/local/pgsql/bin/pg_ctl</strong> indicates the path of <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b611010356548">pg_ctl</strong> in the PostgreSQL installation path. <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b568519714558">-D</strong> indicates the user-defined data directory. <strong id="postgresql-0010_0__en-us_topic_0000001607842332_b1488114275554">-l</strong> indicates the specified log output file during the PostgreSQL database startup. Before specifying the log output file, ensure that the file has the permission to be created successfully.</p>
<pre class="screen" id="postgresql-0010_0__en-us_topic_0000001607842332_screen1388613527407"><em id="postgresql-0010_0__en-us_topic_0000001607842332_i145921251165216">/usr/local/pgsql/bin/pg_ctl</em> -D <em id="postgresql-0010_0__en-us_topic_0000001607842332_i138621547451">/usr/local/pgsql/data</em> -l <em id="postgresql-0010_0__en-us_topic_0000001607842332_i9466985451">logfile</em>  restart</pre>
</li></ol>
</li></ul>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="postgresql-0010-2.html">Backing Up PostgreSQL</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>