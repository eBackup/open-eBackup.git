<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Restoring a Fileset">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="Files-0041_0.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="Files-0044">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Restoring a Fileset</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="Files-0044"></a><a name="Files-0044"></a>

<h1 class="topictitle1">Restoring a Fileset</h1>
<div><p>This section describes how to restore a fileset that has been backed up to the original or a new location.</p>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><p>Before restoration, ensure that the remaining space of the data directory at the target location for restoration is greater than the size of the copy used for restoration before reduction. Otherwise, restoration will fail.</p>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="Files-0044__en-us_topic_0000001839142377_uicontrol1888202316279"><b><span id="Files-0044__en-us_topic_0000001839142377_text16882112392716"><strong>Explore</strong></span> &gt; <span id="Files-0044__en-us_topic_0000001839142377_text13882182372717"><strong>Copy Data</strong></span> &gt; File Systems &gt; <span id="Files-0044__en-us_topic_0000001839142377_text959224913332"><strong>Fileset</strong></span></b></span>.</span><p><div class="note" id="Files-0044__en-us_topic_0000001839142377_note192321935714"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="Files-0044__en-us_topic_0000001839142377_p62332915575">For 1.5.0, choose <span class="uicontrol" id="Files-0044__en-us_topic_0000001839142377_uicontrol35211628145713"><b><span id="Files-0044__en-us_topic_0000001839142377_text15521328105712"><strong>Explore</strong></span> &gt; <span id="Files-0044__en-us_topic_0000001839142377_text9521152813577"><strong>Copy Data</strong></span> &gt; <span id="Files-0044__en-us_topic_0000001839142377_text18417734832"><strong>Bare Metal</strong></span> &gt; <span id="Files-0044__en-us_topic_0000001839142377_text05221228115716"><strong>Fileset</strong></span></b></span>.</p>
</div></div>
</p></li><li><span>You can search for copies by fileset resource or copy. This section uses resources as an example.</span><p><p>On the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab page, find the resource to be restored by resource name and click the resource name.</p>
</p></li><li><span>In the <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> area, select the year, month, and day in sequence to find copies.</span><p><p>If <span><img src="en-us_image_0000001792548360.png"></span> is displayed below a month or date, copies exist in the month or on the day.</p>
</p></li><li><span>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Restore</strong></span></b></span> in the row where the copy is located.</span><p><p><a href="#Files-0044__table93951625101715">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="Files-0044__table93951625101715"></a><a name="table93951625101715"></a><table cellpadding="4" cellspacing="0" summary="" id="Files-0044__table93951625101715" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Parameters for restoring a fileset</caption><colgroup><col style="width:23.09%"><col style="width:76.91%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="23.09%" id="mcps1.3.3.2.4.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="76.91%" id="mcps1.3.3.2.4.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Restore To</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>Fileset restoration location.</p>
<ul><li><span><strong>Original location</strong></span>: Restores data to the original location on the original host.</li><li><span><strong>New location</strong></span>: Restores data to a new location. You need to specify the target host and path for restoration.<div class="note" id="Files-0044__note665716381615"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="Files-0044__p17892164110119">If restored files are not displayed in the restoration target location after restoration to a new location in Windows OS, open the folder in Windows OS and choose <strong id="Files-0044__b136041111123718">View</strong> &gt; <strong id="Files-0044__b1460431153713">Options</strong> to open the <strong id="Files-0044__b206049117372">Folder Options</strong> dialog box. Click the <strong id="Files-0044__b96041911203716">View</strong> tab and deselect <strong id="Files-0044__b126041111113715">Hide protected operating system files (Recommended)</strong>.</p>
</div></div>
</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only when data is restored to the original location.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Target Host</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only when data is restored to a new location. You need to select a target host.</p>
<p>You can search for a host name or IP address to select the host.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Target Path</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>This parameter is displayed only when data is restored to a new location. You need to select a target location.</p>
<p>After expanding the data source, you can search for a file name to select the corresponding path.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><strong>Incremental Restoration</strong></p>
</td>
<td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>After this function is enabled, files added and modified during incremental backup can be restored, but files deleted during incremental backup will not be deleted.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is displayed only in 1.6.0 and later versions. Only copies of 1.6.0 and later versions can be restored.</p>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Overwrite Rule</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>If a file with the same name exists in the restoration path, you can choose to replace or skip the existing file.</p>
<ul><li><span><strong>Replace existing files</strong></span></li><li><span><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li><li><span><strong>Only replace the files older than the restoration file</strong></span>: The latest file with the same name in the target path is retained.<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>For restoration using a hard link file copy, if a file with the same name exists in the restoration path, the overwriting rule supports only <strong>Replace existing files</strong>.</p>
</div></div>
</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Script to Run Before Restoration</strong></span></p>
</td>
<td class="cellrowborder" rowspan="3" valign="top" width="76.91%" headers="mcps1.3.3.2.4.2.2.2.3.1.2 "><p>You can configure <strong>Script to Run Before Restoration</strong>, <strong>Script to Run upon Restoration Success</strong>, and <strong>Script to Run upon Restoration Failure</strong> based on site requirements. When the message "ProtectAgent started executing the prerequisite task." is displayed during a backup job, the configured script is executed in the corresponding scenario.</p>
<ul><li>If the non-Windows OS is used, enter the script name, which ends with <strong>.sh</strong>. Ensure that the script is stored in <span class="filepath"><b>DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty</b></span> in the ProtectAgent installation directory.</li><li>If the Windows OS is used, enter the script name, which ends with <strong>.bat</strong>. Ensure that the script is stored in <span class="filepath"><b>DataBackup\ProtectClient\ProtectClient-E\bin\thirdparty</b></span> in the ProtectAgent installation directory.<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>If <span class="uicontrol"><b><span>Script to Run upon Restoration Success</span></b></span> is configured, the status of the restoration job is displayed as <span class="uicontrol"><b><span>Successful</span></b></span> on the <span>OceanProtect</span> even if the script fails to be executed. Check whether the job details contain information indicating that a post-processing script fails to be executed. If yes, modify the script in a timely manner. </p>
</div></div>
</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Script to Run upon Restoration Success</strong></span></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.2.2.3.1.1 "><p><span><strong>Script to Run upon Restoration Failure</strong></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="Files-0041_0.html">Restoration</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>