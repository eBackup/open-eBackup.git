<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing openGauss">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="opengauss-0159.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="opengauss-0161">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing openGauss</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="opengauss-0161"></a><a name="opengauss-0161"></a>
  <h1 class="topictitle1">Managing openGauss</h1>
  <div>
   <p>The <span>product</span> allows you to modify, remove, activate, or disable protection for openGauss databases.</p>
   <div class="section">
    <h4 class="sectiontitle">Related Operations</h4>
    <p>You need to log in to the management page, choose <span class="uicontrol"><b><span id="opengauss-0161__en-us_topic_0000001839142377_text197344403116"><strong>Protection</strong></span> &gt; Databases &gt; openGauss</b></span>, click the <strong>Databases</strong> or <strong>Instance</strong> tab, and find the target database or instance.</p>
   </div>
   <p><a href="#opengauss-0161__en-us_topic_0000001311097825_table24934294919">Table 1</a> describes related operations.</p>
   <div class="tablenoborder">
    <a name="opengauss-0161__en-us_topic_0000001311097825_table24934294919"></a><a name="en-us_topic_0000001311097825_table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="opengauss-0161__en-us_topic_0000001311097825_table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Operations related to openGauss database or instance protection
     </caption>
     <colgroup>
      <col style="width:12.15%">
      <col style="width:60.12%">
      <col style="width:27.73%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="12.15%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="60.120000000000005%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="27.730000000000004%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Modify Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>If you need to reselect an SLA associated with a resource, you can modify the protection plan. After the modification, the new protection policy will be used for the next replication or archiving.</p> <p><strong>Note</strong></p> <p>The modification does not affect the protection job that is being executed. The modified protection plan will take effect in the next protection period.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span> in the row where the resource is located.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Remove Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>If you do not need to protect a resource, you can remove the protection. After the removal, the system cannot execute protection jobs for the resource. To protect the resource again, you need to re-associate it with an SLA.</p> <p><strong>Note</strong></p> <p>When a protection job of the resource is running, the protection cannot be removed.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span> in the row where the resource is located.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Disable Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to disable a protection plan when periodic protection for the resource is not required. After the protection plan is disabled, the system does not execute automatic replication or archive jobs of the resource.</p> <p><strong>Note</strong></p> <p>Disabling protection does not affect ongoing backup jobs.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span>Disable Protection</span></b></span> in the row where the resource is located.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Activate Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>After a protection plan for a resource is disabled, you can activate the protection plan to make it take effect again. After the protection plan is activated, the system backs up the resource based on the protection plan.</p> <p><strong>Note</strong></p> <p>This operation can be performed only on instances that have been associated with an SLA and are in the <span class="uicontrol"><b><span><strong>Unprotected</strong></span></b></span> state.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span> in the row where the resource is located.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Adding a tag
         <div class="note" id="opengauss-0161__en-us_topic_0000001792344090_note161679211561">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="opengauss-0161__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="opengauss-0161__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol" id="opengauss-0161__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="opengauss-0161__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note" id="opengauss-0161__en-us_topic_0000001792344090_note164681316118">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul id="opengauss-0161__en-us_topic_0000001792344090_ul22969251338">
            <li id="opengauss-0161__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="opengauss-0161__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="opengauss-0161__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
            <li id="opengauss-0161__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="opengauss-0161__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="opengauss-0161__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="opengauss-0161__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Removing a tag
         <div class="note" id="opengauss-0161__en-us_topic_0000001792344090_note4957114117575">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="opengauss-0161__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="opengauss-0161__en-us_topic_0000001792344090_b1495945913127_1">Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p> <p></p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="opengauss-0161__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="opengauss-0161__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 "><p>Performing restoration</p></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to use a copy to restore database data.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target resource, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Restoration</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="12.15%" headers="mcps1.3.4.2.4.1.1 "><p>Performing a manual backup</p></td>
       <td class="cellrowborder" valign="top" width="60.120000000000005%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to execute a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
       <td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target resource, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="opengauss-0159.html">openGauss Database Environments</a>
    </div>
   </div>
  </div>
 </body>
</html>