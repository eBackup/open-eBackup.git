# 添加编译的子目录
add_subdirectory(file_resource)
add_subdirectory(host_restore)
add_subdirectory(host_backup)
add_subdirectory(host_livemount)
add_subdirectory(snapshot_provider)
add_subdirectory(host_archive)
add_subdirectory(host_index)
